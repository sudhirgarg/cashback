@include('front_include.front_header')
@include('front_include.front_menu')
    <div class="container-fluid banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(!empty($banner[0]))	
                    @foreach($banner as $img)
                    <img src="{{URL::asset('banner_images/'.$img->img)}}">
		    @endforeach
		    @else	
		    <img src="{{URL::asset('banner_images/banner-1.jpg')}}">	
		    @endif
                    <div class="banner-box">
                        <div class="banner-text">
                            <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                            <form name="search" action="{{action('SearchController@index')}}" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
			    </form>
                        </div>		
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
    	<div class="row">
        	<div class="col-md-12 bread">
            	<ol class="breadcrumb nopadding">
                  <li class="breadcrumb-item"><a href="{{action('IndexController@index')}}">Home</a></li>
                  <li class="breadcrumb-item active">FAQ's</li>
                </ol>
            </div>
        </div>
    </div>
    
    <!--Start Content section-->
    <div class="container">
    	<div class="row">
        	<div class="col-md-12">
                <div class="niews_title">
                    <h1 class="col-md-6 col-sm-6 col-xs-12 nopadding">FREQUENTLY ASKED QUESTIONS</h1>
                    
                </div>
            </div>	
        </div>
    </div>
    
    <div class="container m-t-3">
    	<div class="row">
		@if(!empty($faqlist))
		@foreach($faqlist as $list)
        	<div class="cool-blue col-md-12 faq-box">
		<h1>{{$list->name}}</h1>
		<p>{!! $list->detail !!}</p>
		</div>
            	
              @endforeach  
            </div>
            
	    @else
	    	<div class="col-md-10 col-sm-10 col-xs-12">
                    <div class="col-md-9 col-sm-8 col-xs-12 nopadding">
                        <p>{{'No records found'}}</p>
                    </div>
                    
                </div>
	    @endif	
            
           	<div class="pagination-section col-md-12 niews-pagi">
            	<nav aria-label="Page navigation">
                  <ul class="pagination">
		    <?php echo $faqlist1; ?>	
                  </ul>
                </nav>
            </div>
	    
        </div>
    </div>
    

    
    
        
    <!--End Content section-->
    
@include('front_include.front_footer')
