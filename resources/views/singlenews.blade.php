@include('front_include.front_header')
@include('front_include.front_menu')
    <div class="container-fluid banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(!empty($banner[0]))	
                    @foreach($banner as $img)
                    <img src="{{URL::asset('banner_images/'.$img->img)}}">
		    @endforeach
		    @else	
		    <img src="{{URL::asset('banner_images/banner-1.jpg')}}">	
		    @endif
                    <div class="banner-box">
                        <div class="banner-text">
                            <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                            <form name="search" action="{{action('SearchController@index')}}" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
			    </form>
                        </div>		
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
    	<div class="row">
        	<div class="col-md-12 bread">
            	<ol class="breadcrumb nopadding">
                  <li class="breadcrumb-item"><a href="{{action('IndexController@index')}}">Home</a></li>
                  <li class="breadcrumb-item"><a href="{{action('FrontNewsController@index')}}">Nieuws</a></li>
                  <li class="breadcrumb-item active">{{$news_detail[0]->name}}</li>
                  
                </ol>
            </div>
        </div>
    </div>
    
    <div class="container m-t-3">
    	<div class="row">
        	
	<div class="cool-blue col-md-12">
            	<div class="col-md-2 col-sm-2 col-xs-12 nopadding">
                	<div class="cool-box">
                    	<img src="{{URL::asset('news_images/'.$news_detail[0]->file)}}">
                    </div>
               	</div>
                <div class="col-md-10 col-sm-10 col-xs-12 mobilenews">
                    <div class="col-md-9 col-sm-8 col-xs-12 nopadding">
                       <h1>{{$news_detail[0]->name}}</h1>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12 nopadding">
                        <p class="niews_date"><?php echo date("d-m-Y", strtotime($news_detail[0]->date_upd)); ?></p>
                    </div>
                    {!! $news_detail[0]->news_content !!}
                </div>
            </div>
            
        </div>
	@if($previous!='')
	
	
	<a href="{{URL::to('singlenews/'.$pre_slug)}}"><input type="button" id="pre" value="TERUG"></a>
	
	@endif
	@if($next!='')
	
	<a href="{{URL::to('singlenews/'.$next_slug)}}"><input type="button" id="next" value="VOLGENDE"></a>
	
	@endif
    </div>
	
    <div style="clear:both;"></div>
    <hr class="linehr">

    <!--End Content section-->
    
    <div class="container-fluid slider-border">
    	<div class="slider-section">
    		<div class="container">
    			<div class="col-md-12 nopadding-slide">
                	
        			<div class="well">
            			<div id="myCarousel" class="carousel slide">
                
                            <!-- Carousel items -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="row">
                                        <?php $a=0; $st='0' ?>
						@foreach($links as $aff_link)


                            				<div class="col-sm-3 col-xs-3 first text-center">
                            					@if(!empty($email))<a href="{{$aff_link->affiliate_link}}">@else<a href="#" onclick="please_login()">@endif<img src="{{URl::asset('store_logos/'.$aff_link->logo)}}" alt="{{$aff_link->logo}}" class="img-responsive"></a>
                                				@if(!empty($email))<a href="{{$aff_link->affiliate_link}}" style="text-decoration:none;">@else<a href="#" onclick="please_login()" style="text-decoration:none;">@endif<h3>@if($a%2==0){{'cashback'}}@else{{'Ontvang'}}@endif</h3></a>
                                				@if(!empty($email))<a href="{{$aff_link->affiliate_link}}" style="text-decoration:none;">@else<a href="#" onclick="please_login()" style="text-decoration:none;">@endif<h1>{{$aff_link->cashback}}</h1></a>
                                				<p>{{$aff_link->affiliate_name}}</p>
                            				</div>
					<?php $a++; $st++; ?>   
					@if($a==4 && $st!='12')
						</div>
					    </div>
					<div class="item">
						<div class="row">
						<?php $a='0'; ?>
						@endif
						@endforeach
                               			</div><!--/row-->
                                </div>
                            </div><!--/carousel-inner-->  
                      	</div>
                        <!--/myCarousel-->
                     </div>
                    <!--/well-->
  				</div>
        	</div>
            <a class="left l-arrow carousel-control" href="#myCarousel" data-slide="prev"><img src="{{URL::asset('front_img/left-icon.png')}}"></a>
            <a class="right r-arrow carousel-control" href="#myCarousel" data-slide="next"><img src="{{URL::asset('front_img/right-icon.png')}}"></a>
     	</div>
    </div>
@include('front_include.front_footer')
