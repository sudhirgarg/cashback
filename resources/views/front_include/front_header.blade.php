<?php
$title_head = 'Cashback4you';
$parse = parse_url(url()->current());
if (isset($parse)) {
    $path = isset($parse['path']) ? $parse['path'] : '';
    if ($path) {
        $title_head = explode('/', $path);
        if (isset($title_head)) {
            $last_value = end($title_head);
            if ($last_value) {
                $last_value = str_replace('_', ' ', $last_value);
                $title_head = ucfirst($last_value) . ' | Cashback4you';
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $title_head ?></title>
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <!--<title>Cashback4you | @if(Request::segment(1) == 'signup') {{'Aanmelden'}} @elseif(Request::segment(1) == 'login') {{'Log in'}} @elseif(Request::segment(1) == 'news') {{'Nieuws'}} @elseif(Request::segment(1) == 'contact') {{'Contact Us'}} @elseif(Request::segment(1) == 'singlenews') {{'Nieuws'}} @elseif(Request::segment(1) == 'content') {{'Inhoud'}} @elseif(Request::segment(1) == '') {{'Home'}} @endif
            @if(Request::segment(1) == 'catlinks')
            @foreach($cat_lists as $cats)	                            
                                    @if(Request::segment(2) == $cats->id) {{$cats->name}} @endif
            @endforeach
            @endif
            @if(Request::segment(1) == 'alllinks') {{'Alle Affiliate Links'}} @endif
            @if(Request::segment(1) == 'faq') {{'FAQ'}} @endif
            @if(Request::segment(1) == 'account') {{'MIJN PROFIEL'}} @endif	
            @if(Request::segment(1) == 'forgot') {{'Wachtwoord vergeten'}} @endif
            @if(Request::segment(1) == 'search') {{'ZOEKEN'}} @endif
            @if(Request::segment(1) == 'allcatlink' || Request::segment(1) == 'catlinks' || Request::segment(1) == 'alllinks') {{'Affiliate Links'}} @endif                  
        </title>-->

        <!-- Bootstrap -->
        <link href="{{URL::asset('front_css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('front_css/style.css')}}" rel="stylesheet">
        <link href="{{URL::asset('front_css/responsive.css')}}" rel="stylesheet">
        <link href="{{url::asset('front_css/account.css')}}" rel="stylesheet">
        <link href="{{URL::asset('front_css/niews.css')}}" rel="stylesheet">
        <link href="{{URl::asset('front_css/page.css')}}" rel="stylesheet">
        <link href="{{URL::asset('front_css/login.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{URL::asset('front_css/font-awesome.min.css')}}">	
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- Begin TradeTracker SuperTag Code -->
        <script type="text/javascript">

            var _TradeTrackerTagOptions = {
                t: 'a',
                s: '259674',
                chk: '5be76f17334d906545e9b7fb8bf5f20b',
                overrideOptions: {}
            };

            (function () {
                var tt = document.createElement('script'), s = document.getElementsByTagName('script')[0];
                tt.setAttribute('type', 'text/javascript');
                tt.setAttribute('src', (document.location.protocol == 'https:' ? 'https' : 'http') + '://tm.tradetracker.net/tag?t=' + _TradeTrackerTagOptions.t + '&amp;s=' + _TradeTrackerTagOptions.s + '&amp;chk=' + _TradeTrackerTagOptions.chk);
                s.parentNode.insertBefore(tt, s);
            })();</script>
        <!-- End TradeTracker SuperTag Code -->

        <!-- Begin TradeTracker Code -->
        <script type="text/javascript"><!-- // -->< ![CDATA[
                    document.write('<' + 'script src="' + (document.location.protocol == 'https:' ? 'https' : 'http') + '://tm.tradetracker.net/public/tracker.js" type="text/javascript"><' + '/script>');
                    // ]]></script>
        <script type="text/javascript"><!-- // -->< ![CDATA[
                            var oTracker = new Tracker({
                                type: 'a',
                                site: 259674,
                                version: 1
                            });
                    // ]]></script>
    <noscript><img src="http://tm.tradetracker.net/pageview?t=a&amp;s=259674&amp;v=1" alt="" /></noscript>
    <!-- End TradeTracker Code -->
</head>