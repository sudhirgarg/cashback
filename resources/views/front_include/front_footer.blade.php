<div class="nav-footer container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="foo-navbar">
                    <ul class="navbar-nav nav footer_menu">
                        @foreach($single_content as $single)	
                        <li @if(Request::segment(2) == $single->slug)class="active"@endif><a href="{{URL::to('content/'.$single->slug)}}">{{$single->name}}</a></li><span>-</span>
                        @endforeach
                        <li @if(Request::segment(1) == 'faq')class="active"@endif><a href="{{action('IndexController@faq')}}">FAQ's</a></li><span>-</span>		
                        <li @if(Request::segment(1) == 'news')class="active"@endif><a href="{{action('FrontNewsController@index')}}">NIEuws</a></li><span>-</span>
                        @foreach($menus as $pages)	
                        <li @if(Request::segment(2) == $pages->slug)class="active"@endif><a href="{{URL::to('content/'.$pages->slug)}}">{{$pages->name}}</a></li><span>-</span>
                        @endforeach	
                        <li @if(Request::segment(1) == 'contact')class="active"@endif><a href="{{action('IndexController@contact')}}">contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container footer m-t-2">
    <div class="row">
        <div class="foo-bottom col-md-12">
            <div class="col-md-5 col-sm-5 col-xs-12 social m-t-2">
                <iframe src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2F128.199.143.5%2Fcashback%2Fpublic%2F&layout=button_count&size=small&mobile_iframe=true&appId=1155930261165446&width=68&height=20" width="68" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                <a class='for_mobile_twitter' href="https://twitter.com/share?url=http://128.199.143.5/cashback/public/&amp;text=Cash%20Back%20Site&amp;hashtags=cashback4you" target="_blank">
                    <img src="{{URL::asset('front_img/tw.png')}}" style="margin-top:-10px !important;">
                </a>
            </div>
            <?php
            $iframe = DB::table('iframe')->where('id', '=', '1')->where('is_active', '=', '1')->first();
            ?>
            <div class="col-md-7 col-sm-7 col-xs-12 foo-text">
                <div class="col-md-5 col-sm-5 col-xs-12 met">
                <!--<p>Klanten beoordelen ons met een:</p>-->
                </div>
                <div class="rating-section col-md-5 col-sm-6">
                    <?php echo (!empty($iframe->detail)) ? $iframe->detail : ''; ?>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{URL::asset('front_js/jquery.min.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{URL::asset('front_js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('front_js/bootbox.min.js')}}"></script>		
<script>
$("#claim_submit").on("click", function () {
    var purchase_immidiate = $('input[name=purchase_immidiate]:checked').val();
    var use_discount_code = $('input[name=use_discount_code]:checked').val();
    var cookies = $('input[name=cookies]:checked').val();
    var browser_in = $('input[name=browser_in]:checked').val();
    var ideal_payment = $('input[name=ideal_payment]:checked').val();
    var condition_of_shop = $('input[name=condition_of_shop]:checked').val();
    if (purchase_immidiate == undefined || use_discount_code == undefined || cookies == undefined || browser_in == undefined || ideal_payment == undefined || condition_of_shop == undefined) {
        $(".custom-error").css("display", "block");
        return false;
    }
    else {
        return true;
    }
});

function please_login() {
    bootbox.alert('U moet ingelogd zijn om deze pagina te bekijken.');
}
function more_info(str) {
    bootbox.alert('U moet ingelogd zijn om deze pagina te bekijken.');
}
function newclose() {
    var url = '<?php echo action("IndexController@login"); ?>';
    window.location.href = url;
}
function toggleIcon(e) {
    $(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('glyphicon-triangle-top glyphicon-triangle-bottom');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
})

var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("moreinfo");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close-pop")[0];

// When the user clicks the button, open the modal 
btn.onclick = function () {
    $("#myModal").css("display", "block");
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

</script>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>			
</body>
</html>
