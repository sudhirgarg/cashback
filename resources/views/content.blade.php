@include('front_include.front_header')
@include('front_include.front_menu')
    <div class="container-fluid banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(!empty($banner[0]))	
                    @foreach($banner as $img)
                    <img src="{{URL::asset('banner_images/'.$img->img)}}">
		    @endforeach
		    @else	
		    <img src="{{URL::asset('banner_images/banner-1.jpg')}}">	
		    @endif
                    <div class="banner-box">
                        <div class="banner-text">
                            <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                            <form name="search" action="{{action('SearchController@index')}}" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
			    </form>
                        </div>		
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
    	<div class="row">
        	<div class="col-md-12 bread">
            	<ol class="breadcrumb nopadding">
                  <li class="breadcrumb-item"><a href="{{action('IndexController@index')}}">Home</a></li>
                  <li class="breadcrumb-item active">{{$content_detail[0]->page_name}}</li>
                  
                </ol>
            </div>
        </div>
    </div>
    <div class="container">
    	<div class="row">
        	<div class="col-md-12">
                <div class="niews_title">
                    <h1 class="col-md-6 col-sm-6 col-xs-12 nopadding">{{$content_detail[0]->page_name}}</h1>
                </div>
            </div>	
        </div>
    </div>		
    <div class="container m-t-3 content">
    	<div class="row">
        	<div class="cool-blue col-md-12">                
                    <p class="">{!! $content_detail[0]->page !!}</p>
            </div>
            
        </div>
    </div>
    
   
    <!--End Content section-->
@if(Request::segment(2) == 'howitwork')
<div style="clear:both;"></div>
    <hr class="linehr">
 <div class="container-fluid slider-border">
    	<div class="slider-section">
    		<div class="container">
    			<div class="col-md-12 nopadding-slide">
                	
        			<div class="well">
            			<div id="myCarousel" class="carousel slide">
                
                            <!-- Carousel items -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="row">
                                        <?php $a=0; $st='0' ?>
						@foreach($links as $aff_link)


                            				<div class="col-sm-3 col-xs-3 first text-center">@if(!empty($email))
                                        <a href="{{$aff_link->affiliate_link}}" target="_blank">@else<a href="#" onclick="please_login()">
                                                @endif
                                                <?php if (filter_var($aff_link->logo, FILTER_VALIDATE_URL)) { ?>
                                                <img src="<?php echo $aff_link->logo?>"  alt="{{$aff_link->logo}}" class="img-responsive" />
                                                <?php } else { ?>
                                                    <img src="{{URl::asset('store_logos/'.$aff_link->logo)}}" alt="{{$aff_link->logo}}" class="img-responsive">
                                                <?php } ?>
                                            </a>
                                				@if(!empty($email))<a href="{{$aff_link->affiliate_link}}" style="text-decoration:none;">@else<a href="#" onclick="please_login()" style="text-decoration:none;">@endif<h3>@if($a%2==0){{'cashback'}}@else{{'Ontvang'}}@endif</h3></a>
                                				@if(!empty($email))<a href="{{$aff_link->affiliate_link}}" style="text-decoration:none;">@else<a href="#" onclick="please_login()" style="text-decoration:none;">@endif<h1>{{$aff_link->cashback}}</h1></a>
                                				<p>{{$aff_link->affiliate_name}}</p>
                            				</div>
					<?php $a++; $st++; ?>   
					@if($a==4 && $st!='12')
						</div>
					    </div>
					<div class="item">
						<div class="row">
						<?php $a='0'; ?>
						@endif
						@endforeach
                               			</div><!--/row-->
				
                                </div>
                            </div><!--/carousel-inner--> 
                      	</div>
                        <!--/myCarousel-->
                     </div>
                    <!--/well-->
  				</div>
        	</div>
            <a class="left l-arrow carousel-control" href="#myCarousel" data-slide="prev"><img src="{{URL::asset('front_img/left-icon.png')}}"></a>
            <a class="right r-arrow carousel-control" href="#myCarousel" data-slide="next"><img src="{{URL::asset('front_img/right-icon.png')}}"></a>
     	</div>
    </div>
@endif
@include('front_include.front_footer')
