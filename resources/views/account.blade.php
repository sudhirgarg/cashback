@include('front_include.front_header')
@include('front_include.front_menu')
<?php

function month($monthName) {
    $month = array('January' => 'januari',
        'February' => 'februari',
        'March' => 'maart',
        'April' => 'april',
        'May' => 'mei',
        'June' => 'juni',
        'July' => 'juli',
        'August' => 'augustus',
        'September' => 'september',
        'October' => 'oktober',
        'November' => 'november',
        'December' => 'december'
    );

    foreach ($month as $key => $value):
        $monthName = str_replace($key, $value, $monthName);
    endforeach;

    return $monthName;
}
?>
<div class="container-fluid banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(!empty($banner[0]))	
                @foreach($banner as $img)
                <img src="{{URL::asset('banner_images/'.$img->img)}}">
                @endforeach
                @else	
                <img src="{{URL::asset('banner_images/banner-1.jpg')}}">	
                @endif
                <div class="banner-box">
                    <div class="banner-text">
                        <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                        <form name="search" action="{{action('SearchController@index')}}" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
                        </form>
                    </div>		
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 bread">
            <ol class="breadcrumb nopadding">
                <li class="breadcrumb-item"><a href="{{action('IndexController@index')}}">Home</a></li>
                <li class="breadcrumb-item active">Account</li>
            </ol>
        </div>
        <div class="col-md-12 mail-address">
            <h1>{{$email}}</h1>
        </div>
        <div class="col-md-12 date">
            <p>Lid sinds <!--1 juli 2016-->{{ date('d', strtotime($user_detail[0]->date_add)) }} <?php
                $month = date('F', strtotime($user_detail[0]->date_add));
                echo month($month)
                ?> {{ date('Y', strtotime($user_detail[0]->date_add)) }}</p>
        </div>
    </div>
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    @endforeach
</div>

<div class="container-fluid page-tab-section">
    <div class="container m-t-2">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-section">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#section1" data-toggle="tab">OVERZICHT</a></li>
                        <li><a data-toggle="tab" href="#section2">TRANSACTIES</a></li>
                        <li><a data-toggle="tab" href="#section3">UITBETALINGEN</a></li>
                        <li><a data-toggle="tab" href="#section4">CASHBACK CLAIMS</a></li>
                        <li><a data-toggle="tab" href="#section5">MIJN GEGEVENS</a></li>
                        <li><a data-toggle="tab" href="#section6">MELD IEMAND AAN</a></li>

                    </ul>
                </div>
            </div>

            <div class="col-md-12 m-t-2">
                <div class="tab-content">
                    <div id="section1" class="tab-pane fade m-t-1 in active">
                        <div class="col-md-4 col-sm-4">
                            <div class="tab-box">
                                <h3>JOUW SALDO:</h3>
                                <h1 class="saldo_price">€{{number_format($balance_redeem_amount, 2, ',', '.')}}</h1>
                                <div class="saldo">
                                    <p>Jouw saldo is berekend met het optellen van al jouw cashbacks minus alle mogelijke uitbetalingen.</p>
                                    <p class="alleen">Alleen cashbacks die met succes zijn verwerkt wordt in de optelsom meegerekend.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="tab-box transact">
                                <h3>Transacties</h3>
                                <ul class="transact-line">
                                    @foreach($transactions as $trans_list)
                                    <li>
                                        <p class="col-md-6">#<!--{{$trans_list->transaction_id}}-->Transactie 1 </p>
                                        <h1 class="col-md-6">€ {{number_format($trans_list->amount, 2, ',', '.')}} </h1>
                                    </li>
                                    @endforeach
                                    <div class="transactie-btn"><a href="{{action('FrontDashboardController@listtrans')}}"><button class="btn btn-default more-gray-btn" type="submit">LEES MEER</button></a></div>

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="tab-box transact">
                                <h3>10% bonus transacties</h3>
                                <ul class="transact-line">
                                    <li>
                                        <p class="col-md-6">Transactie 1 </p>
                                        <h1 class="col-md-6">€ 12,50 </h1>
                                    </li>
                                    <li>
                                        <p class="col-md-6">Transactie 1 </p>
                                        <h1 class="col-md-6">€ 12,50 </h1>
                                    </li>
                                    <li>
                                        <p class="col-md-6">Transactie 1 </p>
                                        <h1 class="col-md-6">€ 12,50 </h1>
                                    </li>
                                    <li>
                                        <p class="col-md-6">Transactie 1 </p>
                                        <h1 class="col-md-6">€ 12,50 </h1>
                                    </li>
                                    <li>
                                        <p class="col-md-6">Transactie 1 </p>
                                        <h1 class="col-md-6">€ 12,50 </h1>
                                    </li>
                                    <li>
                                        <p class="col-md-6">Transactie 1 </p>
                                        <h1 class="col-md-6">€ 12,50 </h1>
                                    </li>
                                    <div class="transactie-btn"><a href="{{action('FrontDashboardController@bonustrans')}}"><button class="btn btn-default more-gray-btn" type="submit">LEES MEER</button></a></div>

                                </ul>
                            </div>
                        </div>

                    </div>

                    <div id="section2" class="transact tab-pane fade">
                        <h2>TRANSACTIES & CASHBACKS</h2>
                        <div class="col-md-12 nopadding status-transact">
                            <div class="col-md-2 col-sm-3 col-xs-12">
                                <h2>STATUS UITLEG</h2>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 leg">
                                <h5>Bevestigd</h5>
                                <p>Jouw aankoop is geregistreerd door de webshop en door ons.</p>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 leg">
                                <h5>Goedgekeurd</h5>
                                <p>De webshop heeft jouw transactie en de cashback goedgekeurd.</p>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 leg">
                                <h5>Afgekeurd</h5>
                                <p>De webshop heeft de cashback afgekeurd.</p>
                            </div>
                        </div>

                        <div class="transact-table table-responsive m-t-3">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Webshop</th>
                                        <th>Datum</th>
                                        <th>CashbacK</th>
                                        <th>Status</th>
                                        <th>DEEL JOUW WINST OP FACBOOK</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($transactions_all as $detail_tran)
                                    <tr>
                                        <td>{{$detail_tran->store_id}}</td>
                                        <td><?php echo date("d-m-Y", strtotime($detail_tran->datetime)); ?></td>
                                        <td>€{{number_format($detail_tran->amount, 2, ',', '.')}} </td>
                                        <td><strong>@if($detail_tran->status == 1){{'Goedgekeurd'}}@else{{'Rejected'}}@endif</strong></td>
                                        <td><a class="scmFacebook" href='https://www.facebook.com/dialog/share?app_id=1808259536097718&display=popup&href=http://139.59.3.57/cashback/public/&summary=test123&title=cashback&nbsp;received=€<?php echo number_format($detail_tran->amount, 2, ',', '.') ?>&redirect_uri=http://139.59.3.57/cashback/public/account'><strong>@if($detail_tran->type == 'referral' || $detail_tran->type == 'api' || $detail_tran->type == 'Api'){{'Delen'}}@else {{ucfirst($detail_tran->type)}}@endif</strong></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div> 
                    </div>

                    <div id="section3" class="tab-pane fade ">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="uitbatel-aan">
                                <h1>UITBETALING AANvragen</h1>
                                <p>Uitbetalingen zijn mogelijk vanaf een bedrag van € 25 </p>
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <form class="form-horizontal m-t-3" action="{{action('FrontDashboardController@paymentreq')}}" method="post">
                                    <div class="form-uitbatel">
                                        <div class="form-group">
                                            <label class="control-label col-sm-6" for="email">Saldo</label>
                                            <div class="col-sm-5 nopadding">
                                                <h4 class="uti-price">€ {{number_format($balance_redeem_amount, 2, ',', '.')}}</h4>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-6" for="email">Gewenst uitbetalings bedrag:</label>
                                            <div class="col-sm-5 nopadding">
                                                <input type="text" class="form-control" id="text" name="desired_amount" value="{{old('desired_amount')}}" placeholder="">
                                            </div>
                                        </div>
                                        <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                                        <div class="form-group">
                                            <label class="control-label col-sm-6" for="email">Rekeningnummer:</label>
                                            <div class="col-sm-5 nopadding">
                                                <input type="text" class="form-control" id="text" name="account_no" value="{{old('account_no')}}" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-6" for="email">BIC nummer:</label>
                                            <div class="col-sm-5 nopadding">
                                                <input type="text" class="form-control" id="txt" name="bic_number" value="{{old('bic_number')}}" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-6" for="email">Ter name van:</label>
                                            <div class="col-sm-5 nopadding">
                                                <input type="text" class="form-control" name="ter_name_van" value="{{old('ter_name_van')}}" id="email" placeholder="">
                                            </div>
                                        </div>

                                        <div class="col-md-12 opslaan-btn">
                                            <button class="btn btn-default more-gray-btn " type="submit">UITBETALING AANVRAGEN</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h1>UITBETALINGEN</h1>
                            <p class="blank"><br></p>
                            <div class="uitbatel-table table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Datum</th>
                                            <th>CashbacK</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>	
                                        @foreach($transactions as $trans)
                                        <tr>
                                            <td>{{ date('d-m-Y', strtotime($trans->datetime))}} </td>
                                            <td>€{{number_format($trans->amount, 2, ',', '.')}} </td>
                                            <td><strong>@if($trans->status == 1){{'Goedgekeurd'}}@else{{'Rejected'}}@endif</strong></td>                                               
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 	
                        </div>
                    </div>

                    <div id="section4" class="tab-pane fade">
                        <h1>Cashback Claims</h1>
                        <p>Als er iets is misgegaan met een cashback-registratie kan je hieronder een cashback claim indienen. Lees wel eerst de FAQ - Hoe werkt een Cashback Claim?</p>
                        <p>Op het moment dat je een claim indient, ga je akkoord met de Cashback Claim Voorwaarden. Lees deze daarom goed door om een eventuele teleurstelling te voorkomen.</p>
                        <p>Maak pas een claim aan als de periode om te retourneren voorbij is en laat ons weten welke items je eventueel hebt geretourneerd. </p>

                        <div class="form-claim col-md-12">
                            <h1>Claim INDIENEN</h1>
                            <p>Vul alle verplichte velden in, anders kunnen we de claim niet in behandeling nemen.</p>
                            <div class="custom-error" style="display:none;">
                                <ul>
                                    <li>Er staan nog keuzes open, vul alle antwoorden in</li>
                                </ul>
                            </div>
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form class="form-horizontal m-t-3" method="post" action="{{action('FrontDashboardController@claimrequest')}}">
                                <div class="col-md-5 nopadding">
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Soort claim:*</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="text" name="type_claim" value="{{ old('type_claim') }}" class="form-control" id="text" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Selecteer webshop:*</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="text" class="form-control" value="{{ old('type_shop') }}" name="type_shop" id="text" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Selecteer bezoek:*</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="text" class="form-control" value="{{ old('select_visit') }}" name="select_visit" id="txt" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Klantnummer:*</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="email" class="form-control" value="{{ old('client_number') }}" name="client_number" id="email" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="pwd">Order -of polisnummer:*</label>
                                        <div class="col-sm-5 nopadding">          
                                            <input type="text" class="form-control" value="{{ old('order_of') }}" id="pwd" name="order_of"  placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Polis of factuur:*</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="text" class="form-control" value="{{ old('policy') }}" id="txt" name="policy" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Gebruikt apparaat:*</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="text" class="form-control" id="txt" name="used_equipment" value="{{ old('used_equipment') }}" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Gebruikte browser:*</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="text" class="form-control" value="{{ old('used_browser') }}" id="txt" name="used_browser" placeholder="">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-7 nopadding">
                                    <div id="file1" class="btn-group" data-toggle="buttons">
                                        <div class="form-group">
                                            <label for="" class="col-md-10 col-sm-10 col-xs-12 nopadding control-label">Heb je na je click vanaf Cashback4you direct je aankoop gedaan?*</label>
                                            <div class="col-md-2 col-sm-2 col-xs-12 radio-btn">
                                                <div class="radio">
                                                    <label><input type="radio" id="purchase_immidiate" name="purchase_immidiate" value="1" @if(old('purchase_immidiate')=='1') checked @endif>ja</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input type="radio" id="purchase_immidiate" name="purchase_immidiate" @if(old('purchase_immidiate')=='0')checked @endif value="0">nee</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-md-10 col-sm-10 col-xs-12 nopadding control-label">Heb je een kortingscode van een andere site gebruikt?*</label>
                                            <div class="col-md-2 col-sm-2 col-xs-12 radio-btn">
                                                <div class="radio">
                                                    <label><input type="radio" id="use_discount_code" name="use_discount_code" @if(old('use_discount_code')=='1') checked @endif value="1">ja</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input type="radio" id="use_discount_code" name="use_discount_code" @if(old('use_discount_code')=='0') checked @endif value="0">nee</label>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>	
                                        <div class="form-group">
                                            <label for="" class="col-md-10 col-sm-10 col-xs-12 nopadding control-label">Heb je alle cookies geaccepteerd?*</label>
                                            <div class="col-md-2 col-sm-2 col-xs-12 radio-btn">
                                                <div class="radio">
                                                    <label><input type="radio" id="cookies" name="cookies" @if(old('cookies')=='1') checked @endif value="1">ja</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input type="radio" id="cookies" name="cookies" @if(old('cookies')=='0') checked @endif value="0">nee</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-md-10 col-sm-10 col-xs-12 nopadding control-label">Stond je browser in incognitomodus of Do Not Track modus?*</label>
                                            <div class="col-md-2 col-sm-2 col-xs-12 radio-btn">
                                                <div class="radio">
                                                    <label><input type="radio" id="browser_in" name="browser_in" @if(old('browser_in')=='1') checked @endif value="1">ja</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input type="radio" id="browser_in" name="browser_in" @if(old('browser_in')=='0') checked @endif value="0">nee</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-md-10 col-sm-10 col-xs-12 nopadding control-label">Ben je na je iDeal betaling teruggegaan naar de bedanktpagina?*</label>
                                            <div class="col-md-2 col-sm-2 col-xs-12 radio-btn">
                                                <div class="radio">
                                                    <label><input type="radio" id="ideal_payment" name="ideal_payment" @if(old('ideal_payment')=='1') checked @endif value="1">ja</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input type="radio" id="ideal_payment" name="ideal_payment" @if(old('ideal_payment')=='0') checked @endif value="0">nee</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-md-10 col-sm-10 col-xs-12 nopadding control-label">Heb je rekening gehouden met de voorwaarden van de webshop?*</label>
                                            <div class="col-md-2 col-sm-2 col-xs-12 radio-btn">
                                                <div class="radio">
                                                    <label><input type="radio" id="condition_of_shop" name="condition_of_shop" @if(old('condition_of_shop')=='1') checked @endif value="1">ja</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input type="radio" id="condition_of_shop" name="condition_of_shop" @if(old('condition_of_shop')=='0') checked @endif value="0">nee</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group text-area">
                                            <label class="col-md-3 nopadding" for="exampleTextarea">Toelichting claim:</label>
                                            <textarea class="form-control col-md-9 " id="exampleTextarea" name="description" rows="3">{{ old('description') }}</textarea>
                                        </div>


                                    </div>
                                </div>
                                <button class="btn btn-default more-gray-btn" id="claim_submit" type="submit">INDIENEN</button>
                            </form>	
                        </div>
                    </div>

                    <div id="section5" class="tab-pane fade">
                        <h1>MIJN GEGEVENS</h1>
                        <p>Onderstaand zijn jouw accountgegevens weergegeven.</p>

                        <div class="form-claim form-mijn col-md-12">
                            <p>Vul alle verplichte velden in, anders kunnen we de claim niet in behandeling nemen.</p>
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form class="form-horizontal m-t-3" action="{{action('FrontDashboardController@updatedetail')}}" method="post">
                                <div class="col-md-5 nopadding">
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">E-mailadres:</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="email" class="form-control" name="email" id="text" placeholder="Email" value="{{$user_detail[0]->email}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Nieuw wachtwoord:</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="password" class="form-control" name="password" id="text" placeholder="**********">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Herhaal wachtwoord:</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="password" class="form-control" name="confirm_password" id="txt" placeholder="**********">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Geboortedatum:</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="date" class="form-control" id="email" name="dob" value="{{$user_detail[0]->dob}}" placeholder="YYYY-mm-dd">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="pwd">Geslacht:</label>
                                        <div class="col-sm-3 col-xs-12 nopadding">
                                            <div class="radio">
                                                <label><input type="radio" name="sex" value="male" @if($user_detail[0]->sex == 'male') {{'checked'}}@endif>Man</label>
                                            </div>
                                            <div class="radio">
                                                <label><input type="radio" name="sex" value="female" @if($user_detail[0]->sex == 'female') {{'checked'}}@endif>Vrouw</label>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Voornaam:</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="text" class="form-control" name="firstname" value="{{$user_detail[0]->firstname}}" id="txt" placeholder="First name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Achternaam:</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="text" class="form-control" name="lastname" value="{{$user_detail[0]->lastname}}" id="txt" placeholder="Last name">
                                        </div>
                                    </div>



                                </div>
                                <div class="col-md-7 nopadding">
                                    <div id="file1" class="btn-group" data-toggle="buttons">
                                        <div class="form-group">        
                                            <div class="col-sm-12 nopadding">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="vehicle" checked> Bevestiging- en goedkeuringsemails van je cashback na een aankoop.
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">        
                                            <div class="col-sm-12 nopadding">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="vehicle" checked> Belangrijke mails m.b.t. je account
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">        
                                            <div class="col-sm-12 nopadding">
                                                <div class="checkbox">
                                                    <label><input type="checkbox" name="vehicle" checked> De beste aanbiedingen & cashbacks van de webshops op Cashback4you. 
                                                        Je ontvangt één keer per week de nieuwsbrief.
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 opslaan-btn">
                                    <button class="btn btn-default more-gray-btn " type="submit">OPSLAAN</button>
                                </div>
                            </form>	
                        </div>
                    </div>

                    <div id="section6" class="meld tab-pane fade">
                        <div class="aan">
                            <h1>MELD IEMAND AAN</h1>
                            <p class="pull-left">Verdien 10 euro door iemand aan te melden!</p>
                            <div class="total"><p>Totaal aangebrachte leden:</p><span>{{$total_referral_count}} LEDEN</span></div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 uitleg">
                            <h1>UITLEG</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam porta, ante a consectetur scelerisque, orci nisi accumsan erat, ut accumsan mi elit in nulla. Nam nisi urna, convallis at tellus vel, volutpat porttitor urna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque nec sodales sem. Aliquam sit amet dui pharetra, consequat metus in, volutpat lacus. Cras hendrerit pellentesque lobortis. Sed ultrices maximus molestie. Nam molestie est sapien, vitae sagittis purus rutrum vitae. Fusce at bibendum nulla.</p>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-uitleg">	
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <form action="{{action('FrontDashboardController@referral')}}" id="ref" method="post" class="form-horizontal m-t-3">
                                <div class="">
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Naam:</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="text" class="form-control" id="text" name="name" value="{{ old('name') }}" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">Achternaam:</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="text" class="form-control" id="text" value="{{ old('surname') }}" name="surname" placeholder="">
                                        </div>
                                    </div>
                                    <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                                    <div class="form-group">
                                        <label class="control-label col-sm-6" for="email">E-mailadres vriend:</label>
                                        <div class="col-sm-5 nopadding">
                                            <input type="email" class="form-control" value="{{ old('email') }}" id="txt" name="email" placeholder="">
                                        </div>
                                    </div>
                                    <input type="hidden" class="form-control" value="{{$user_detail[0]->email}}" id="email" name="my_email" placeholder="" readonly>

                                    <div class="col-md-12 opslaan-btn">
                                        <button class="btn btn-default more-gray-btn " id="submit" type="submit">VERSTUUR</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!--Containerfluid-->   

<div class="container-fluid slider-border">
    <div class="slider-section">
        <div class="container">
            <div class="col-md-12 nopadding-slide">
                <h1 class="text-left slide-heading">INTERESSANTE cASHBACKS VOOR JOU</h1>
                <div class="well">
                    <div id="myCarousel" class="carousel slide">

                        <!-- Carousel items -->
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="row">
                                    <?php
                                    $a = 0;
                                    $st = '0'
                                    ?>
                                    @foreach($links as $aff_link)


                                    <div class="col-sm-3 col-xs-3 first text-center">
                                         @if(!empty($email))
                                        <a href="{{$aff_link->affiliate_link}}" target="_blank">@else<a href="#" onclick="please_login()">
                                                @endif
                                                <?php if (filter_var($aff_link->logo, FILTER_VALIDATE_URL)) { ?>
                                                <img src="<?php echo $aff_link->logo?>"  alt="{{$aff_link->logo}}" class="img-responsive" />
                                                <?php } else { ?>
                                                    <img src="{{URl::asset('store_logos/'.$aff_link->logo)}}" alt="{{$aff_link->logo}}" class="img-responsive">
                                                <?php } ?>
                                            </a>
                                            @if(!empty($email))<a href="{{$aff_link->affiliate_link}}" style="text-decoration:none;" target="_blank">@else<a href="#" onclick="please_login()" style="text-decoration:none;">@endif<h3>@if($a%2==0){{'cashback'}}@else{{'Ontvang'}}@endif</h3></a>
                                                @if(!empty($email))<a href="{{$aff_link->affiliate_link}}" style="text-decoration:none;" target="_blank">@else<a href="#" onclick="please_login()" style="text-decoration:none;">@endif<h1>{{$aff_link->cashback}}</h1></a>
                                                    <p>{{$aff_link->affiliate_name}}</p>
                                                    </div>
                                                    <?php
                                                    $a++;
                                                    $st++;
                                                    ?>   
                                                    @if($a==4 && $st!='12')
                                                    </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="row">
<?php $a = '0'; ?>
                                                            @endif
                                                            @endforeach
                                                        </div><!--/row-->
                                                    </div>
                                                    </div><!--/carousel-inner--> 
                                                    </div>
                                                    <!--/myCarousel-->
                                                    </div>
                                                    <!--/well-->
                                                    </div>
                                                    </div>
                                                    <a class="left l-arrow carousel-control" href="#myCarousel" data-slide="prev"><img src="{{URL::asset('front_img/left-icon.png')}}"></a>
                                                    <a class="right r-arrow carousel-control" href="#myCarousel" data-slide="next"><img src="{{URL::asset('front_img/right-icon.png')}}"></a>
                                                    </div>
                                                    </div>
                                                    @include('front_include.front_footer')
