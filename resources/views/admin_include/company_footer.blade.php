<!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; CASHBACK4YOU.
                <!--<a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>-->
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{URL::asset('admin_css/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('admin_css/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('admin_css/assets/global/plugins/js.cookie.min.js" type="text/javascript')}}"></script>
        <script src="{{URL::asset('admin_css/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('admin_css/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('admin_css/assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('admin_css/assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('admin_css/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{URL::asset('admin_css/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('admin_css/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('admin_css/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{URL::asset('admin_css/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{URL::asset('admin_css/assets/pages/scripts/form-samples.min.js')}}" type="text/javascript"></script>
	<script src="{{URL::asset('admin_css/assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{URL::asset('admin_css/assets/layouts/layout4/scripts/layout.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('admin_css/assets/layouts/layout4/scripts/demo.min.js')}}" type="text/javascript"></script>
	<script src="{{URL::asset('admin_css/assets/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
	<script>
	 CKEDITOR.replace( 'content_page_detail' );
	</script>
	<script>
	function select_category(str){
		$.ajax({
	      		//url: 'http://127.0.0.1/cashback/public/index.php/admin/select_category',
			url: 'http://139.59.3.57/cashback/public/admin/select_category',
	     		type: 'GET',
	      		data: {'storeid':str},
	      		success: function(data){
				if(data!=''){
					document.getElementById("category").innerHTML =data;
				}
	      		}
    		}); 
	}

	function check_total(){
		var inviter_percentage=document.getElementById("inviter_percentage").value;
		var user_percentage=document.getElementById("user_percentage").value;
		var cashback4you_percentage=document.getElementById("cashback4you_percentage").value;
		var total=parseFloat(inviter_percentage)+parseFloat(user_percentage)+parseFloat(cashback4you_percentage)
		if(total!=100){
			document.getElementById("msg").innerHTML='Total % must be 100';
			$(".green").css("display","none");
		}
		else{
			$(".green").css("display","");
			document.getElementById("msg").innerHTML='';
		}
		return false;
	}
	
	</script>
    </body>

</html>
