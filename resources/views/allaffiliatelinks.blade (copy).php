@include('front_include.front_header')
@include('front_include.front_menu')
    <div class="container-fluid banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
		    @if(!empty($banner[0]))	
                    @foreach($banner as $img)
                    <img src="{{URL::asset('banner_images/'.$img->img)}}" style="width:1010px; height:337px;">
		    @endforeach
		    @else	
		    <img src="{{URL::asset('banner_images/banner-1.jpg')}}">	
		    @endif		
                    <div class="banner-box">
                        <div class="banner-text">
                            <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                            <form name="search" action="{{action('SearchController@index')}}" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
			    </form>
                        </div>		
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
    	<div class="row">
        	<div class="col-md-12 bread">
            	<ol class="breadcrumb nopadding">
                  <li class="breadcrumb-item"><a href="{{action('IndexController@index')}}">Home</a></li>
                  <li class="breadcrumb-item active">Affilate Links</li>	
                </ol>
            </div>
        </div>
    </div>
    
    <!--Start Content section-->
    <div class="container">
    	<div class="row">
        	<div class="col-md-12">
                <div class="niews_title">
                    <h1 class="col-md-6 col-sm-6 col-xs-12 nopadding">Affiliate Links</h1>
                    
                </div>
            </div>	
        </div>
    </div>
    
    <div class="container m-t-3">
    	<div class="row">
		@if(!empty($allaffiliatelinks))
			<div class="item">
                        	<div class="row new">
				@foreach($allaffiliatelinks as $list)
                            		<div class="col-sm-3 col-xs-3 first text-center">
						<div class="list-aff">
                            			@if(!empty($email))<a href="{{$list->affiliate_link}}">@else<a href="#" onclick="please_login()">@endif<img src="{{URl::asset('store_logos/'.$list->logo)}}" alt="{{$list->logo}}" style="width:254px;height:60px;" class="img-responsive"></a>
                                		<h3>cashback</h3>
                                		<h1>{{$list->cashback}}</h1>
                                		<p>{{$list->affiliate_name}}</p>
						</div>
                            		</div>	
				@endforeach
            			</div>
			</div>
	    	@else
	    	<div class="col-md-10 col-sm-10 col-xs-12">
                    <div class="col-md-9 col-sm-8 col-xs-12 nopadding">
                        <p>{{'No records found'}}</p>
                    </div>    
                </div>
	    	@endif	
            
           	<div class="pagination-section col-md-12 niews-pagi">
            	<nav aria-label="Page navigation">
                  <ul class="pagination">
		    <?php echo $affiliatelinks1; ?>	
                  </ul>
                </nav>
            </div>
	    
        </div>
    </div>
    @foreach($cats as $catlist)
	{{$catlist->name}}
	<?php
	echo '<br/>';
	$subcats=AffiliateController::getsubcats($catlist->id); 	
	foreach($subcats as $sub){ ?>
	<a href="{{URL::to('catlinks/'.$sub->id)}}"><?php echo '---'.$sub->name; ?></a>
		<?php echo '<br/>'; ?>
	<?php }
		
	?>
    @endforeach    

    <!--End Content section-->
    
    <div class="container-fluid slider-border">
    	<div class="slider-section">
    		<div class="container">
    			<div class="col-md-12 nopadding-slide">
                	<h1 class="text-left slide-heading">INTERESSANTE cASHBACKS VOOR JOU</h1>
        			<div class="well">
            			<div id="myCarousel" class="carousel slide">
                
                            <!-- Carousel items -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="row">
                                        <?php $a=0; $st='0' ?>
						@foreach($links as $aff_link)


                            				<div class="col-sm-3 col-xs-3 first text-center">
                            					@if(!empty($email))<a href="{{$aff_link->affiliate_link}}">@else<a href="#" onclick="please_login()">@endif<img src="{{URl::asset('store_logos/'.$aff_link->logo)}}" alt="{{$aff_link->logo}}" style="width:233px;height:52px;" class="img-responsive"></a>
                                				<h3>cashback</h3>
                                				<h1>{{$aff_link->cashback}}</h1>
                                				<p>{{$aff_link->affiliate_name}}</p>
                            				</div>
					<?php $a++; $st++; ?>   
					@if($a==4 && $st!='12')
						</div>
					    </div>
					<div class="item">
						<div class="row">
						<?php $a='0'; ?>
						@endif
						@endforeach
                               			</div><!--/row-->
                                </div>
                            </div><!--/carousel-inner--> 
                      	</div>
                        <!--/myCarousel-->
                     </div>
                    <!--/well-->
  				</div>
        	</div>
            <a class="left l-arrow carousel-control" href="#myCarousel" data-slide="prev"><img src="{{URL::asset('front_img/left-icon.png')}}"></a>
            <a class="right r-arrow carousel-control" href="#myCarousel" data-slide="next"><img src="{{URL::asset('front_img/right-icon.png')}}"></a>
     	</div>
    </div>
@include('front_include.front_footer')
