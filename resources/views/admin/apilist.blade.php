@include("admin_include.company_header")
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
	@include("admin_include.admin_center")
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            @include("admin_include.admin_sidebar")
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Api's List</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{action('admin\DashboardController@index')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Api's List</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">	
				@foreach (['danger', 'warning', 'success', 'info'] as $msg)
      					@if(Session::has('alert-' . $msg))
						<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      					@endif
    				@endforeach
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <!--<div class="btn-group pull-right">
                                                    <a href="{{action('admin\FaqController@index')}}"><button id="sample_editable_1_new" class="btn sbold green"> Add New FAQ
                                                        <i class="fa fa-plus"></i>
                                                    </button></a>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
                                                <th> Name </th>
						<th> Api Key </th>
                                                <th> Api Secret Key </th>
						<th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
					    @foreach($api as $alist)	
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> {{$alist->api_name}} </td>
						<td> {{$alist->api_key}} </td>
                                                
                                                   <td> {{$alist->api_secret}} </td>
                                                
                                                <td>
                                                    <div class="btn-group">
							
                                                        <a href="{{URl::to('admin/editapi/'.$alist->id)}}"><button class="btn btn-xs green dropdown-toggle" type="button" aria-expanded="false"> <i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit
                                                        </button></a>

                                                    </div>
                                                </td>
                                            </tr>
					    @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include("admin_include.company_footer")        
