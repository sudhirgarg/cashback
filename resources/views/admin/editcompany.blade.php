@include("admin_include.company_header")
<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        @include("admin_include.admin_center")
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            @include("admin_include.admin_sidebar")
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Company
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{ action('admin\DashboardController@index') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Edit Company</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_4">
                                        <div class="portlet light bordered form-fit">
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
							<ul>
							    @foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							    @endforeach
							</ul>
						    </div>
						@endif
                                                <form action="{{URL::to('admin/updatecompany/'.$company_detail[0]->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal form-row-seperated">
                                                    <div class="form-body">
							<div class="form-group">
                                                            <label class="control-label col-md-3">Add Company Logo <span style="color:red;">*</span></label>
                                                            <div class="col-md-3">
                                                                <input type="file" name="file" id="file"  />
                                                            </div>
							    <div class="col-md-6">
                                                                <img src="{{URL::asset('store_logos/'.$company_detail[0]->logo)}}" alt="$company_detail[0]->logo)}}" style="width:60px; height:60px;">	
                                                        </div>
							<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Company Name <span style="color:red;">*</span></label>
                                                            <div class="col-md-9">
								<select name="company_name" id="company_name" class="form-control" onchange="select_category(this.value)">
									<option value="">---Select Store---</option>
									@foreach($store_list as $stores)
									<option value="{{$stores->id}}"@if($stores->id == $company_detail[0]->name) {{'selected'}} @endif>{{$stores->name}}</option>
									@endforeach
								</select>
                                                            </div>
                                                        </div>
							<div class="form-group">
                                                            <label class="control-label col-md-3">Select Category <span style="color:red;">*</span></label>
                                                            <div class="col-md-9" id="category">
								<select name="category_name" id="category_name" class="form-control">
									@foreach($store_cats as $cats)
									<option value="{{$cats->id}}"@if($cats->id == $company_detail[0]->cat_id) {{'selected'}} @endif>{{$cats->name}}</option>
									@endforeach
								</select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Company Description <span style="color:red;">*</span></label>
                                                            <div class="col-md-9">
								<textarea name="company_description" id="content_page_detail">{{$company_detail[0]->detail}}</textarea>
                                                            </div>
                                                        </div>
							<div class="form-group">
                                                            <label class="control-label col-md-3">Affiliate Company Name <span style="color:red;">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" placeholder="Affiliate Company Name" name="affiliate_company_name" id="aff_company_name" class="form-control" value="{{$company_detail[0]->affiliate_name}}"/>
                                                            </div>
                                                        </div>
							<div class="form-group">
                                                            <label class="control-label col-md-3">Cashabck <span style="color:red;">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" placeholder="3%" name="cashback" id="cashback" class="form-control" value="{{$company_detail[0]->cashback}}"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Affiliate Link <span style="color:red;">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" placeholder="Affiliate Link" name="affiliate_link" id="affiliate_link" class="form-control" value="{{$company_detail[0]->affiliate_link}}"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-md-9">
                                                                <div class="radio-list">
                                                                    <label>
                                                                        <label class="checkbox-inline">
				                                        <input type="checkbox" name="is_active" id="is_active" value="1" @if($company_detail[0]->is_active == 1) {{"checked"}} @endif>Is Active</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn green">
                                                                     Update</button>
                                                                <a href="{{action('admin\CompanyController@companylist')}}"><button type="button" class="btn default">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include("admin_include.company_footer")
