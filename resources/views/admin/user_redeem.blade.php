@include("admin_include.company_header")
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
	@include("admin_include.admin_center")
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            @include("admin_include.admin_sidebar")
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>User's Payment Request</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{action('admin\DashboardController@index')}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">User's Payment Request</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
				@foreach (['danger', 'warning', 'success', 'info'] as $msg)
      					@if(Session::has('alert-' . $msg))
						<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      					@endif
    				@endforeach
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
                                                <th> Name </th>
                                                <th> Amount </th>
						<th> Account Number </th>
                                                <th> Bic Number </th>
						<th> Ter Name </th>
                                                <th> Requested Date </th>
                                                <th> Status </th>
                                            </tr>
                                        </thead>
                                        <tbody>
					    @foreach($user_redeem as $redeem)	
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> {{ucfirst($redeem->firstname)}} {{ucfirst($redeem->lastname)}} </td>
                                                <td>
                                                    € {{number_format($redeem->amount, 2, ',', '.')}} </a>
                                                </td>
						<td>
                                                    {{$redeem->acc_no}}
                                                </td>
						<td>
                                                    {{$redeem->bic_number}}
                                                </td>
						<td>
                                                    {{$redeem->ter_name}}
                                                </td>
						<td class="center"> {{ date('F d, Y', strtotime($redeem->datetime)) }} </td>
                                                <td>
                                                    <span class="label label-sm"> 
							@if($redeem->status == 1)
								<a href="{{URL::to('admin/updateredeemstatus/'.$redeem->id.'/0')}}">Complete</a>
							@else
								<a href="{{URL::to('admin/updateredeemstatus/'.$redeem->id.'/1')}}">Pending</a>	
							@endif
						    </span>
                                                </td>
                                            </tr>
					    @endforeach	
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include("admin_include.company_footer")        
