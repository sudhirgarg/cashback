<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Dashboard</title>
<link href="{{URL::asset('assets/css/style.css')}}" rel="stylesheet" type="text/css" /> 
<link href="{{URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" /> 
<link href="{{URL::asset('assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" /> 
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'%20rel='stylesheet" type="text/css">
</head>

<body>
<div class="wrapper">
 <div class="header top-bar navbar-fixed-top">
@include("_particles.header")
</div>
  <div class="left nav-left-menu top-fixed">
@include("_particles.leftmenu")
</div>
<div class="right">
@yield("content")
</div>

</div>
</body>

<script src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
 <script src="{{URL::asset('assets/js/bootstrap.min.js')}}"></script>
 <script>$(document).ready(function(){    $(".burger-icon").click(function(){        $("body").toggleClass("open");    });		$(".nav-left-menu").hover(  function () {    $(this).addClass("menu_hover");  },  function () {    $(this).removeClass("menu_hover");  });});$(function() {    var div = $(".top-fixed");    $(window).scroll(function() {            var scroll = $(window).scrollTop();            if (scroll >= 1) {            div.removeClass('top-fixed').addClass("top-scroll");        } else {            div.removeClass("top-scroll").addClass('top-fixed');        }    });});
</script>
</html>