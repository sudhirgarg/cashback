@include("admin_include.company_header")
<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        @include("admin_include.admin_center")
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            @include("admin_include.admin_sidebar")
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>View Complete Detail
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{ action('admin\DashboardController@index') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">View Complete Detail</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_4">

		
					<div class="portlet light bordered form-fit">
                                            
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form class="form-horizontal form-row-seperated" action="#">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Soort claim</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="{{$view_detail[0]->type_claim}}" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Selecteer webshop</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="{{$view_detail[0]->select_shop}}" disabled>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Selecteer bezoek</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="{{$view_detail[0]->select_visit}}" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Klantnummer</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="{{$view_detail[0]->client_number}}" disabled> </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Order -of polisnummer</label>
                                                            <div class="col-md-9">
                                                                 <input type="text" class="form-control" placeholder="small" value="{{$view_detail[0]->order_of}}" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Polis of factuur</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="{{$view_detail[0]->policy}}" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Gebruikt apparaat</label>
                                                            <div class="col-md-9">
                                                                 <input type="text" class="form-control" placeholder="small" value="{{$view_detail[0]->used_equipment}}" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Gebruikte browser</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="{{$view_detail[0]->used_browser}}" disabled> </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Heb je na je click vanaf Cashback4you direct je aankoop gedaan?</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="@if($view_detail[0]->purchase_immediately == 1){{'Yes'}}@else{{'No'}}@endif" disabled> </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Heb je een kortingscode van een andere site gebruikt?</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="@if($view_detail[0]->use_discount_code == 1){{'Yes'}}@else{{'No'}}@endif" disabled> </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Heb je alle cookies geaccepteerd?</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="@if($view_detail[0]->cookies == 1){{'Yes'}}@else{{'No'}}@endif" disabled> </div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3">Stond je browser in incognitomodus of Do Not Track modus?</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="@if($view_detail[0]->browser_in == 1){{'Yes'}}@else{{'No'}}@endif" disabled>
                                                            </div>
                                                        </div>
							<div class="form-group last">
                                                            <label class="control-label col-md-3">Ben je na je iDeal betaling teruggegaan naar de bedanktpagina?</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="@if($view_detail[0]->ideal_payment == 1){{'Yes'}}@else{{'No'}}@endif" disabled>
                                                            </div>
                                                        </div>
							<div class="form-group last">
                                                            <label class="control-label col-md-3">Heb je rekening gehouden met de voorwaarden van de webshop?</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="small" value="@if($view_detail[0]->condition_of_shop == 1){{'Yes'}}@else{{'No'}}@endif" disabled>
                                                            </div>
                                                        </div>
							<div class="form-group last">
                                                            <label class="control-label col-md-3">Toelichting claim</label>
                                                            <div class="col-md-9">
                                                                <textarea class="form-control" disabled>{{$view_detail[0]->description}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include("admin_include.company_footer")
