@include("admin_include.company_header")
<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        @include("admin_include.admin_center")
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            @include("admin_include.admin_sidebar")
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Edit Cashback % Distribution
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{ action('admin\DashboardController@index') }}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Edit Cashback % Distribution</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_4">

					@foreach (['danger', 'warning', 'success', 'info'] as $msg)
      					@if(Session::has('alert-' . $msg))
						<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      					@endif
    				@endforeach
					@if (count($errors) > 0)
						    <div class="alert alert-danger">
							<ul>
							    @foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							    @endforeach
							</ul>
						    </div>
						@endif
                                        <div class="portlet light bordered form-fit">
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
						
                                                <form action="{{ URL::to('admin/updatecashbackper') }}" method="post" class="form-horizontal form-row-seperated">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">inviter % <span style="color:red;">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="number" step="0.1" placeholder="0" onblur="check_total()" name="inviter_percentage" id="inviter_percentage" class="form-control" value="{{$cashback_distribution_detail[0]->inviter_percentage}}"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">User % <span style="color:red;">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="number" step="0.1" placeholder="0" onblur="check_total()" name="user_percentage" id="user_percentage" class="form-control" value="{{$cashback_distribution_detail[0]->user_percentage}}"/>
                                                            </div>
                                                        </div>
							<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">cashback4you % <span style="color:red;">*</span></label>
                                                            <div class="col-md-9">
                                                                <input type="number" step="0.1" placeholder="0" onblur="check_total()" name="cashback4you_percentage" id="cashback4you_percentage" class="form-control" value="{{$cashback_distribution_detail[0]->cashback4you_percentage}}"/>
                                                            </div>
                                                        </div>
							<div class="form-group">
                                                            <label class="control-label col-md-12" id="msg"></label>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn green">
                                                                     Update</button>
                                                                <a href="{{action('admin\DashboardController@index')}}"><button type="button" class="btn default">Cancel</button></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
@include("admin_include.company_footer")
