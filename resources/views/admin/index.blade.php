@include("admin_include.header")
<body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="{{ action('admin\IndexController@index') }}">
		<img src="{{URL::asset('admin_css/assets/pages/img/logo-big.png')}}" alt="CashBack"/></a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
	@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
      					@if(Session::has('alert-' . $msg))
						<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      					@endif
    				@endforeach
            <!-- BEGIN LOGIN FORM -->
	    <!--{{ Form::open(array('url' => 'index.php/admin/login', 'method' => 'post', 'class' => 'login-form')) }}-->	
            <form class="login-form" action="{{ action('admin\IndexController@postLogin') }}" method="post">
                <h3 class="form-title font-green">Sign In</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Email/Password is not correct. Please try again ! </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" /> </div>
		<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase">Login</button>
                    <!--<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>--->
                </div>
               
                
            </form>
	   <!-- {{Form::close()}}-->	
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="index.html" method="post">
                <h3 class="font-green">Forget Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn btn-default">Back</button>
                    <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
                    </div>
@include("admin_include.footer")        
