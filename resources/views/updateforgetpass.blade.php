@include('front_include.front_header')
@include('front_include.front_menu')
    <div class="container-fluid banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(!empty($banner[0]))	
                    @foreach($banner as $img)
                    <img src="{{URL::asset('banner_images/'.$img->img)}}">
		    @endforeach
		    @else	
		    <img src="{{URL::asset('banner_images/banner-1.jpg')}}">	
		    @endif
                    <!--<div class="banner-box">
                        <div class="banner-text">
                            <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                            <form name="search" action="{{action('SearchController@index')}}" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
			    </form>
                        </div>		
                    </div>-->
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
    	<div class="row">
        	<div class="col-md-12 bread">
            	<ol class="breadcrumb nopadding">
                  <li class="breadcrumb-item"><a href="{{action('IndexController@index')}}">Home</a></li>
                  <li class="breadcrumb-item active">Create New Password</li>
                </ol>
            </div>
        </div>
    </div>
    
    <div class="container login-page">
    	<div class="row">
        	<div class="col-md-12">
            	<div class="niews_title">
            		<h1>Create New Password</h1>
                </div>
            </div>
            <div class="col-md-12">
            	<div class="login-form m-t-2">
                    <div class="col-md-5 col-sm-5 col-xs-12 nopadding">
			@if (count($errors) > 0)
						    <div class="alert alert-danger">
							<ul>
							    @foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							    @endforeach
							</ul>
						    </div>
						@endif
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
      					@if(Session::has('alert-' . $msg))
						<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      					@endif
    				@endforeach
                        <form action="{{action('IndexController@changeforgetpass')}}" method="post" class="form-horizontal login-page-form">
                        	<div class="form-uitbatel">
                                <input type="hidden" name="code" value="{{$code}}"/>
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                                <div class="form-group">
                                    <label class="control-label col-sm-6 col-sm-6 col-xs-12" for="email">New Password:</label>
                                    <div class="col-sm-6 col-sm-6 col-xs-12 nopadding">
                                        <input id="password" class="form-control" type="password" placeholder="*******" name="password">
                                    </div>
                                </div>
				<div class="form-group">
                                    <label class="control-label col-sm-6 col-sm-6 col-xs-12" for="email">Confirm Password:</label>
                                    <div class="col-sm-6 col-sm-6 col-xs-12 nopadding">
                                        <input id="confirm_password" class="form-control" type="password" placeholder="*******" name="confirm_password">
                                    </div>
                                </div>
                            </div>
                            <div class="transactie-btn">
                           		<button class="btn btn-default login-green-btn" type="submit">Create</button>
                            </div>
                        </form>
                    </div>
            	</div>
            </div>
        </div>
    </div>
    
  
    
        
    <!--End Content section-->   	
@include('front_include.front_footer')
