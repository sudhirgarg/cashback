<?php echo $__env->make('front_include.front_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('front_include.front_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="container-fluid banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if(!empty($banner[0])): ?>	
                    <?php foreach($banner as $img): ?>
                    <img src="<?php echo e(URL::asset('banner_images/'.$img->img)); ?>">
		    <?php endforeach; ?>
		    <?php else: ?>	
		    <img src="<?php echo e(URL::asset('banner_images/banner-1.jpg')); ?>">	
		    <?php endif; ?>
                    <!--<div class="banner-box">
                        <div class="banner-text">
                            <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                            <form name="search" action="<?php echo e(action('SearchController@index')); ?>" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
			    </form>
                        </div>		
                    </div>-->
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
    	<div class="row">
        	<div class="col-md-12 bread">
            	<ol class="breadcrumb nopadding">
                  <li class="breadcrumb-item"><a href="<?php echo e(action('IndexController@index')); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Create New Password</li>
                </ol>
            </div>
        </div>
    </div>
    
    <div class="container login-page">
    	<div class="row">
        	<div class="col-md-12">
            	<div class="niews_title">
            		<h1>Create New Password</h1>
                </div>
            </div>
            <div class="col-md-12">
            	<div class="login-form m-t-2">
                    <div class="col-md-5 col-sm-5 col-xs-12 nopadding">
			<?php if(count($errors) > 0): ?>
						    <div class="alert alert-danger">
							<ul>
							    <?php foreach($errors->all() as $error): ?>
								<li><?php echo e($error); ?></li>
							    <?php endforeach; ?>
							</ul>
						    </div>
						<?php endif; ?>
			<?php foreach(['danger', 'warning', 'success', 'info'] as $msg): ?>
      					<?php if(Session::has('alert-' . $msg)): ?>
						<p class="alert alert-<?php echo e($msg); ?>"><?php echo e(Session::get('alert-' . $msg)); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      					<?php endif; ?>
    				<?php endforeach; ?>
                        <form action="<?php echo e(action('IndexController@changeforgetpass')); ?>" method="post" class="form-horizontal login-page-form">
                        	<div class="form-uitbatel">
                                <input type="hidden" name="code" value="<?php echo e($code); ?>"/>
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
                                <div class="form-group">
                                    <label class="control-label col-sm-6 col-sm-6 col-xs-12" for="email">New Password:</label>
                                    <div class="col-sm-6 col-sm-6 col-xs-12 nopadding">
                                        <input id="password" class="form-control" type="password" placeholder="*******" name="password">
                                    </div>
                                </div>
				<div class="form-group">
                                    <label class="control-label col-sm-6 col-sm-6 col-xs-12" for="email">Confirm Password:</label>
                                    <div class="col-sm-6 col-sm-6 col-xs-12 nopadding">
                                        <input id="confirm_password" class="form-control" type="password" placeholder="*******" name="confirm_password">
                                    </div>
                                </div>
                            </div>
                            <div class="transactie-btn">
                           		<button class="btn btn-default login-green-btn" type="submit">Create</button>
                            </div>
                        </form>
                    </div>
            	</div>
            </div>
        </div>
    </div>
    
  
    
        
    <!--End Content section-->   	
<?php echo $__env->make('front_include.front_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
