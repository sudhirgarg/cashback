<?php echo $__env->make("admin_include.company_header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <?php echo $__env->make("admin_include.admin_center", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php echo $__env->make("admin_include.admin_sidebar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Add Content Page
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo e(action('admin\DashboardController@index')); ?>">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Add Content Page</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_4">
                                        <div class="portlet light bordered form-fit">
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
						<?php if(count($errors) > 0): ?>
						    <div class="alert alert-danger">
							<ul>
							    <?php foreach($errors->all() as $error): ?>
								<li><?php echo e($error); ?></li>
							    <?php endforeach; ?>
							</ul>
						    </div>
						<?php endif; ?>
                                                <form action="<?php echo e(action('admin\ContentController@add_content')); ?>" method="post" class="form-horizontal form-row-seperated">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Name<span style="color:red;"> *</span></label>
                                                            <div class="col-md-9">
                                                                <input type="text" placeholder="Content Name" name="page_name" id="page_name" class="form-control" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Content Page Detail<span style="color:red;"> *</span></label>
                                                            <div class="col-md-9">
								<textarea name="content_page_detail" id="content_page_detail"></textarea>
                                                            </div>
                                                        </div>
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"></label>
                                                            <div class="col-md-9">
                                                                <div class="radio-list">
                                                                    <label>
                                                                        <label class="checkbox-inline">
				                                        <input type="checkbox" name="is_active" id="is_active" value="1" checked>Is Active</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn green">
                                                                     Save</button>
                                                                <a href="<?php echo e(action('admin\ContentController@contentpagelist')); ?>"><button type="button" class="btn default">Cancel</button></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
<?php echo $__env->make("admin_include.company_footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
