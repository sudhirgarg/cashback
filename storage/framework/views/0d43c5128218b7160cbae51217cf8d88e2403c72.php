<?php echo $__env->make("admin_include.company_header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
	<?php echo $__env->make("admin_include.admin_center", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php echo $__env->make("admin_include.admin_sidebar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Users List</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo e(action('DashboardController@index')); ?>">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Users List</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
                                                <th> Name </th>
                                                <th> Email </th>
                                                <th> Status </th>
                                                <th> Joined </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
					    <?php foreach($users as $users): ?>	
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> <?php echo e(ucfirst($users->firstname)); ?> <?php echo e(ucfirst($users->lastname)); ?> </td>
                                                <td>
                                                    <a href="mailto:shuxer@gmail.com"> <?php echo e($users->username); ?> </a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm"> 
							<?php if($users->is_active == 1): ?>
								<a href="<?php echo e(URL::to('updateuserstatus/'.$users->id.'/0')); ?>">Active</a>
							<?php else: ?>
								<a href="<?php echo e(URL::to('updateuserstatus/'.$users->id.'/1')); ?>">Deactive</a>	
							<?php endif; ?>
						    </span>
                                                </td>
                                                <td class="center"> <?php echo e(date('F d, Y', strtotime($users->date_add))); ?> </td>
                                                <td>
						    <div class="btn-group">
							<a href="<?php echo e(URL::to('deleteuser/'.$users->id)); ?>"><button class="btn btn-xs green dropdown-toggle" type="button" aria-expanded="false"> Delete
                                                        </button></a>
                                                    </div>
						    <div class="btn-group">
							<a href="<?php echo e(URL::to('usertransaction/'.$users->id)); ?>"><button class="btn btn-xs green dropdown-toggle" type="button" aria-expanded="false"> Transaction History
                                                        </button></a>
                                                    </div>		
                                                </td>
                                            </tr>
					    <?php endforeach; ?>	
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
<?php echo $__env->make("admin_include.company_footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>        
