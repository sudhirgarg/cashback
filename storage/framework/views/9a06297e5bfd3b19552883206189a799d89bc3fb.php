<div class="container-fluid meld-form">
    <div class="container">
        <div class="col-md-8 col-md-offset-3">
            <?php if(count($errors) > 0): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach($errors->all() as $error): ?>
                    <li><?php echo e($error); ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>
            <?php foreach(['danger', 'warning', 'success', 'info'] as $msg): ?>
            <?php if(Session::has('alert-' . $msg)): ?>
            <p class="alert alert-<?php echo e($msg); ?>"><?php echo e(Session::get('alert-' . $msg)); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            <?php endif; ?>
            <?php endforeach; ?>
            <h2 class="col-md-9">MELD JE AAN!</h2>

            <form action="<?php echo e(action('IndexController@sign')); ?>" method="post" class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Voornaam:</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="text" name="firstname" value="<?php echo e(old('firstname')); ?>" placeholder="Voornaam">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Tussenvoegsels:</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="text" name="inserts" value="<?php echo e(old('inserts')); ?>" placeholder="Tussenvoegsels">
                    </div>
                </div>	
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Achternaam:</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="txt" value="<?php echo e(old('lastname')); ?>" name="lastname" placeholder="Achternaam">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email">E-mail:</label>
                    <div class="col-sm-5">
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo e(old('email')); ?>" placeholder="voorbeeld@gmail.com">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="pwd">Wachtwoord:</label>
                    <div class="col-sm-5">          
                        <input type="password" class="form-control" id="pwd" name="password" placeholder="**********">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Bevestig wachtwoord:</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" id="txt" name="confirm_password" placeholder="**********">
                    </div>
                </div>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
                <div class="form-group m-t-2">        
                    <div class="col-sm-10">
                        <div class="checkbox">
                            <label><input type="checkbox" name="terms" value="1" checked> ik ga akkoord met de voorwaarden</label>
                        </div>
                    </div>
                </div>
                <div class="form-group m-t-4  submit-btn">        
                    <div class="col-md-4 col-sm-4 col-xs-12 nopadding">
                        <button type="submit" class="btn btn-default submit-green-btn">aanmelden</button>
                    </div>
                    <?php if (!Auth::check()) : ?>
                    <div class="col-md-4 col-sm-4 col-xs-12 fb-btn">
                        <a href="redirect" class="btn btn-default blue-btn">aanmelden via facebook</a>
                    </div>
                    <?php endif; ?>
                </div>

            </form>
        </div>
    </div>
</div>
