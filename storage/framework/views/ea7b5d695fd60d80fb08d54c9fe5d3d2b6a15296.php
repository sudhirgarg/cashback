<?php echo $__env->make("admin_include.header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?php echo e(action('IndexController@index')); ?>">
		<img src="<?php echo e(URL::asset('admin_css/assets/pages/img/logo-big.png')); ?>" alt="CashBack"/></a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
	<?php foreach($errors->all() as $error): ?>
		<li><?php echo e($error); ?></li>
	<?php endforeach; ?>
            <!-- BEGIN LOGIN FORM -->
	    <?php echo e(Form::open(array('url' => 'index.php/login', 'method' => 'post', 'class' => 'login-form'))); ?>	
            <!--<form class="login-form" action="<?php echo e(action('IndexController@postLogin')); ?>" method="post">-->
                <h3 class="form-title font-green">Sign In</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase">Login</button>
                    <!--<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>--->
                </div>
               
                
            <!--</form>-->
	    <?php echo e(Form::close()); ?>	
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="index.html" method="post">
                <h3 class="font-green">Forget Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn btn-default">Back</button>
                    <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
                    </div>
<?php echo $__env->make("admin_include.footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>        
