<?php echo $__env->make('front_include.front_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('front_include.front_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container-fluid banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if(!empty($banner[0])): ?>	
                <?php foreach($banner as $img): ?>
                <img src="<?php echo e(URL::asset('banner_images/'.$img->img)); ?>">
                <?php endforeach; ?>
                <?php else: ?>	
                <img src="<?php echo e(URL::asset('banner_images/banner-1.jpg')); ?>">	
                <?php endif; ?>
                <div class="banner-box">
                    <div class="banner-text">
                        <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                        <form name="search" action="<?php echo e(action('SearchController@index')); ?>" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
                        </form>	
                    </div>		
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container four-section">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="boxes">
                <img src="<?php echo e(URL::asset('front_img/img1.png')); ?>">
                <div class="box">
                    <h3>1. Login op je eigen gratis account</h3>
                </div>
            </div>
        </div>  
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="boxes">
                <img src="<?php echo e(URL::asset('front_img/img2.png')); ?>">
                <div class="box">
                    <h3>2. Zoek je webshop en klik door</h3>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="boxes">
                <img src="<?php echo e(URL::asset('front_img/img3.png')); ?>">
                <div class="box">
                    <h3>3.  Plaats je bestelling op de webshop</h3>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="boxes">
                <img src="<?php echo e(URL::asset('front_img/img4.png')); ?>">
                <div class="box">
                    <h3>4.  Krijg geld terug via cashback4you</h3>
                </div>
            </div>
        </div>      
    </div>
</div>

<div class="container-fluid more-info-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>VERDIEN GELD MET HET AANBRENGEN VAN LEDEN! <br>ONTVANG 10% CASHBACK VAN AANGEBRACHtE LEDEN</h1>
                <a href="<?php echo e(action('IndexController@signup')); ?>"><span class="btn btn-default more-green-btn">MELD JE NU AAN!</span></a>
                <span id="moreinfo" class="btn btn-default more-gray-btn">MEER INFO</span>
            </div>
        </div>    
    </div>
</div>

<div class="container-fluid slider-border">
    <div class="slider-section">
        <div class="container">
            <div class="col-md-12">


                <div class="well">
                    <div id="myCarousel" class="carousel slide">

                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="row">
                                    <?php
                                    $a = 0;
                                    $st = '0'
                                    ?>
                                    <?php foreach($links as $aff_link): ?>
                                    <div class="col-sm-3 col-xs-3 first text-center">
                                        <?php if(!empty($email)): ?>
                                        <a href="<?php echo e($aff_link->affiliate_link); ?>" target="_blank"><?php else: ?><a href="#" onclick="please_login()">
                                                <?php endif; ?>
                                                <?php if (filter_var($aff_link->logo, FILTER_VALIDATE_URL)) { ?>
                                                <img src="<?php echo $aff_link->logo?>"  alt="<?php echo e($aff_link->logo); ?>" class="img-responsive" />
                                                <?php } else { ?>
                                                    <img src="<?php echo e(URl::asset('store_logos/'.$aff_link->logo)); ?>" alt="<?php echo e($aff_link->logo); ?>" class="img-responsive">
                                                <?php } ?>
                                            </a>
                                            <?php if(!empty($email)): ?><a href="<?php echo e($aff_link->affiliate_link); ?>" style="text-decoration:none;" target="_blank"><?php else: ?><a href="#" onclick="please_login()" style="text-decoration:none;"><?php endif; ?><h3><?php if($a%2==0): ?><?php echo e('cashback'); ?><?php else: ?><?php echo e('Ontvang'); ?><?php endif; ?></h3></a>
                                                <?php if(!empty($email)): ?><a href="<?php echo e($aff_link->affiliate_link); ?>" style="text-decoration:none;" target="_blank"><?php else: ?><a href="#" onclick="please_login()" style="text-decoration:none;"><?php endif; ?><h1><?php echo e($aff_link->cashback); ?></h1></a>
                                                    <p><?php echo e($aff_link->affiliate_name); ?></p>
                                                    </div>
                                                    <?php
                                                    $a++;
                                                    $st++;
                                                    ?>   
                                                    <?php if($a==4 && $st!='12'): ?>
                                                    </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="row">
                                                            <?php $a = '0'; ?>
                                                            <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </div><!--/row-->
                                                    </div>
                                                    </div><!--/carousel-inner--> 
                                                    </div>
                                                    <!--/myCarousel-->
                                                    </div>
                                                    <!--/well-->
                                                    </div>
                                                    </div>
                                                    <a class="left l-arrow carousel-control" href="#myCarousel" data-slide="prev"><img src="<?php echo e(URl::asset('front_img/left-icon.png')); ?>"></a>
                                                    <a class="right r-arrow carousel-control" href="#myCarousel" data-slide="next"><img src="<?php echo e(URL::asset('front_img/right-icon.png')); ?>"></a>
                                                    </div>
                                                    </div>

                                                    <div id="myModal" class="modal-pop">

                                                        <!-- Modal content -->
                                                        <div class="modal-content-pop">
                                                            <span class="close-pop">×</span>
                                                            <?php echo $moreinfo[0]->page; ?>

                                                        </div>

                                                    </div>

                                                    <?php echo $__env->make('front_include.front_contact', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                    <?php echo $__env->make('front_include.front_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
