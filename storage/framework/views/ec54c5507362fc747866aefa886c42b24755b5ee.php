<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Cashback4you | <?php if(Request::segment(2) == 'users'  || Request::segment(2) == 'usertransaction'): ?> Users <?php endif; ?>
			<?php if(Request::segment(2) == 'addbanner'  || Request::segment(2) == 'bannerlist'): ?> Banners <?php endif; ?>
			<?php if(Request::segment(2) == 'addstore'  || Request::segment(2) == 'storelist'  || Request::segment(2) == 'editstore'  || Request::segment(2) == 'updatestorecat_storelist'): ?> Stores <?php endif; ?>
			<?php if(Request::segment(2) == 'company'  || Request::segment(2) == 'companylist'  || Request::segment(2) == 'editcompany'): ?> Company <?php endif; ?>
			<?php if(Request::segment(2) == 'addcontent'  || Request::segment(2) == 'contentlist'  || Request::segment(2) == 'edit_content'): ?> Content's <?php endif; ?>
			<?php if(Request::segment(2) == 'addcategory'  || Request::segment(2) == 'categorylist'  || Request::segment(2) == 'editcat'): ?> Category <?php endif; ?>
			<?php if(Request::segment(2) == 'addfaq'  || Request::segment(2) == 'faqlist'  || Request::segment(2) == 'editfaq'): ?> FAQ's <?php endif; ?>
			<?php if(Request::segment(2) == 'addapi'  || Request::segment(2) == 'apilist'  || Request::segment(2) == 'editapi'): ?> API's <?php endif; ?>
			<?php if(Request::segment(2) == 'addmail'  || Request::segment(2) == 'maillist'  || Request::segment(2) == 'editmail'): ?> Email Setting <?php endif; ?>
			<?php if(Request::segment(2) == 'addnews'  || Request::segment(2) == 'newslist'  || Request::segment(2) == 'editnews'): ?> News <?php endif; ?>
			<?php if(Request::segment(2) == 'transactions'): ?> Transactions History <?php endif; ?>
			<?php if(Request::segment(2) == 'requestpayment'): ?> Payment Request <?php endif; ?>
			<?php if(Request::segment(2) == 'claimfiled' || Request::segment(2) == 'viewclaimdetail'): ?> Cashback Filed Claims <?php endif; ?>
			<?php if(Request::segment(2) == 'transfer' || Request::segment(2) == 'transfercashlist'): ?> Cashback Transfers <?php endif; ?>	
	</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(URL::asset('admin_css/assets/global/plugins/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(URL::asset('admin_css/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(URL::asset('admin_css/assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(URL::asset('admin_css/assets/global/plugins/uniform/css/uniform.default.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(URL::asset('admin_css/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo e(URL::asset('company_css/assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(URL::asset('company_css/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo e(URL::asset('admin_css/assets/global/css/components-rounded.min.css')); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo e(URL::asset('admin_css/assets/global/css/plugins.min.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo e(URL::asset('admin_css/assets/layouts/layout4/css/layout.min.css')); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo e(URL::asset('admin_css/assets/layouts/layout4/css/themes/light.min.css')); ?>" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo e(URL::asset('admin_css/assets/layouts/layout4/css/custom.min.css')); ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
