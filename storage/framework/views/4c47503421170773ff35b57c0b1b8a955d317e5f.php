<?php echo $__env->make("admin_include.company_header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
	<?php echo $__env->make("admin_include.admin_center", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php echo $__env->make("admin_include.admin_sidebar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Filed Claim List</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo e(action('admin\DashboardController@index')); ?>">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Filed Claim List</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
				<?php foreach(['danger', 'warning', 'success', 'info'] as $msg): ?>
      					<?php if(Session::has('alert-' . $msg)): ?>
						<p class="alert alert-<?php echo e($msg); ?>"><?php echo e(Session::get('alert-' . $msg)); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      					<?php endif; ?>
    				<?php endforeach; ?>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
                                                <th> User Name </th>
                                                <th> Claim Type </th>
						<th> Shop </th>
                                                <th> Client Number </th>
						<th> Order Of </th>
                                                <th> Date of File Claim </th>
						<th> Status </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
					    <?php foreach($user_claimfiled as $user_claimfiled): ?>	
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> <?php echo e(ucfirst($user_claimfiled->firstname)); ?> <?php echo e(ucfirst($user_claimfiled->lastname)); ?> </td>
                                                <td>
                                                    <?php echo e($user_claimfiled->type_claim); ?>

                                                </td>
						<td>
                                                    <?php echo e($user_claimfiled->select_shop); ?>

                                                </td>
						<td>
                                                    <?php echo e($user_claimfiled->client_number); ?>

                                                </td>
						<td>
                                                    <?php echo e($user_claimfiled->order_of); ?>

                                                </td>
						<td class="center"> <?php echo e(date('F d, Y', strtotime($user_claimfiled->date_add))); ?> </td>
                                                <td>
                                                    <span class="label label-sm"> 
							<?php if($user_claimfiled->status == 1): ?>
								<a href="<?php echo e(URL::to('admin/updateclaimfilestatus/'.$user_claimfiled->id.'/0')); ?>">Already Read</a>
							<?php else: ?>
								<a href="<?php echo e(URL::to('admin/updateclaimfilestatus/'.$user_claimfiled->id.'/1')); ?>">Not Read Yet</a>	
							<?php endif; ?>
						    </span>
                                                </td>
                                                <td>
						    <div class="btn-group">
							<a href="<?php echo e(URL::to('admin/viewclaimdetail/'.$user_claimfiled->id)); ?>"><button class="btn btn-xs green dropdown-toggle" type="button" aria-expanded="false"> View Complete Detail
                                                        </button></a>
                                                    </div>		
                                                </td>
                                            </tr>
					    <?php endforeach; ?>	
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
<?php echo $__env->make("admin_include.company_footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>        
