<body>
    <div class="container header">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 logo">
                <a href="<?php echo e(action('IndexController@index')); ?>"><img src="<?php echo e(URL::asset('front_img/logo.png')); ?>"></a>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 right-head">
                <div class="right-btn">
                    <a href="<?php echo e(URL::to('content/howitwork')); ?>"><button type="button" class="btn work-btn" style="margin-right:10px;">Hoe werkt het?</button></a>
                    <?php if(!empty($email)): ?>	

                    <?php echo e($email); ?>  	<a href="<?php echo e(action('IndexController@logout')); ?>"><button type="button" class="btn login green-btn">UITLOGGEN</button></a>
                    <a href="<?php echo e(action('FrontDashboardController@index')); ?>"><button type="button" class="btn login green-btn">MIJN PROFIEL</button></a>
                    <?php else: ?>
                    <a href="<?php echo e(action('IndexController@login')); ?>"><button type="button" class="btn login green-btn">login</button></a>

                    <a href="<?php echo e(action('IndexController@signup')); ?>"><button type="button" class="btn login green-btn">AANMELDEN</button></a>	
                    <?php endif; ?>

                </div>
            </div>
       	</div>
    </div>
    <div class="container-fluid menu m-t-2">
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <div class="col-md-9 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-2">
                        <ul class="nav navbar-nav">
                            <?php foreach($cat_lists as $cats): ?>	                        
                            <li <?php if(Request::segment(2) == $cats->slug): ?>class="active"<?php endif; ?>><a href="<?php echo e(URL::to(strtolower($cats->slug))); ?>"><?php echo e($cats->name); ?></a></li>
                            <?php endforeach; ?>	
                        </ul>
                    </div> 
                </div>
            </div>
        </nav>
    </div>