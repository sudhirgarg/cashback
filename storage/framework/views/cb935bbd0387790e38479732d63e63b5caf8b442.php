<?php echo $__env->make('front_include.front_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('front_include.front_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
function month($monthName){
	$month=array('January'=>'januari',
		     'February'=>'februari',
		     'March'=>'maart',
		     'April'=>'april',
		     'May'=>'mei',
		     'June'=>'juni', 		
		     'July'=>'juli',
		     'August'=>'augustus',
		     'September'=>'september',
		     'October'=>'oktober',
		     'November'=>'november',
		     'December'=>'december'		
	);
	
	foreach($month as $key=>$value):
		$monthName=str_replace($key,$value,$monthName);	
	endforeach;	

	return $monthName;

}
?>
    <div class="container-fluid banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if(!empty($banner[0])): ?>	
                    	<?php foreach($banner as $img): ?>
                    		<img src="<?php echo e(URL::asset('banner_images/'.$img->img)); ?>">
		    	<?php endforeach; ?>
		    <?php else: ?>	
		    	<img src="<?php echo e(URL::asset('banner_images/banner-1.jpg')); ?>">	
		    <?php endif; ?>
                    <div class="banner-box">
                        <div class="banner-text">
                            <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                            <form name="search" action="<?php echo e(action('SearchController@index')); ?>" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
			    </form>
                        </div>		
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
    	<div class="row">
        	<div class="col-md-12 bread">
            	<ol class="breadcrumb nopadding">
                  <li class="breadcrumb-item"><a href="<?php echo e(action('IndexController@index')); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Account</li>
                </ol>
            </div>
            <div class="col-md-12 mail-address">
            	<h1><?php echo e($email); ?></h1>
            </div>
            <div class="col-md-12 date">
           	<p>Lid sinds <!--1 juli 2016--><?php echo e(date('d', strtotime($user_detail[0]->date_add))); ?> <?php $month=date('F', strtotime($user_detail[0]->date_add)); echo month($month) ?> <?php echo e(date('Y', strtotime($user_detail[0]->date_add))); ?></p>
            </div>
        </div>
	<?php foreach(['danger', 'warning', 'success', 'info'] as $msg): ?>
      					<?php if(Session::has('alert-' . $msg)): ?>
						<p class="alert alert-<?php echo e($msg); ?>"><?php echo e(Session::get('alert-' . $msg)); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      					<?php endif; ?>
    				<?php endforeach; ?>
    </div>
    
    <div class="container-fluid page-tab-section">
        <div class="container m-t-2">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-section">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#section2">Bonus transacties</a></li>
                    </ul>
                    </div>
                </div>
                
                <div class="col-md-12 m-t-2">
                    <div class="tab-content">
                        <div id="section2" class="tab-pane fade m-t-1 in active">
                          	<h2>Bonus transacties</h2>
                          	<div class="col-md-12 nopadding status-transact">
                          		<div class="col-md-2 col-sm-3 col-xs-12">
                                	<h2>STATUS UITLEG</h2>
                                </div>
                          		<div class="col-md-3 col-sm-3 col-xs-12 leg">
                                	<h5>Bevestigd</h5>
                                    <p>Jouw aankoop is geregistreerd door de webshop en door ons.</p>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 leg">
                                	<h5>Goedgekeurd</h5>
                                    <p>De webshop heeft jouw transactie en de cashback goedgekeurd.</p>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 leg">
                                	<h5>Afgekeurd</h5>
                                    <p>De webshop heeft de cashback afgekeurd.</p>
                                </div>
                          	</div>
                          
                          	<div class="transact-table table-responsive m-t-3">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Webshop</th>
                                            <th>Datum</th>
                                            <th>CashbacK</th>
                                            <th>Status</th>
                                            <th>DEEL JOUW WINST OP FACBOOK</th>
                                        </tr>
                                    </thead>
                                    <tbody>
					<?php foreach($allbonustrans as $detail_tran): ?>
                                        <tr>
                                            <td><!--<?php echo e($detail_tran->store_id); ?>-->#Transactie 1 </td>
                                            <td><?php echo date("d-m-Y", strtotime($detail_tran->datetime)); ?></td>
                                            <td>€12,50<!--<?php echo e(number_format($detail_tran->amount, 2, ',', '.')); ?>--> </td>
                                            <td><strong><?php if($detail_tran->status == 1): ?><?php echo e('Goedgekeurd'); ?><?php else: ?><?php echo e('Rejected'); ?><?php endif; ?></strong></td>
                                            <td><a class="scmFacebook" href='https://www.facebook.com/dialog/share?app_id=1808259536097718&display=popup&href=http://139.59.3.57/cashback/public/&summary=test123&title=cashback&nbsp;received=€12,50&redirect_uri=http://139.59.3.57/cashback/public/account'><strong><?php if($detail_tran->type == 'referral' || $detail_tran->type == 'api' || $detail_tran->type == 'Api'): ?><?php echo e('Delen'); ?><?php else: ?> <?php echo e(ucfirst($detail_tran->type)); ?><?php endif; ?></strong></a></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
				<?php if(!empty($bonustrans)): ?>	
				<div class="pagination-section niews-pagi">
					<nav aria-label="Page navigation">
						<ul class="pagination">
					    		<?php echo $allbonustrans1; ?>	
					  	</ul>
					</nav>
				</div>
				<?php endif; ?>
                            </div> 
                        </div>
                   </div>
                </div>
            </div>
        </div>
    </div><!--Containerfluid-->   
    
    <div class="container-fluid slider-border">
    	<div class="slider-section">
    		<div class="container">
    			<div class="col-md-12 nopadding-slide">
                	<h1 class="text-left slide-heading">INTERESSANTE cASHBACKS VOOR JOU</h1>
        			<div class="well">
            			<div id="myCarousel" class="carousel slide">
                
                            <!-- Carousel items -->
                            <!-- Carousel items -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="row">
                                        <?php $a=0; $st='0' ?>
						<?php foreach($links as $aff_link): ?>


                            				<div class="col-sm-3 col-xs-3 first text-center">
                            					<?php if(!empty($email)): ?><a href="<?php echo e(URL::to('link_click/'.$aff_link->id)); ?>"><?php else: ?><a href="#" onclick="please_login()"><?php endif; ?><img src="<?php echo e(URl::asset('store_logos/'.$aff_link->logo)); ?>" alt="<?php echo e($aff_link->logo); ?>" class="img-responsive"></a>
                                				<?php if(!empty($email)): ?><a href="<?php echo e(URL::to('link_click/'.$aff_link->id)); ?>" style="text-decoration:none;"><?php else: ?><a href="#" onclick="please_login()" style="text-decoration:none;"><?php endif; ?><h3><?php if($a%2==0): ?><?php echo e('cashback'); ?><?php else: ?><?php echo e('Ontvang'); ?><?php endif; ?></h3></a>
                                				<?php if(!empty($email)): ?><a href="<?php echo e(URL::to('link_click/'.$aff_link->id)); ?>" style="text-decoration:none;"><?php else: ?><a href="#" onclick="please_login()" style="text-decoration:none;"><?php endif; ?><h1><?php echo e($aff_link->cashback); ?></h1></a>
                                				<p><?php echo e($aff_link->affiliate_name); ?></p>
                            				</div>
					<?php $a++; $st++; ?>   
					<?php if($a==4 && $st!='12'): ?>
						</div>
					    </div>
					<div class="item">
						<div class="row">
						<?php $a='0'; ?>
						<?php endif; ?>
						<?php endforeach; ?>
                               			</div><!--/row-->
                                </div>
                            </div><!--/carousel-inner--> 
                      	</div>
                        <!--/myCarousel-->
                     </div>
                    <!--/well-->
  				</div>
        	</div>
            <a class="left l-arrow carousel-control" href="#myCarousel" data-slide="prev"><img src="<?php echo e(URL::asset('front_img/left-icon.png')); ?>"></a>
            <a class="right r-arrow carousel-control" href="#myCarousel" data-slide="next"><img src="<?php echo e(URL::asset('front_img/right-icon.png')); ?>"></a>
     	</div>
    </div>
<?php echo $__env->make('front_include.front_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
