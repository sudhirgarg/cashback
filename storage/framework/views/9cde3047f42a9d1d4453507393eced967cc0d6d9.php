<?php echo $__env->make("admin_include.company_header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
	<?php echo $__env->make("admin_include.admin_center", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php echo $__env->make("admin_include.admin_sidebar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Content's Pages</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo e(action('DashboardController@index')); ?>">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Content's Pages</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
				<?php foreach(['danger', 'warning', 'success', 'info'] as $msg): ?>
      					<?php if(Session::has('alert-' . $msg)): ?>
						<p class="alert alert-<?php echo e($msg); ?>"><?php echo e(Session::get('alert-' . $msg)); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      					<?php endif; ?>
    				<?php endforeach; ?>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="btn-group pull-right">
                                                    <a href="<?php echo e(action('ContentController@index')); ?>"><button id="sample_editable_1_new" class="btn sbold green"> Add New Content Page
                                                        <i class="fa fa-plus"></i>
                                                    </button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
                                                <th> Name </th>
                                                <th> Status </th>
						<th> Position </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
					    <?php foreach($content as $list): ?>	
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> <?php echo e($list->page_name); ?> </td>
                                                <td>
                                                    <span class="label label-sm">
						    <?php if($list->is_active == '1'): ?>
							<a href="<?php echo e(URL::to('updatestatus_content/'.$list->id.'/0')); ?>">Active</a>
						    <?php else: ?>
							<a href="<?php echo e(URL::to('updatestatus_content/'.$list->id.'/1')); ?>">Deactive</a>
						    <?php endif; ?>		
						    </span>
                                                </td>
						<td><input style="width:80px;" type="number" name="position" class="form-control" id="position" value="<?php echo e($list->position); ?>" onchange="change_position(this.value,<?php echo e($list->id); ?>)"></td>
                                                <td>
                                                    <div class="btn-group">
							<a href="<?php echo e(URL::to('edit_content/'.$list->id)); ?>"><button class="btn btn-xs green dropdown-toggle" type="button" aria-expanded="false"> <i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit
                                                        </button></a>
							<a href="<?php echo e(URL::to('deletecontent/'.$list->id)); ?>"><button class="btn btn-xs green dropdown-toggle" type="button" aria-expanded="false"> <i class="fa fa-pencil"></i>&nbsp;&nbsp;Delete
                                                        </button></a> 
                                                    </div>
                                                </td>
                                            </tr>
					    <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
<script>
function change_position(str,str1){
	$.ajax({
      		url: 'update_position/'+str+'/'+str1,
      		type: "get",
      		//data: {'position':str,'id':str1},
      		success: function(data){
        		alert("Position Successfully Updated")
      		}
    	}); 
	return false;
}
</script>
<?php echo $__env->make("admin_include.company_footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>        
