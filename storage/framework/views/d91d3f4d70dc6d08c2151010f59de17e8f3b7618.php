<?php echo $__env->make('front_include.front_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('front_include.front_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container-fluid banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if(!empty($banner[0])): ?>	
                <?php foreach($banner as $img): ?>
                <img src="<?php echo e(URL::asset('banner_images/'.$img->img)); ?>">
                <?php endforeach; ?>
                <?php else: ?>	
                <img src="<?php echo e(URL::asset('banner_images/banner-1.jpg')); ?>">	
                <?php endif; ?>
                <div class="banner-box">
                    <div class="banner-text">
                        <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                        <form name="search" action="<?php echo e(action('SearchController@index')); ?>" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
                        </form>
                    </div>		
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 bread">
            <ol class="breadcrumb nopadding">
                <li class="breadcrumb-item"><a href="<?php echo e(action('IndexController@index')); ?>">Home</a></li>
                <li class="breadcrumb-item active">Aanmelden</li>
            </ol>
        </div>
    </div>
</div>

<div class="container login-page">
    <div class="row">
        <div class="col-md-12">
            <div class="niews_title">
                <h1>Aanmelden</h1>
            </div>
        </div>
        <div class="col-md-12">
            <div class="login-form m-t-2">
                <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                    <?php if(count($errors) > 0): ?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php foreach($errors->all() as $error): ?>
                            <li><?php echo e($error); ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php foreach(['danger', 'warning', 'success', 'info'] as $msg): ?>
                    <?php if(Session::has('alert-' . $msg)): ?>
                    <p class="alert alert-<?php echo e($msg); ?>"><?php echo e(Session::get('alert-' . $msg)); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    <?php endif; ?>
                    <?php endforeach; ?>
                    <form action="<?php echo e(action('IndexController@sign')); ?>" method="post" class="form-horizontal login-page-form">
                        <div class="form-uitbatel">
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-sm-4 col-xs-12" for="email">Voornaam:</label>
                                <div class="col-sm-4  col-sm-4 col-xs-12 nopadding">
                                    <input id="text" class="form-control" type="text" placeholder="Voornaam" name="firstname" value="<?php echo e(old('firstname')); ?>">
                                </div>
                                <input id="text" class="form-control" type="hidden" placeholder="Voornaam" name="inserts" value="<?php echo e(old('inserts')); ?>">	
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-sm-4 col-xs-12" for="email">Achternaam:</label>
                                <div class="col-sm-4  col-sm-4 col-xs-12 nopadding">
                                    <input id="text" class="form-control" type="text" placeholder="Achternaam" name="lastname" value="<?php echo e(old('lastname')); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-sm-4 col-xs-12" for="email">E-mail:</label>
                                <div class="col-sm-4  col-sm-4 col-xs-12 nopadding">
                                    <input id="text" class="form-control" type="email" placeholder="voorbeeld@gmail.com" name="email" value="<?php echo e(old('email')); ?>">
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-sm-4 col-xs-12" for="email">Wachtwoord:</label>
                                <div class="col-sm-4 col-sm-4 col-xs-12 nopadding">
                                    <input id="password" class="form-control" type="password" placeholder="*******" name="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4 col-sm-4 col-xs-12" for="email">Bevestig Wachtwoord:</label>
                                <div class="col-sm-4 col-sm-4 col-xs-12 nopadding">
                                    <input id="confirm_password" class="form-control" type="password" placeholder="*******" name="confirm_password">
                                </div>
                            </div>
                            
                               <div class="form-group">        
                    <div class="col-sm-8" style="float:right;">
                        <div class="checkbox">
                            <label><input type="checkbox" name="terms" value="1" checked> <span style="color:black;">ik ga akkoord met de voorwaarden</span></label>
                        </div>
                    </div>
                </div>
                                <!--<div class="col-sm-4 col-sm-4 col-xs-12 nopadding">
                                    algemene voorwaarden
                                </div>-->
                            	
                        </div>
                        <div class="transactie-btn col-sm-8 col-sm-8 col-xs-12 signup">
                            <button class="btn btn-default login-green-btn" type="submit">Aanmelden</button>
			    
                        </div>
                        <div class="transactie-btn col-sm-6 col-sm-6 col-xs-12 fb-btn">
                        	<a href="redirect" class="btn btn-default blue-btn" style="float: left !important;">facebook login</a>
                    	    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




<!--End Content section-->   	
<?php echo $__env->make('front_include.front_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
