<?php echo $__env->make('front_include.front_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('front_include.front_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="container-fluid banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if(!empty($banner[0])): ?>	
                <?php foreach($banner as $img): ?>
                <img src="<?php echo e(URL::asset('banner_images/'.$img->img)); ?>">
                <?php endforeach; ?>
                <?php else: ?>	
                <img src="<?php echo e(URL::asset('banner_images/banner-1.jpg')); ?>">	
                <?php endif; ?>
                <div class="banner-box">
                    <div class="banner-text">
                        <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                        <form name="search" action="<?php echo e(action('SearchController@index')); ?>" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
                        </form>
                    </div>		
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 bread">
            <ol class="breadcrumb nopadding">
                <li class="breadcrumb-item"><a href="<?php echo e(action('IndexController@index')); ?>">Home</a></li>
                <?php if(Request::segment(1) == 'catlinks'): ?>
                <?php if($pname!=''): ?>
                <li class="breadcrumb-item"><a href="<?php echo e(URL::to('allcatlink/'.$pslug)); ?>"><?php echo e(ucwords(strtolower($pname))); ?></a></li>
                <?php endif; ?>
                <?php endif; ?>
                <li class="breadcrumb-item"><a href="#"><?php echo e(ucwords(strtolower($cat_name))); ?></a></li>
                <li class="breadcrumb-item active"><?php echo e(ucwords(strtolower('AANBIEDERS'))); ?></li>	
            </ol>
        </div>
    </div>
</div>
<!--Start Content section-->
<div class="container aan-section">
    <div class="row">
        <div class="col-md-9 custom_right_panel">
            <div class="niews_title">
                <h1 class="col-md-6 col-sm-6 col-xs-6 nopadding">Aanbieders</h1>
                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                    <div class="sort-section">
                        <p class="sort-title">Sorteer op :</p>
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Alfabet
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><?php if(Request::segment(1) == 'catlinks'): ?>
                                        <a href="<?php echo e(URL::to('catlinks/sort/'.$slug.'/date')); ?>">
                                            <?php else: ?>
                                            <a href="<?php echo e(URL::to($slug.'/date')); ?>">
                                                <?php endif; ?>	
                                                Date</a></li>
                                    <li><?php if(Request::segment(1) == 'catlinks'): ?>
                                        <a href="<?php echo e(URL::to('catlinks/sort/'.$slug.'/asc')); ?>">
                                            <?php else: ?>
                                            <a href="<?php echo e(URL::to($slug.'/asc')); ?>">
                                                <?php endif; ?>
                                                A-Z Alfabet</a></li>
                                    <li><?php if(Request::segment(1) == 'catlinks'): ?>
                                        <a href="<?php echo e(URL::to('catlinks/sort/'.$slug.'/des')); ?>">
                                            <?php else: ?>
                                            <a href="<?php echo e(URL::to($slug.'/des')); ?>">
                                                <?php endif; ?>
                                                D-A Alfabet</a></li> 
                                </ul>
                            </li>                       
                        </ul>
                    </div>
                </div>
            </div>

            <?php if(!empty($affiliatelinks)): ?>
            <?php foreach($affiliatelinks as $list): ?>
            <div class="aan-binder ">
                <div class="cool-blue col-md-12 nopadding">
                    <div class="col-md-2 col-sm-2 col-xs-12 nopadding">
                        <div class="ben-box">
                            <?php if(!empty($email)): ?>
                            <a href="?rid=<?php echo e($list->affiliate_link); ?>" target="_blank"><?php else: ?><a href="#" onclick="please_login()"><?php endif; ?>
                                    <?php if (filter_var($list->logo, FILTER_VALIDATE_URL)) { ?>
                                        <img src="<?php echo $list->logo ?>" class="img-responsive" />
                                    <?php } else { ?>
                                        <img src="<?php echo e(URl::asset('store_logos/'.$list->logo)); ?>" alt="<?php echo e($list->logo); ?>" class="img-responsive">
                                    <?php } ?>
                               </a>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 nopadding">
                        <h1 class="ben"><span data-toggle="tooltip" data-placement="top" title="<?php echo e($list->affiliate_name); ?>"><?php echo e($list->affiliate_name); ?></span></h1>
                        <h1 class="ben_smallscreen" style="display:none;"><span data-toggle="tooltip" data-placement="top" title="<?php echo e($list->affiliate_name); ?>"><?php echo $list->affiliate_name; ?></span></h1>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 nopadding ontv">
                        <h1>Ontvang <?php echo e($list->cashback); ?> van het aankoop bedrag</h1>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12 nopadding">
                        <?php if(!empty($email)): ?><a href="<?php echo e($list->affiliate_link); ?>"><?php else: ?><a href="#" onclick="please_login()"><?php endif; ?><button class="btn btn-default lees-btn">LEES MEER</button></a>
                    </div>
                </div>
            </div>			
            <?php endforeach; ?>
            <?php else: ?>
            <div class="">
                <div class="cool-blue col-md-12 nopadding" style="font-size: 20px;margin-top: 10px;">
                    Geen record gevonden.
                </div> 
            </div>
            <?php endif; ?>
            <div style='clear:both;'></div>
            <?php if(!empty($affiliatelinks)): ?>	
            <div class="pagination-section niews-pagi">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <?php echo $affiliatelinks1; ?>	
                    </ul>
                </nav>
            </div>
            <?php endif; ?>
        </div>

        <div class="col-md-3 aan-panel">
            <h2 class="cat-title">Categoriëen</h2>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php foreach($cats as $catlist): ?>

                <?php $subcats = AffiliateController::getsubcats($catlist->id); ?>                      <?php
                if (!$subcats) {
                    $is_category_product_exist = AffiliateController::is_category_product_exist($catlist->id);
                    if ($is_category_product_exist) {
                        $is_category_product_exist = true;
                    } else {
                        $is_category_product_exist = false;
                    }
                }
                ?>
                <div class="panel panel-default">
                    <?php if (!empty($subcats)) {
                        ?>
                        <?php $sub_cat_count = 0 ?>
                        <?php foreach($subcats as $sub): ?>
                        <?php
                        $is_category_product_exist = AffiliateController::is_category_product_exist($sub->id);
                        if ($is_category_product_exist) {
                            $is_category_product_exist = true;
                        } else {
                            $is_category_product_exist = false;
                        }
                        ?>
                        <?php
                        if ($is_category_product_exist) {
                            $sub_cat_count++;
                        }
                        ?>
                        <?php endforeach; ?>
                        <?php if ($sub_cat_count > 0) { ?>
                            <div class="panel-heading <?php if($parent_id == $catlist->id || Request::segment(2) == $catlist->slug): ?><?php echo e('open'); ?><?php endif; ?>">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo e($catlist->id); ?>" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="more-less glyphicon glyphicon-triangle-top"></i><?php echo e($catlist->name); ?></a>
                                </h4>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if ($is_category_product_exist) { ?>
                            <div class="panel-heading <?php if($parent_id == $catlist->id || Request::segment(2) == $catlist->slug): ?><?php echo e('open'); ?><?php endif; ?>">
                                <h4 class="panel-title">
                                    <a href="<?php echo e(URL::to(strtolower($catlist->slug))); ?>"><?php echo e($catlist->name); ?></a>

                                </h4>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if (!empty($subcats)) { ?>
                        <?php
//                        $slug_data = AffiliateController::get_using_slug($slug);
                        ?>
                        <div id="collapse<?php echo e($catlist->id); ?>" class="panel-collapse collapse<?php //echo ($slug_data[0]->parent_id == $catlist->id) ? ' in' : ''     ?>">
                            <div class="panel-body">
                                <ul>
                                    <?php foreach($subcats as $sub): ?>
                                    <?php
                                    $is_category_product_exist = AffiliateController::is_category_product_exist($sub->id);
                                    if ($is_category_product_exist) {
                                        $is_category_product_exist = true;
                                    } else {
                                        $is_category_product_exist = false;
                                    }
                                    ?>
                                    <?php if ($is_category_product_exist) { ?>
                                        <?php
                                        $sub_sub_cats = AffiliateController::getsubcats($sub->id);
                                        if ($sub_sub_cats) {
                                            foreach ($sub_sub_cats as $sub_sub_cat) {
                                                ?>
                                                <li <?php if($id == $sub_sub_cat->id): ?><?php echo e('class=active'); ?><?php endif; ?>><a href="<?php echo e(URL::to(strtolower($sub_sub_cat->slug))); ?>"><?php echo e($sub_sub_cat->name); ?></a></li>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <li <?php if($id == $sub->id): ?><?php echo e('class=active'); ?><?php endif; ?>><a href="<?php echo e(URL::to(strtolower($sub->slug))); ?>"><?php echo e($sub->name); ?></a></li>
                                    <?php } ?>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php endforeach; ?>    
            </div>     
        </div>

    </div>    
</div>
</div>
<?php echo $__env->make('front_include.front_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>