<?php echo $__env->make('front_include.front_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('front_include.front_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php

function month($monthName) {
    $month = array('January' => 'januari',
        'February' => 'februari',
        'March' => 'maart',
        'April' => 'april',
        'May' => 'mei',
        'June' => 'juni',
        'July' => 'juli',
        'August' => 'augustus',
        'September' => 'september',
        'October' => 'oktober',
        'November' => 'november',
        'December' => 'december'
    );

    foreach ($month as $key => $value):
        $monthName = str_replace($key, $value, $monthName);
    endforeach;

    return $monthName;
}
?>
<div class="container-fluid banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if(!empty($banner[0])): ?>	
                <?php foreach($banner as $img): ?>
                <img src="<?php echo e(URL::asset('banner_images/'.$img->img)); ?>">
                <?php endforeach; ?>
                <?php else: ?>	
                <img src="<?php echo e(URL::asset('banner_images/banner-1.jpg')); ?>">	
                <?php endif; ?>
                <div class="banner-box">
                    <div class="banner-text">
                        <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                        <form name="search" action="<?php echo e(action('SearchController@index')); ?>" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
                        </form>
                    </div>		
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 bread">
            <ol class="breadcrumb nopadding">
                <li class="breadcrumb-item"><a href="<?php echo e(action('IndexController@index')); ?>">Home</a></li>
                <li class="breadcrumb-item active">Nieuws</li>
            </ol>
        </div>
    </div>
</div>

<!--Start Content section-->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="niews_title">
                <h1 class="col-md-6 col-sm-6 col-xs-12 nopadding">Nieuws</h1>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="sort-section newssort">
                        <p class="sort-title">Sorteer op :</p>
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Date
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <!--<li><a href="<?php echo e(URL::to('sortnews/name')); ?>">Alfabet</a></li>-->
                                    <li><a href="<?php echo e(URL::to('sortnews/date')); ?>">Date</a></li> 
                                </ul>
                            </li>                       
                        </ul>
                    </div>
                </div>
            </div>
        </div>	
    </div>
</div>

<div class="container m-t-3">
    <div class="row">
        <?php if(!empty($newslist)): ?>
        <?php foreach($newslist as $list): ?>
        <div class="cool-blue col-md-12">
            <div class="col-md-2 col-sm-2 col-xs-12 nopadding">
                <div class="cool-box">
                    <a href="<?php echo e(URL::to('singlenews/'.$list->slug)); ?>"><img src="<?php echo e(URL::asset('news_images/'.$list->file)); ?>"></a>
                </div>
            </div>
            <div class="col-md-10 col-sm-10 col-xs-12 mobilenews">
                <div class="col-md-9 col-sm-8 col-xs-12 nopadding">
                    <a href="<?php echo e(URL::to('singlenews/'.$list->slug)); ?>"><h1><?php echo e($list->name); ?></h1></a>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12 nopadding">
                    <p class="niews_date"><?php echo date("d-m-Y", strtotime($list->date_upd)); ?></p>
                </div>
                <?php echo str_limit($list->news_content, $limit = 350, $end = '...'); ?>

            </div>
        </div>
        <?php endforeach; ?>
        <?php else: ?>
        <div class="col-md-10 col-sm-10 col-xs-12">
            <div class="col-md-9 col-sm-8 col-xs-12 nopadding">
                <p><?php echo e('No records found'); ?></p>
            </div>

        </div>
        <?php endif; ?>	

        <div class="pagination-section col-md-12 niews-pagi">
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <?php echo $newslist1; ?>	
                </ul>
            </nav>
        </div>

    </div>

</div>
<div style="clear:both;"></div>
<hr class="linehr">



<!--End Content section-->

<div class="container-fluid slider-border">
    <div class="slider-section">
        <div class="container">
            <div class="col-md-12 nopadding-slide">

                <div class="well">
                    <div id="myCarousel" class="carousel slide">

                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="row">
                                    <?php $a = 0;
                                    $st = '0' ?>
                                    <?php foreach($links as $aff_link): ?>


                                    <div class="col-sm-3 col-xs-3 first text-center">
                                        <?php if(!empty($email)): ?>
                                        <a href="<?php echo e($aff_link->affiliate_link); ?>" target="_blank"><?php else: ?><a href="#" onclick="please_login()">
                                                <?php endif; ?>
                                                <?php if (filter_var($aff_link->logo, FILTER_VALIDATE_URL)) { ?>
                                                    <img src="<?php echo $aff_link->logo ?>"  alt="<?php echo e($aff_link->logo); ?>" class="img-responsive" />
                                                <?php } else { ?>
                                                    <img src="<?php echo e(URl::asset('store_logos/'.$aff_link->logo)); ?>" alt="<?php echo e($aff_link->logo); ?>" class="img-responsive">
<?php } ?>
                                            </a>
                                            <?php if(!empty($email)): ?><a href="<?php echo e($aff_link->affiliate_link); ?>" style="text-decoration:none;"><?php else: ?><a href="#" onclick="please_login()" style="text-decoration:none;"><?php endif; ?><h3><?php if($a%2==0): ?><?php echo e('cashback'); ?><?php else: ?><?php echo e('Ontvang'); ?><?php endif; ?></h3></a>
                                                <?php if(!empty($email)): ?><a href="<?php echo e($aff_link->affiliate_link); ?>" style="text-decoration:none;"><?php else: ?><a href="#" onclick="please_login()" style="text-decoration:none;"><?php endif; ?><h1><?php echo e($aff_link->cashback); ?></h1></a>
                                                    <p><?php echo e($aff_link->affiliate_name); ?></p>
                                                    </div>
<?php $a++;
$st++; ?>   
                                                    <?php if($a==4 && $st!='12'): ?>
                                                    </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="row">
<?php $a = '0'; ?>
                                                            <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </div><!--/row-->

                                                    </div>
                                                    </div><!--/carousel-inner--> 
                                                    </div>
                                                    <!--/myCarousel-->
                                                    </div>
                                                    <!--/well-->
                                                    </div>
                                                    </div>
                                                    <a class="left l-arrow carousel-control" href="#myCarousel" data-slide="prev"><img src="<?php echo e(URL::asset('front_img/left-icon.png')); ?>"></a>
                                                    <a class="right r-arrow carousel-control" href="#myCarousel" data-slide="next"><img src="<?php echo e(URL::asset('front_img/right-icon.png')); ?>"></a>
                                                    </div>
                                                    </div>











                                                    <?php echo $__env->make('front_include.front_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
