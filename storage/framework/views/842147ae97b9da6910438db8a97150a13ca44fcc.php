<?php echo $__env->make("admin_include.company_header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
	<?php echo $__env->make("admin_include.admin_center", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <?php echo $__env->make("admin_include.admin_sidebar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>News List</h1>
                        </div>
                        <!-- END PAGE TITLE -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo e(action('DashboardController@index')); ?>">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">News List</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
				<?php foreach(['danger', 'warning', 'success', 'info'] as $msg): ?>
      					<?php if(Session::has('alert-' . $msg)): ?>
						<p class="alert alert-<?php echo e($msg); ?>"><?php echo e(Session::get('alert-' . $msg)); ?> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      					<?php endif; ?>
    				<?php endforeach; ?>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
                                                <th> Image </th>
                                                <th> Name </th>
						<th> Content </th>
                                                <th> Status </th>
                                                <th style="width:110px;"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
					    <?php foreach($news as $list): ?>	
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
						<td> <img src="<?php echo e(URL::asset('news_images/'.$list->file)); ?>" alt="<?php echo e($list->file); ?>" style="width:60px; height:60px;"> </td>                                                
						<td> <?php echo e($list->name); ?> </td>
                                                <td>
                                                     <?php echo str_limit($list->news_content, $limit = 50, $end = '...'); ?>

                                                </td>
                                                <td>
                                                    <span class="label label-sm"> 
								<?php if($list->is_active == 1): ?>
    									<a href="<?php echo e(URL::to('updatestatus/'.$list->id.'/0')); ?>">Active</a>
								<?php else: ?>
    									<a href="<?php echo e(URL::to('updatestatus/'.$list->id.'/1')); ?>">Deactive</a>
								<?php endif; ?> 
						   </span>
                                                </td>
                                                <td>
                                                    <div class="btn-group">
							<a href="<?php echo e(URL::to('editnews/'.$list->id)); ?>"><span class="btn btn-xs green dropdown-toggle" type="button" aria-expanded="false"> <i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit
                                                        </span></a>
							<a href="<?php echo e(URL::to('deletenews/'.$list->id)); ?>"><span class="btn btn-xs green dropdown-toggle" type="button" aria-expanded="false"> <i class="fa fa-times"></i>&nbsp;&nbsp;Delete
                                                        </span></a>
                                                    </div>
                                                </td>
                                            </tr>
					    <?php endforeach; ?>	
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
<?php echo $__env->make("admin_include.company_footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>        
