<?php echo $__env->make('front_include.front_header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('front_include.front_menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="container-fluid banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if(!empty($banner[0])): ?>	
                    <?php foreach($banner as $img): ?>
                    <img src="<?php echo e(URL::asset('banner_images/'.$img->img)); ?>">
		    <?php endforeach; ?>
		    <?php else: ?>	
		    <img src="<?php echo e(URL::asset('banner_images/banner-1.jpg')); ?>">	
		    <?php endif; ?>
                    <div class="banner-box">
                        <div class="banner-text">
                            <h1>Zoek uit meer dan 1.000 webshops jouw cashback!</h1>
                            <form name="search" action="<?php echo e(action('SearchController@index')); ?>" method="get">	
                            <input type="text" name="search" class="banner-input col-md-12" placeholder="Zoek een webshop waar je een aankoop wilt doen">	
                            <button type="submit" class="btn work-btn" value="ZOEKEN">ZOEKEN
			    </form>
                        </div>		
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
    	<div class="row">
        	<div class="col-md-12 bread">
            	<ol class="breadcrumb nopadding">
                  <li class="breadcrumb-item"><a href="<?php echo e(action('IndexController@index')); ?>">Home</a></li>
                  <li class="breadcrumb-item active">FAQ's</li>
                </ol>
            </div>
        </div>
    </div>
    
    <!--Start Content section-->
    <div class="container">
    	<div class="row">
        	<div class="col-md-12">
                <div class="niews_title">
                    <h1 class="col-md-6 col-sm-6 col-xs-12 nopadding">FREQUENTLY ASKED QUESTIONS</h1>
                    
                </div>
            </div>	
        </div>
    </div>
    
    <div class="container m-t-3">
    	<div class="row">
		<?php if(!empty($faqlist)): ?>
		<?php foreach($faqlist as $list): ?>
        	<div class="cool-blue col-md-12 faq-box">
		<h1><?php echo e($list->name); ?></h1>
		<p><?php echo $list->detail; ?></p>
		</div>
            	
              <?php endforeach; ?>  
            </div>
            
	    <?php else: ?>
	    	<div class="col-md-10 col-sm-10 col-xs-12">
                    <div class="col-md-9 col-sm-8 col-xs-12 nopadding">
                        <p><?php echo e('No records found'); ?></p>
                    </div>
                    
                </div>
	    <?php endif; ?>	
            
           	<div class="pagination-section col-md-12 niews-pagi">
            	<nav aria-label="Page navigation">
                  <ul class="pagination">
		    <?php echo $faqlist1; ?>	
                  </ul>
                </nav>
            </div>
	    
        </div>
    </div>
    

    
    
        
    <!--End Content section-->
    
<?php echo $__env->make('front_include.front_footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
