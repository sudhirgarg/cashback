<!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li <?php if(Request::is('admin/dashboard')): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="<?php echo e(action('admin\DashboardController@index')); ?>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard
				</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="heading">
                            <h3 class="uppercase">Features</h3>
                        </li>
			<li <?php if(Request::segment(2) == 'bannerlist'  || Request::segment(2) == 'addbanner'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-picture-o"></i>
                                <span class="title">Manage Banners</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li <?php if(Request::is('admin/addbanner')): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\BannerimgController@index')); ?>" class="nav-link ">
                                        <span class="title">Add Banner</span>
                                    </a>
                                </li>
                        	<li <?php if(Request::segment(2) == 'bannerlist'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\BannerimgController@bannerlist')); ?>" class="nav-link ">
                                        <span class="title">Banner's List</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li <?php if(Request::segment(2) == 'users'  || Request::segment(2) == 'usertransaction'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="<?php echo e(action('admin\HomeuserController@index')); ?>" class="nav-link nav-toggle">
                                <i class="fa fa-users"></i>
                                <span class="title">Users</span>
                            </a>
                        </li>
			<li <?php if(Request::segment(2) == 'addstore'  || Request::segment(2) == 'storelist'  || Request::segment(2) == 'editstore'  || Request::segment(2) == 'updatestorecat_storelist'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-list"></i>
                                <span class="title">Manage Store</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li <?php if(Request::segment(2) == 'addstore'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\StoreController@index')); ?>" class="nav-link ">
                                        <span class="title">Add Store</span>
                                    </a>
                                </li>
                                <li <?php if(Request::segment(2) == 'storelist'  || Request::segment(2) == 'editstore'  || Request::segment(2) == 'updatestorecat_storelist'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\StoreController@storelist')); ?>" class="nav-link ">
                                        <span class="title">All Store List</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li <?php if(Request::segment(2) == 'companylist'  || Request::segment(2) == 'company'  || Request::segment(2) == 'editcompany' || Request::segment(2) == 'updatestorecat' ): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-industry"></i>
                                <span class="title">Manage Company</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li <?php if(Request::is('admin/company')): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\CompanyController@index')); ?>" class="nav-link ">
                                        <span class="title">Add Company</span>
                                    </a>
                                </li>
                        	<li <?php if(Request::segment(2) == 'companylist'  || Request::segment(2) == 'editcompany' || Request::segment(2) == 'updatestorecat'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\CompanyController@companylist')); ?>" class="nav-link ">
                                        <span class="title">Company's List</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li <?php if(Request::segment(2) == 'contentlist' || Request::segment(2) == 'addcontent' || Request::segment(2) == 'edit_content'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-list"></i>
                                <span class="title">Manage Contents</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li <?php if(Request::is('admin/addcontent')): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\ContentController@index')); ?>" class="nav-link ">
                                        <span class="title">Add Content Page</span>
                                    </a>
                                </li>
                                <li <?php if(Request::segment(2) == 'contentlist' || Request::segment(2) == 'edit_content'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\ContentController@contentpagelist')); ?>" class="nav-link ">
                                        <span class="title">Content Pages List</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
			<li <?php if(Request::segment(2) == 'categorylist' || Request::segment(2) == 'addcategory' || Request::segment(2) == 'editcat'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-sitemap"></i>
                                <span class="title">Manage Categories</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li <?php if(Request::is('admin/addcategory')): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\CategoryController@index')); ?>" class="nav-link ">
                                        <span class="title">Add Category</span>
                                    </a>
                                </li>
                                <li <?php if(Request::segment(2) == 'categorylist' || Request::segment(2) == 'editcat'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\CategoryController@categorylist')); ?>" class="nav-link ">
                                        <span class="title">All Categories List</span>
                                    </a>
                                </li>
                            </ul>
                        </li>	
			<li <?php if(Request::segment(2) == 'newslist' || Request::segment(2) == 'addnews' || Request::segment(2) == 'editnews'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-newspaper-o"></i>
                                <span class="title">Manage News</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li <?php if(Request::is('admin/addnews')): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\NewsController@index')); ?>" class="nav-link ">
                                        <span class="title">Add News</span>
                                    </a>
                                </li>
                                <li <?php if(Request::segment(2) == 'newslist' || Request::segment(2) == 'editnews'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\NewsController@newslist')); ?>" class="nav-link ">
                                        <span class="title">All News List</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
			<li <?php if(Request::is('admin/transactions')): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="<?php echo e(action('admin\TransactionController@index')); ?>" class="nav-link nav-toggle">
                                <i class="fa fa-money"></i>
                                <span class="title">Transaction's History</span>
                            </a>
                        </li>
			<li <?php if(Request::is('admin/requestpayment')): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="<?php echo e(action('admin\RequestpaymentController@index')); ?>" class="nav-link nav-toggle">
                                <i class="fa fa-eur"></i>
                                <span class="title">Payment Requests</span>
                            </a>
                        </li>
			<li <?php if(Request::is('admin/claimfiled')): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="<?php echo e(action('admin\ClaimfiledController@index')); ?>" class="nav-link nav-toggle">
                                <i class="fa fa-briefcase"></i>
                                <span class="title">Cashback Filed Claim's</span>
                            </a>
                        </li>
			<li <?php if(Request::segment(2) == 'transfer'  || Request::segment(2) == 'transfercashlist'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-eur"></i>
                                <span class="title">Manage Transfers</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li <?php if(Request::segment(2) == 'transfer'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\TransfercashController@index')); ?>" class="nav-link ">
                                        <span class="title">Cashback Transfer to User</span>
                                    </a>
                                </li>
                                <li <?php if(Request::segment(2) == 'transfercashlist'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\TransfercashController@transferlist')); ?>" class="nav-link ">
                                        <span class="title">List Cashback Transfers</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
			<li <?php if(Request::segment(2) == 'emaillist' || Request::segment(2) == 'addemailtemplate' || Request::segment(2) == 'editcat'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-sitemap"></i>
                                <span class="title">Manage Email Templates</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li <?php if(Request::is('admin/addemailtemplate')): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\EmailController@index')); ?>" class="nav-link ">
                                        <span class="title">Add Email Template</span>
                                    </a>
                                </li>
                                <li <?php if(Request::segment(2) == 'emaillist' || Request::segment(2) == 'editcat'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\EmailController@emaillist')); ?>" class="nav-link ">
                                        <span class="title">All Email Templates List</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
			<li <?php if(Request::segment(2) == 'addfaq'  || Request::segment(2) == 'faqlist'  || Request::segment(2) == 'editfaq'  || Request::segment(2) == 'apilist'  || Request::segment(2) == 'editapi'  || Request::segment(2) == 'maillist'  || Request::segment(2) == 'editmail'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-cog fa-fw"></i>
                                <span class="title">Settings</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li <?php if(Request::segment(2) == 'addfaq'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\FaqController@index')); ?>" class="nav-link ">
                                        <span class="title">Add Faq</span>
                                    </a>
                                </li>
                                <li <?php if(Request::segment(2) == 'faqlist' || Request::segment(2) == 'editfaq'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\FaqController@faqlist')); ?>" class="nav-link ">
                                        <span class="title">List Faq's</span>
                                    </a>
                                </li>
				<li <?php if(Request::segment(2) == 'apilist' || Request::segment(2) == 'editapi'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\ApiController@apilist')); ?>" class="nav-link ">
                                        <span class="title">Api Setting</span>
                                    </a>
                                </li>
                                <li <?php if(Request::segment(2) == 'maillist' || Request::segment(2) == 'editmail'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(action('admin\MailController@maillist')); ?>" class="nav-link ">
                                        <span class="title">Email Setting</span>
                                    </a>
                                </li>
				<li <?php if(Request::segment(2) == 'editcashbackper'): ?>class="nav-item start active"<?php else: ?> class="nav-item"<?php endif; ?>>
                                    <a href="<?php echo e(URl::to('admin/editcashbackper')); ?>" class="nav-link ">
                                        <span class="title">Cashback % distribution</span>
                                    </a>
                                </li>
                            </ul>
                        </li>	
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
