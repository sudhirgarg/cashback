<?php

require 'connection.php';
$customer_id = '143754';
$access_key = '800a555f0f1b014c8be058a2e65e5c385d68a38d';

$client = new SoapClient('http://ws.tradetracker.com/soap/affiliate?wsdl', array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP));
$client->authenticate(143754, '800a555f0f1b014c8be058a2e65e5c385d68a38d');

$affiliateSiteID = 259674;

$options = array(
    'campaignCategoryID' => 1,
);
$response = $client->getFeedProducts($affiliateSiteID, $options);

pr($response);
die;
$p = isset($_GET['p']) ? $_GET['p'] : 1;

if ((array) $response) {
    $data = $response;
    foreach ($data as $key => $value) {
        $value = (array) $value;
        $website_name = parse_url($value['productURL'])['host'];
        $website_name = explode('.', $website_name);
        $website_name = $website_name[0];
        $is_exist_category = check('category', array(
            'name' => $value['productCategoryName'],
        ));
        if (!$is_exist_category) {
            $slug = strtolower(str_replace(' ', '_', $value['productCategoryName']));
            $data_category = array(
                'name' => $value['productCategoryName'],
                'slug' => $slug,
                'is_active' => 1,
                'sequence_number' => $slug,
                'parent_id' => 23,
                'api_cat_id' => $value['identifier']
            );
            $cat_id = save('category', $data_category);
        } else {
            $cat_id = $is_exist_category['id'];
        }
        if ($cat_id) {
            $rand = rand();
            $logo_name = $root . '/public/store_logos/logo_' . $rand;
            $ch = curl_init($value['imageURL']);
            $fp = fopen($logo_name, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
            $CommissionTypes = isset($value['CommissionTypes']) ? $value['CommissionTypes']->CommissionTypeDetail : '';
            $CommissionTypes_min = isset($CommissionTypes->VolumeMin) ? $CommissionTypes->VolumeMin : '';
            $CommissionTypes_max = isset($CommissionTypes->VolumeMin) ? $CommissionTypes->VolumeMax : '';
            $CommissionTypes_unit = isset($CommissionTypes->Unit) ? $CommissionTypes->Unit : '';
            if ($CommissionTypes_min && $CommissionTypes_max && $CommissionTypes_unit) {
                $cashback = $CommissionTypes_min . '-' . $CommissionTypes_max . $CommissionTypes_unit;
            } else {
                $cashback = '';
            }
            $data_company = array(
                'name' => $website_name,
                'cat_id' => $cat_id,
                'product_name' => $website_name,
                'slug' => $rand,
                'detail' => $value['description'],
                'logo' => 'logo_' . $rand,
                'affiliate_link' => $value['productURL'],
                'affiliate_name' => $website_name,
                'cashback' => $cashback,
                'is_active' => 1,
                'api_product_id' => $value['identifier']
            );
            $same_category_field_exist = check('company', array(
                'cat_id' => $cat_id,
                'api_product_id' => $value['identifier']
            ));
            if (!$same_category_field_exist) {
                save('company', $data_company);
            } else {
                $data_company['id'] = $same_category_field_exist['id'];
                update('company', $data_company);
            }
        }
    }
}
echo 'Cron Job Done.';
