<?php

require_once 'db.php';
header('Content-Type: application/json');

$url1 = 'http://api.zanox.com/json/2011-03-01/programs?connectid=CFB6F0742D488F418046';
$cURL = curl_init();
curl_setopt($cURL, CURLOPT_URL, $url1);
curl_setopt($cURL, CURLOPT_HTTPGET, true);
curl_setopt($cURL, CURLOPT_RETURNTRANSFER, TRUE);
$result = curl_exec($cURL);
curl_close($cURL);

$exe = json_decode($result);
$temp = $exe->programItems->programItem;

foreach ($temp as $programs) {
    $programs = (array) $programs;
    $program_id = $programs['@id'];
    $program_name = $programs['name'];
    $status = $programs['status'];
    $url = $programs['url'];
    $main_url = $programs['url'];
    $date = date("Y-m-d H:m:s", strtotime($programs['startDate']));

    $commission_percentage = isset($programs['commission']->salePercentMin) ? $programs['commission']->salePercentMin : '';
    $cats = $programs['categories'];

    if ($status == 'active') {
        $query_get = "select id from store where store_api_id='$program_id'";
        $res = mysqli_query($connection, $query_get);
        $row = mysqli_fetch_array($res);
        if (!empty($row)) {
            $store_id = $row['id'];
        } else { // insert new store
            $slug = str_replace(" ", "", $program_name);
            $slug = $slug . '' . rand(11111, 99999);
            $query = "insert into store(name,slug,is_active,date_add,store_api_id) values('" . $program_name . "','" . $slug . "','1','" . $date . "','" . $program_id . "')";
            mysqli_query($connection, $query);
            $store_id = $connection->insert_id;
        }
        $list_cat = $cats[0]->category;
        $list_cat = (array) $list_cat;
        foreach ($list_cat as $cat) {
            $cat = (array) $cat;
            $cat_name = (!empty($cat['$'])) ? $cat['$'] : $list_cat['$'];
            $cat_id = (!empty($cat['@id'])) ? $cat['@id'] : $list_cat['@id'];
            $date = date("Y-m-d H:m:s");
            $query_get = "select id from category where api_cat_id='$cat_id'";
            $res = mysqli_query($connection, $query_get);
            $row = mysqli_fetch_array($res);
            if (!empty($row)) { // existing category link with store
                $cat_id = $row['id'];
                $query_get = "select id from company_category where store_id='$store_id' and category_id='$cat_id'";
                $res = mysqli_query($connection, $query_get);
                $row = mysqli_fetch_array($res);
                if (empty($row)) {
                    $query = "insert into company_category(store_id,category_id) values('" . $store_id . "','" . $cat_id . "')";
                    mysqli_query($connection, $query);
                }
            } else { // insert new category and link with store
                $slug = str_replace(" ", "", $cat_name);
                $slug = $slug . '' . rand(11111, 99999);
//                $query = "insert into category(name,slug,sequence_number,is_active,date_add,api_cat_id) values('" . $cat_name . "','" . $slug . "','" . $slug . "','1','" . $date . "','" . $cat_id . "')";
//                mysqli_query($connection, $query);
                $cat_id = $connection->insert_id;
                $query = "insert into company_category(store_id,category_id) values('" . $store_id . "','" . $cat_id . "')";
                mysqli_query($connection, $query);
            }
        }
        /* update query to all products in in-active state */
        $query_upd = "update company set is_active='0' where name='" . $store_id . "'";
        mysqli_query($connection, $query_upd);

        /* update query to all products in in-active state end */
        $url2 = 'http://api.zanox.com/json/2011-03-01/products?connectid=43EEF0445509C7205827&programs=7408';
        $cURL1 = curl_init();
        curl_setopt($cURL1, CURLOPT_URL, $url2);
        curl_setopt($cURL1, CURLOPT_HTTPGET, true);
        curl_setopt($cURL1, CURLOPT_RETURNTRANSFER, TRUE);
        $result1 = curl_exec($cURL1);
        curl_close($cURL1);

        $exe1 = json_decode($result1);
        $products = $exe1->productItems->productItem;

        foreach ($products as $pro_list) {
            $pro_list = (array) $pro_list;
            $product_id = $pro_list['@id'];
            $product_desc = $pro_list['description'];
            $product_name = $pro_list['name'];
            $product_cat = $pro_list['merchantCategory'];
            $affiliate_name = $pro_list['program'];
            $affiliate_name = (array) $affiliate_name;
            $aff_name = $affiliate_name['$'];
            $query_get = "select id from category where name='$product_cat'";
            $res = mysqli_query($connection, $query_get);
            $row = mysqli_fetch_array($res);
            if (!empty($row)) {
//                $cat_id = $row['id'];
                $cat_id = 8;
            } else {
                $cat_id = 8;
            }
            $product_slug = rand(11111, 99999);
            $image_name = $pro_list['image']->large;
            $path = '../public/store_logos';
            $product_img = rand(10101, 99999);
            $content = file_get_contents($image_name);
            file_put_contents($path . '/' . $product_img . '.jpg', $content);
            $image_name = $product_img . '.jpg';
            $comm = $commission_percentage . '%';

            $query_get = "select id from company where product_name='$product_name'";
            $res = @mysqli_query($connection, $query_get);
            $row = @mysqli_fetch_array($res);
            if (!empty($row)) { // update product
                $query_upd = "update company set name='" . $store_id . "',cat_id='" . $cat_id . "',slug='" . $product_slug . "',detail='" . $product_desc . "',logo='" . $image_name . "',cashback='" . $comm . "',affiliate_name='" . $aff_name . "',affiliate_link='" . $url . "',is_active='1' where product_name='" . $product_name . "'";
                mysqli_query($connection, $query_upd);
            }
            $cat_id = 8;
            if (empty($row)) { // insert new product	
                $date_add = date('Y-m-d h:i:s');
                $query_ins = "insert into company(name,cat_id,product_name,slug,detail,logo,cashback,affiliate_name,affiliate_link,is_active,date_add,api_product_id) values('" . $store_id . "','" . $cat_id . "','" . $product_name . "','" . $product_slug . "','" . $product_desc . "','" . $image_name . "','" . $comm . "','" . $aff_name . "','" . $url . "','1',
'" . $date_add . "','" . $product_id . "')";
                mysqli_query($connection, $query_ins);
            }
        }
    }
}
echo 'Cron Job Done.';
?>