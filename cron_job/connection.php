<?php

$root = DIRNAME(__DIR__);

$db_username = 'root';
$db_password = 'cwebco';
$db_name = 'cashback';
$is_connect = mysqli_connect('localhost', $db_username, $db_password, $db_name);
if (!$is_connect) {
    echo 'Please Check Database Connection';
}

function pr($e) {
    echo '<pre>';
    print_r($e);
    echo '</pre>';
}

function save($table, $data = array(), $show_query = false) {
    global $is_connect;
    $query1 = "INSERT INTO " . $table . " SET ";
    foreach ($data as $key => $value) {
        if ($value != '') {
            $query1.=$key . "=" . "'" . @mysqli_real_escape_string($is_connect, $value) . "'" . ', ';
        }
    }
    if ($show_query) {
        echo $query = substr($query1, 0, strlen($query1) - 2);
        die;
    }
    $query = substr($query1, 0, strlen($query1) - 2);
    $done = mysqli_query($is_connect, $query);
    if ($done) {
        return mysqli_insert_id($is_connect);
    }
}

function update($table, $data = array(), $field = 'id', $field_value = '', $show_query = false) {
    global $is_connect;
    $query1 = "UPDATE " . $table . " SET ";
    foreach ($data as $key => $value) {
        if ($value != '') {
            $query1.=$key . "=" . "'" . @mysqli_real_escape_string($is_connect, $value) . "'" . ', ';
        }
    }
    $query = substr($query1, 0, strlen($query1) - 2);
    if (isset($data['id'])) {
        $query .= ' WHERE `id` = ' . $data['id'];
    } else {
        $query .= ' WHERE ' . $field . ' = "' . $field_value . '"';
    }
    if ($show_query) {
        echo $query;
        die;
    }
    $done = mysqli_query($is_connect, $query);
    if ($done && $field && $field_value) {
        $result_query = 'SELECT * FROM ' . $table . ' WHERE ' . $field . ' = "' . $field_value . '"';
        $result = mysqli_query($is_connect, $result_query);
        $r = mysqli_fetch_object($result);
        return $r->id;
    } else if ($done) {
        return true;
    }
}

function display($table, $field = '', $value = '', $show_field = true) {
    global $is_connect;
    $data = array();
    $query = "SELECT * FROM $table";
    if ($field && $value) {
        $query .= ' WHERE ' . $field . ' = ' . $value;
    }
    if ($show_field) {
        echo $query;
        die;
    }
    $result = mysqli_query($is_connect, $query);
    while ($r = @mysqli_fetch_assoc($result)) {
        $data[] = $r;
    }
    return $data;
}

function check($table, $data = array(), $show_query = false) {
    global $is_connect;
    $query = "SELECT * FROM `$table` WHERE ";
    foreach ($data as $key => $value) {
        $query.=$key . "=" . "'" . @mysqli_real_escape_string($is_connect, $value) . "'" . ' AND ';
    }
    $query = substr($query, 0, strlen($query) - 4);
    if ($show_query) {
        echo $query;
        die;
    }
    $result = mysqli_query($is_connect, $query);
    $r = @mysqli_fetch_assoc($result);
    return $r;
}

?>