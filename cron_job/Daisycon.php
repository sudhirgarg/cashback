<?php

require_once 'connection.php';

error_reporting(E_ALL);
ini_set("display_errors", 1);
$publisher_id = 379880;
$url = 'https://services.daisycon.com/categories';
$username = 'info@cashback4you.nl';
$password = 'Cashbackkoningen1';
$ch = curl_init();
$headers = array('Authorization: Basic ' . base64_encode($username . ':' . $password));
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

curl_close($ch);
if ($code == 200) {
    $category_responses = json_decode($response);
    foreach ($category_responses as $category_response) {
        $slug = str_replace(' ', '_', $category_response->name);
        $slug = str_replace('/', '_', $category_response->name);
        if ($category_response->id == 35 || $category_response->id == 12) {
            $parent_id = 13;
        } else if ($category_response->id == 27 || $category_response->id == 26 || $category_response->id == 38 || $category_response->id == 21 || $category_response->id == 31) {
            $parent_id = 21;
        } else if ($category_response->id == 19 || $category_response->id == 5 || $category_response->id == 21 || $category_response->id == 25 || $category_response->id == 2 || $category_response->id == 4 || $category_response->id == 9 || $category_response->id == 23) {
            $parent_id = 18;
        } else if ($category_response->id == 24 || $category_response->id == 40) {
            $parent_id = 19;
        } else if ($category_response->id == 10 || $category_response->id == 41) {
            $parent_id = 1;
        } else if ($category_response->id == 18) {
            $parent_id = 10;
        } else if ($category_response->id == 1) {
            $parent_id = 23;
        } else if ($category_response->id == 8) {
            $parent_id = 16;
        } else if ($category_response->id == 7 || $category_response->id == 44) {
            $parent_id = 17;
        } else if ($category_response->id == 3) {
            $parent_id = 7;
        } else if ($category_response->id == 36) {
            $parent_id = 8;
        } else {
            $parent_id = 15;
        }
        $category_data = array(
            'name' => $category_response->name,
            'slug' => $slug,
            'sequence_number' => $slug,
            'is_active' => 1,
            'api_cat_id' => $category_response->id,
            'parent_id' => $parent_id
        );
        $is_sub_category_exist = check('category', array(
            'name' => $category_response->name,
            'api_cat_id' => $category_response->id
        ));
        if (!$is_sub_category_exist) {
            save('category', $category_data);
        } else {
            unset($category_data['parent_id']);
            update('category', $category_data, 'name', $category_response->name);
        }
    }
}
// Inserting Category Product Data
$publisher_id = 379880;
$url = 'https://services.daisycon.com/publishers/' . $publisher_id . '/programs';
$username = 'info@cashback4you.nl';
$password = 'Cashbackkoningen1';

$ch = curl_init();

$headers = array('Authorization: Basic ' . base64_encode($username . ':' . $password));

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);
if ($code == 200) {
    $programs = json_decode($response);
    foreach ($programs as $program) {
        $categories_ids = $program->category_ids;
        foreach ($categories_ids as $categories_id) {
            $which_category = check('category', array(
                'api_cat_id' => $categories_id
            ));
            $slug = str_replace(' ', '_', $program->name);
            $rand = rand();
//            $logo_name = $root . '/public/store_logos/logo_' . $rand . '.png';
//            $link_logo = 'https:' . $program->logo;
//            $ch = curl_init($link_logo);
//            $fp = fopen($logo_name, 'wb');
//            curl_setopt($ch, CURLOPT_FILE, $fp);
//            curl_setopt($ch, CURLOPT_HEADER, 0);
//            curl_exec($ch);
//            curl_close($ch);
//            fclose($fp);
            $min_fixed = isset($program->commission->min_fixed) ? $program->commission->min_fixed : '';
            $max_fixed = isset($program->commission->max_fixed) ? $program->commission->max_fixed : '';
            if ($min_fixed && $max_fixed) {
                $cashback = $min_fixed . ' - ' . $max_fixed;
            } else {
                $cashback = '';
            }
            $data_company = array('name' => $program->name,
                'cat_id' => $which_category['id'],
                'product_name' => $program->name,
                'slug' => $rand,
                'detail' => $program->descriptions[0]->description,
//                'logo' => 'logo_' . $rand . '.png',
                'logo' => 'https:' . $program->logo,
                'affiliate_link' => 'http://' . $program->display_url,
                'affiliate_name' => $program->name,
                'cashback' => $cashback,
                'is_active' => 1,
                'api_product_id' => $program->id
            );

            $same_category_field_exist = check('company', array(
                'name' => $program->name,
                'cat_id' => $which_category['id'],
                'api_product_id' => $program->id
            ));
            if (!$same_category_field_exist) {
                save('company', $data_company);
            } else {
                $data_company['id'] = $same_category_field_exist['id'];
                update('company', $data_company);
            }
        }
    }
}
echo 'Cron Job Done.';
?>