<?php

require 'connection.php';

// Set webservice endpoints
define("WSDL_LOGON", "https://api.affili.net/V2.0/Logon.svc?wsdl");
define("WSDL_WS", "https://api.affili.net/V2.0/PublisherProgram.svc?wsdl");

// Set credentials
$username = "782926"; // the publisher ID
$password = "tZgLMisgQBVam9V0MdUS"; // the publisher web services password
// Send a request to the Logon Service to get an authentication token
$soapLogon = new SoapClient(WSDL_LOGON);
$token = $soapLogon->Logon(array(
    'Username' => $username,
    'Password' => $password,
    'WebServiceType' => 'Publisher'
        ));

$soapRequest = new SoapClient(WSDL_WS);
$response = $soapRequest->GetProgramCategories($token);

$response_data = $response->RootCategories->ProgramCategory;
foreach ($response_data as $response_d) {
    $response_d = (array) $response_d;
    $sub_categories = isset($response_d['SubCategories']->ProgramCategory) ? $response_d['SubCategories']->ProgramCategory : '';
    $slug = str_replace(' ', '_', $response_d['Name']);
    if ($response_d['CategoryId'] == 751) {
        $parent_id = 23;
    } else if ($response_d['CategoryId'] == 767) {
        $parent_id = 19;
    } else if ($response_d['CategoryId'] == 756 || $response_d['CategoryId'] == 759 || $response_d['CategoryId'] == 761) {
        $parent_id = 18;
    } else if ($response_d['CategoryId'] == 760) {
        $parent_id = 16;
    } else if ($response_d['CategoryId'] == 762 || $response_d['CategoryId'] == 752) {
        $parent_id = 21;
    } else if ($response_d['CategoryId'] == 763) {
        $parent_id = 1;
    } else if ($response_d['CategoryId'] == 754) {
        $parent_id = 20;
    } else {
        $parent_id = 15;
    }
    $category_data = array(
        'name' => $response_d['Name'],
        'slug' => $slug,
        'sequence_number' => $slug,
        'is_active' => 1,
        'api_cat_id' => $response_d['CategoryId'],
        'parent_id' => $parent_id
    );
    $is_category_exist = check('category', array(
        'name' => $response_d['Name'],
        'api_cat_id' => $response_d['CategoryId']
    ));
    if (!$is_category_exist) {
        $parent_id = save('category', $category_data);
    } else {
        unset($category_data['parent_id']);
        $parent_id = update('category', $category_data, 'name', $response_d['Name']);
    }
    if ($sub_categories) {
        if (is_array($sub_categories)) {
            foreach ($sub_categories as $sub_category) {
                $slug_child = str_replace(' ', '_', $sub_category->Name);
                $category_child_data = array(
                    'name' => $sub_category->Name,
                    'slug' => $slug_child,
                    'sequence_number' => $slug_child,
                    'parent_id' => $parent_id,
                    'is_active' => 1,
                    'api_cat_id' => $sub_category->CategoryId,
                );
                $is_sub_category_exist = check('category', array(
                    'name' => $sub_category->Name,
                    'api_cat_id' => $sub_category->CategoryId
                ));
                if (!$is_sub_category_exist) {
                    $child_id = save('category', $category_child_data);
                } else {
                    $child_id = update('category', $category_child_data, 'name', $sub_category->Name);
                }
            }
        } else {
            $slug_child = str_replace(' ', '_', $sub_categories->Name);
            $category_child_data = array(
                'name' => $sub_categories->Name,
                'slug' => $slug_child,
                'sequence_number' => $slug_child,
                'parent_id' => $parent_id,
                'is_active' => 1,
                'api_cat_id' => $sub_categories->CategoryId,
            );
            $is_sub_category_exist = check('category', array(
                'name' => $sub_categories->Name,
                'api_cat_id' => $sub_categories->CategoryId
            ));
            if (!$is_sub_category_exist) {
                $child_id = save('category', $category_child_data);
            } else {
                $child_id = update('category', $category_child_data, 'name', $sub_categories->Name);
            }
        }
    }
}

define("WSDL_PROGRAM", "https://api.affili.net/V2.0/PublisherProgram.svc?wsdl");

// Set credentials
$username = '782926'; // the publisher ID
$password = 'tZgLMisgQBVam9V0MdUS'; // the publisher web services password
// Send a request to the Logon Service to get an authentication token

$soapLogon = new SoapClient(WSDL_LOGON);
$token = $soapLogon->Logon(array(
    'Username' => $username,
    'Password' => $password,
    'WebServiceType' => 'Publisher'
        ));

$p = isset($_GET['p']) ? $_GET['p'] : 1;

$PageSize = 10;
$CurrentPage = $p;

$displaySettings = array(
    'PageSize' => $PageSize,
    'CurrentPage' => $CurrentPage
);

$getProgramsQuery = array(
    'PartnershipStatus' => array('Active', 'Waiting', 'Paused', 'Refused', 'NoPartnership', 'Cancelled')
);

//$getProgramsQuery = array(
//    'PartnershipStatus' => array('Active')
//);
// Send request to Publisher Program Service
$soapRequest = new SoapClient(WSDL_PROGRAM);
$response = $soapRequest->GetPrograms(array(
    'CredentialToken' => $token,
    'DisplaySettings' => $displaySettings,
    'GetProgramsQuery' => $getProgramsQuery
        ));

$TotalResults = $response->TotalResults;
if ((array) $response->ProgramCollection) {
    $response_data = $response->ProgramCollection->Program;
    foreach ($response_data as $response_d) {
        $category_ids = $response_d->ProgramCategoryIds->int;
        foreach ($category_ids as $category_id) {
            $which_category = check('category', array(
                'api_cat_id' => $category_id,
            ));
            $CommissionTypes = isset($response_d->CommissionTypes) ? $response_d->CommissionTypes->CommissionTypeDetail : '';
            $CommissionTypes_min = isset($response_d->CommissionTypes) ? $CommissionTypes->VolumeMin : '';
            $CommissionTypes_max = isset($CommissionTypes->VolumeMin) ? $CommissionTypes->VolumeMax : '';
            $CommissionTypes_unit = isset($CommissionTypes->Unit) ? $CommissionTypes->Unit : '';
            $rand = rand();
            $logo_name = $root . '/public/store_logos/logo_' . $rand;
            $ch = curl_init($response_d->LogoURL);
            $fp = fopen($logo_name, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
            if ($CommissionTypes_min && $CommissionTypes_max && $CommissionTypes_unit) {
                $cashback = $CommissionTypes_min . '-' . $CommissionTypes_max . $CommissionTypes_unit;
            } else {
                $cashback = '';
            }
            $data_company = array(
                'name' => $response_d->ProgramTitle,
                'cat_id' => $which_category['id'],
                'product_name' => $response_d->ProgramTitle,
                'slug' => $rand,
                'detail' => $response_d->ProgramDescription,
                'logo' => 'logo_' . $rand,
                'affiliate_link' => $response_d->ProgramURL,
                'affiliate_name' => $response_d->ProgramTitle,
                'cashback' => $cashback,
                'is_active' => 1,
                'api_product_id' => $response_d->ProgramId
            );
            $same_category_field_exist = check('company', array(
                'name' => $response_d->ProgramTitle,
                'cat_id' => $which_category['id'],
                'api_product_id' => $response_d->ProgramId
            ));
            if (!$same_category_field_exist) {
                save('company', $data_company);
            } else {
                $data_company['id'] = $same_category_field_exist['id'];
                update('company', $data_company);
            }
        }
    }
    $new_p = $p + 1;
    header('Location:http://' . $_SERVER['HTTP_HOST'] . '/cashback/cron_job/Affilinet.php/?p=' . $new_p);
}
echo 'Cron Job Done.';
?>