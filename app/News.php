<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = ['name','slug','news_content','file','link','position','is_active','date_add','date_upd'];
 
	
	 public $timestamps = false;
    
}
