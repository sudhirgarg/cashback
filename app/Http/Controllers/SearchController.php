<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use Mail;
use App\Content;
use App\Category;
use App\Company;
use App\Bannerimg;
use App\Store;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;

class SearchController extends MainAdminController {

    public function __construct() {
        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return redirect('account');
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }
    }

    public function index(Request $request) {
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $cats = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get();
        $links = Company::where('is_active', 1)->orderBy('id', 'desc')->take(12)->groupby('product_name')->distinct()->get();
        $loginid = Auth::id();
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        include 'Paginator.php';

        /* search query */
        $name = $request->input('search');
        $check_store = Store::where('name', $name)->take(1)->get();
        if (!empty($check_store[0])) {
            $store_id = $check_store[0]->id;
            $krows = \Illuminate\Support\Facades\DB::table('company')->where('name', $store_id)->where('is_active', '1')->count();
            $pages = new Paginator('12', 'p');
            $pages->set_total($krows);

            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('name', $store_id)->where('is_active', '1')->orderBy('date_upd', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->get();
            $data['page_links'] = $pages->page_links('?');
        } elseif (empty($check_store[0])) {
            $check_cat = Category::where('name', $name)->take(1)->get();
            if (!empty($check_cat[0])) {
                $cat_id = $check_cat[0]->id;
                $krows = \Illuminate\Support\Facades\DB::table('company')->where('cat_id', $cat_id)->where('is_active', '1')->count();
                $pages = new Paginator('12', 'p');
                $pages->set_total($krows);
                $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('cat_id', $cat_id)->where('is_active', '1')->orderBy('date_upd', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->get();
                $data['page_links'] = $pages->page_links('?');
            }
            if (empty($check_cat[0])) {
                $krows = \Illuminate\Support\Facades\DB::table('company')->where('affiliate_name', 'like', '%' . $name . '%')->orWhere('detail', 'like', '%' . $name . '%')->where('is_active', '1')->count();
                $pages = new Paginator('12', 'p');
                $pages->set_total($krows);
                $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('affiliate_name', 'like', '%' . $name . '%')->orWhere('detail', 'like', '%' . $name . '%')->where('is_active', '1')->orderBy('date_upd', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->get();
                $data['page_links'] = $pages->page_links('?');
            }
        }
        if (!empty(Auth::check())) {

            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                $loginid = Auth::id();
                if (!empty(Auth::user()->email)) {
                    $email = Auth::user()->email;
                } else {
                    $email = '';
                }
                return \View('searchlinks', ['menus' => $results, 'email' => $email, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'search_name' => $name, 'links' => $links, 'single_content' => $single_content])->with('searchlinks', $data['links'])->with('searchlinks1', $data['page_links']);
            } else {
                return \View('searchlinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'search_name' => $name, 'links' => $links, 'single_content' => $single_content])->with('searchlinks', $data['links'])->with('searchlinks1', $data['page_links']);
            }
        } else {
            return \View('searchlinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'search_name' => $name, 'links' => $links, 'single_content' => $single_content])->with('searchlinks', $data['links'])->with('searchlinks1', $data['page_links']);
        }
    }
    
    public static function getsubcats($cat_id) {
        $cats = Category::where('is_active', 1)->where('parent_id', '=', $cat_id)->orderBy('id', 'asc')->get();
        $not = '';
        foreach ($cats as $list) {
            $not.=$list;
        }
        if (!empty($not)) {
            return $cats;
        } else {
            $cats = $not;
            return $cats;
        }
    }

    public function sort($search, $var) {
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $cats = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get();
        $links = Company::where('is_active', 1)->orderBy('id', 'desc')->take(12)->groupby('product_name')->distinct()->get();
        $loginid = Auth::id();
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        include 'Paginator.php';

        /* search query */
        $name = $search;
        $check_store = Store::where('name', $name)->take(1)->get();
        if (!empty($check_store[0])) {
            $store_id = $check_store[0]->id;
            $krows = \Illuminate\Support\Facades\DB::table('company')->where('name', $store_id)->where('is_active', '1')->count();
            $pages = new Paginator('12', 'p');
            $pages->set_total($krows);
            if ($var == 'date') {
                $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('name', $store_id)->where('is_active', '1')->orderBy('date_add', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('product_name')->distinct()->get();
            }
            if ($var == 'asc') {
                $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('name', $store_id)->where('is_active', '1')->orderBy('affiliate_name', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('product_name')->distinct()->get();
            }
            if ($var == 'des') {
                $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('name', $store_id)->where('is_active', '1')->orderBy('affiliate_name', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('product_name')->distinct()->get();
            }
            $data['page_links'] = $pages->page_links('?');
        } elseif (empty($check_store[0])) {
            $check_cat = Category::where('name', $name)->take(1)->get();
            if (!empty($check_cat[0])) {
                $cat_id = $check_cat[0]->id;
                $krows = \Illuminate\Support\Facades\DB::table('company')->where('cat_id', $cat_id)->where('is_active', '1')->count();
                $pages = new Paginator('12', 'p');
                $pages->set_total($krows);
                if ($var == 'date') {
                    $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('cat_id', $cat_id)->where('is_active', '1')->orderBy('date_add', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('product_name')->distinct()->get();
                }
                if ($var == 'asc') {
                    $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('cat_id', $cat_id)->where('is_active', '1')->orderBy('affiliate_name', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('product_name')->distinct()->get();
                }
                if ($var == 'des') {
                    $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('cat_id', $cat_id)->where('is_active', '1')->orderBy('affiliate_name', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('product_name')->distinct()->get();
                }
                $data['page_links'] = $pages->page_links('?');
            }
            if (empty($check_cat[0])) {
                $krows = \Illuminate\Support\Facades\DB::table('company')->where('affiliate_name', 'like', '%' . $name . '%')->orWhere('detail', 'like', '%' . $name . '%')->where('is_active', '1')->count();
                $pages = new Paginator('12', 'p');
                $pages->set_total($krows);
                if ($var == 'date') {
                    $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('affiliate_name', 'like', '%' . $name . '%')->orWhere('detail', 'like', '%' . $name . '%')->where('is_active', '1')->orderBy('date_add', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('product_name')->distinct()->get();
                }
                if ($var == 'asc') {
                    $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('affiliate_name', 'like', '%' . $name . '%')->orWhere('detail', 'like', '%' . $name . '%')->where('is_active', '1')->orderBy('affiliate_name', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('product_name')->distinct()->get();
                }
                if ($var == 'des') {
                    $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('affiliate_name', 'like', '%' . $name . '%')->orWhere('detail', 'like', '%' . $name . '%')->where('is_active', '1')->orderBy('affiliate_name', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('product_name')->distinct()->get();
                }
                $data['page_links'] = $pages->page_links('?');
            }
        }
        if (!empty(Auth::check())) {

            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                $loginid = Auth::id();
                if (!empty(Auth::user()->email)) {
                    $email = Auth::user()->email;
                } else {
                    $email = '';
                }
                return \View('searchlinks', ['menus' => $results, 'email' => $email, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'search_name' => $name, 'links' => $links, 'single_content' => $single_content])->with('searchlinks', $data['links'])->with('searchlinks1', $data['page_links']);
            } else {
                return \View('searchlinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'search_name' => $name, 'links' => $links, 'single_content' => $single_content])->with('searchlinks', $data['links'])->with('searchlinks1', $data['page_links']);
            }
        } else {
            return \View('searchlinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'search_name' => $name, 'links' => $links, 'single_content' => $single_content])->with('searchlinks', $data['links'])->with('searchlinks1', $data['page_links']);
        }
    }

}
