<?php

namespace App\Http\Controllers\admin;

use Auth;
use File;
use App\Store;
use App\News;
use App\Company;
use App\Companycategory;
use App\Category;
use App\User;
use App\Transferbyadmin;
use App\Userredeem;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;

class StoreController extends MainAdminController {

    public function __construct() {

	if (!empty(Auth::check())) {
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return redirect('admin/dashboard');
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
    }

    public function index() {
	/* store count start */
	$allstore = DB::table('store')->get();
	$allstore = count($allstore);
	/* news count end */
	/* news count start */
	$allnews = DB::table('news')->get();
	$allnews = count($allnews);
	/* news count end */
	/* categories count start */
	$categories = DB::table('category')->get();
	$categories_count = count($categories);
	/* categories count end */
	/* Affiliate count start */
	$affiliate_links = DB::table('company')->get();
	$affiliate_count = count($affiliate_links);
	/* Affiliate count end */
	/* Users count start */
	$users_count = DB::table('User')->get();
	$users_count = count($users_count);
	/* Users count end */
	$alltrasfers =DB::table('transfer_by_admin')->get();
	$alltrasfers=count($alltrasfers);
	$request =DB::table('user_redeem')->where('status',0)->get();
	$request=count($request);
	$array=[
		'allnews'=>$allnews,	
		'categories_count'=>$categories_count,
		'affiliate_count'=>$affiliate_count,
		'users_count'=>$users_count,
		'alltrasfers'=>$alltrasfers,
		'request'=>$request	
	];
	if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.addstore',['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
    }

    public function addstore(Request $request) {
	$errors = $this->validate($request, [
	    'name' => 'required',
	]);

	$store = new Store(array(
	    'name' => $request->input('name'),
	    'slug'  => $request->input('name').''.rand(11111,99999),
	    'is_active' => $request->input('is_active'),
	    'date_add' => date('Y-m-d h:i:s')
	));
	$store->save();
	$request->session()->flash('alert-success', 'Store successfully added!');
	return redirect('admin/storelist');
    }

    public function editstore(Request $request, $id) {
	/* store count start */
	$allstore = DB::table('store')->get();
	$allstore = count($allstore);
	/* store count end */
	/* news count start */
	$allnews = DB::table('news')->get();
	$allnews = count($allnews);
	/* news count end */
	/* categories count start */
	$categories = DB::table('category')->get();
	$categories_count = count($categories);
	/* categories count end */
	/* Affiliate count start */
	$affiliate_links = DB::table('company')->get();
	$affiliate_count = count($affiliate_links);
	/* Affiliate count end */
	/* Users count start */
	$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
	/* Users count end */
	$alltrasfers =DB::table('transfer_by_admin')->get();
	$alltrasfers=count($alltrasfers);
	$request =DB::table('user_redeem')->where('status',0)->get();
	$request=count($request);
	$array=[
		'allnews'=>$allnews,	
		'categories_count'=>$categories_count,
		'affiliate_count'=>$affiliate_count,
		'users_count'=>$users_count,
		'alltrasfers'=>$alltrasfers,
		'request'=>$request	
	];
	$store_detail = DB::table('store')->where('id', '=', $id)->get();
	if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.editstore',['store_detail' => $store_detail],['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
    }

    public function updatestore(Request $request, $id) {
	$store_detail = DB::table('store')->where('id', '=', $id)->get();
	
	$errors = $this->validate($request, [
	    'name' => 'required',
	]);

	if ($request->file('file') == '') {
	    $store = Store::findOrFail($id);
	    $store->name = $request->input('name');

	   
	    $store->is_active = $request->input('is_active');
	    $store->save();
	    $request->session()->flash('alert-success', 'Store successfully updated !');
	    return redirect('admin/storelist');
	}
	return view('admin.editstore', ['store_detail' => $store_detail]);
    }

    public function deletestore(Request $request, $id) {
	DB::table('store')->delete($id);
	$request->session()->flash('alert-success', 'Record successfully deleted !');
	return redirect('admin/storelist');
    }

    public function updatestatus(Request $request, $id, $status) {
	$store = Store::findOrFail($id);
	$store->is_active = $status;
	$store->save();
	$request->session()->flash('alert-success', 'Status successfully updated !');
	return redirect('admin/storelist');
    }

    public function storelist() {
	/* store count start */
	$allstore = DB::table('store')->get();
	$allstore = count($allstore);
	/* store count end */
	/* news count start */
	$allnews = DB::table('news')->get();
	$allnews = count($allnews);
	/* news count end */
	/* categories count start */
	$categories = DB::table('category')->get();
	$categories_count = count($categories);
	/* categories count end */
	/* Affiliate count start */
	$affiliate_links = DB::table('company')->get();
	$affiliate_count = count($affiliate_links);
	/* Affiliate count end */
	/* Users count start */
	$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
	/* Users count end */
	$alltrasfers =DB::table('transfer_by_admin')->get();
	$alltrasfers=count($alltrasfers);
	$request =DB::table('user_redeem')->where('status',0)->get();
	$request=count($request);
	$array=[
		'allnews'=>$allnews,	
		'categories_count'=>$categories_count,
		'affiliate_count'=>$affiliate_count,
		'users_count'=>$users_count,
		'alltrasfers'=>$alltrasfers,
		'request'=>$request	
	];
	$store = DB::table('store')->get();
	if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.storelist',['store' => $store],['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
    }
    public function updatestorecat_storelist(Request $request,$id)
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('store')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$category = DB::table('category')->where('parent_id', '=', '0')->get();
		$company = DB::table('store')->get();
		$store_cat=DB::table('company_category')->where('store_id', '=', $id)->get();
		$new_arr=array();
		foreach($store_cat as $cat){
			$new_arr[]=$cat->category_id;
		}
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.updatestorecat_storelist',['counts' => $array],['category' => $category,'company' => $company,'storeid' => $id,'store_cat' => $store_cat,'new_arr'=>$new_arr,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}
	
	public function updatestorecat(Request $request,$id)
	{
		$errors=$this->validate($request, [
    			'store_name' => 'required',
    			'category' => 'required'
		]);
		Companycategory::where('store_id','=',$id)->delete();
		foreach($request->input('category') as $list){
			$category_company = new Companycategory(array(
      			'store_id' => $id,
      			'category_id'  => $list	
    			));
    			$category_company->save();
		}
		$request->session()->flash('alert-success', 'Categories successfully Updated of store!');
		return redirect('admin/storelist');
	}	

	public static function getsubcats($cat_id){
		$cats = Category::where('is_active', 1)->where('parent_id','=', $cat_id)->orderBy('id','asc')->get();
		$not='';		
		foreach($cats as $list){
			$not.=$list;
		}
		 if(!empty($not)){
			return $cats;
		}else{
			$cats=$not;
			return $cats;
		}
	}

	  
}
