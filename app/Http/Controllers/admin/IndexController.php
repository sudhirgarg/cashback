<?php

namespace App\Http\Controllers\admin;

use Auth;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;
class IndexController extends MainAdminController
{
	public function __construct(){
		if (!empty(Auth::check())) {
			$loginid = Auth::id();    
		     	$authenticate=User::select('User.email')->where('User.id',$loginid)->where('type','admin')->get();
			return redirect('admin/dashboard'); 
		}
		else{
			return redirect('admin/'); 
		}

		if (!empty(Auth::check())) {
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
		
			if(!empty($authenticate[0])){
            			return redirect('admin/dashboard');
			}
			else{
				return redirect('admin/');
			}
		}else{
			return redirect('admin/');
		}
         }
	
    	public function index(){
		if (!empty(Auth::check())) {
				$loginid = Auth::id();    
		     		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
		
				if(!empty($authenticate[0])){
		    			return redirect('admin/dashboard');
				}
				else{
					return view('admin.index');
				}
			}else{
				return view('admin.index');
			}

		}

	public function postLogin(Request $request){
		$this->validate($request, ['email' => 'required|email', 
					   'password' => 'required']
		);
		$userdata = array(
			'email'     => $request->input('email'),
        		'password'  => $request->input('password'),
			'type'  => 'admin',
			'is_active'  => '1'	
    		);

		if (Auth::attempt($userdata)) {
			return $this->handleUserWasAuthenticated($userdata);
		}else{        
			$request->session()->flash('alert-danger', 'Email/Password is not correct. Please try again !'); 
		       	return redirect('admin');
		}
	}


   	protected function handleUserWasAuthenticated($userdata){
		if (method_exists($this, 'authenticated')) {
			return $this->authenticated($userdata, Auth::user());	
		}
		return redirect('admin/dashboard'); 
    	}

    	public function logout(){
        	Auth::logout();
        	return redirect('admin');
    	}
	
	  
}
