<?php

namespace App\Http\Controllers\admin;

use Auth;
use File;
use App\Company;
use App\News;
use App\Category;
use App\User;
use App\Transactions;
use App\Cashbackclaimrequest;
use App\Transferbyadmin;
use App\Userredeem;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;

class ClaimfiledController extends MainAdminController {

    public function __construct() {

        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return redirect('admin/dashboard');
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function index() {
        /* news count start */
        $allnews = DB::table('news')->get();
        $allnews = count($allnews);
        /* news count end */
        /* categories count start */
        $categories = DB::table('category')->get();
        $categories_count = count($categories);
        /* categories count end */
        /* Affiliate count start */
        $affiliate_links = DB::table('company')->get();
        $affiliate_count = count($affiliate_links);
        /* Affiliate count end */
        /* Users count start */
        $users_count = DB::table('User')->where('type', 'user')->get();
        $users_count = count($users_count);
        /* Users count end */
        $alltrasfers = DB::table('transfer_by_admin')->get();
        $alltrasfers = count($alltrasfers);
        $request = DB::table('user_redeem')->where('status', 0)->get();
        $request = count($request);
        $array = [
            'allnews' => $allnews,
            'categories_count' => $categories_count,
            'affiliate_count' => $affiliate_count,
            'users_count' => $users_count,
            'alltrasfers' => $alltrasfers,
            'request' => $request
        ];
        $user_claimfiled = DB::table("cashback_claim_request")->join('User', 'User.id', '=', 'cashback_claim_request.user_name')->select('User.firstname', 'User.lastname', 'cashback_claim_request.id', 'cashback_claim_request.type_claim', 'cashback_claim_request.select_shop', 'cashback_claim_request.select_visit', 'cashback_claim_request.client_number', 'cashback_claim_request.order_of', 'cashback_claim_request.status', 'cashback_claim_request.date_add')->get();
        if (!empty(Auth::check())) {
            if (!empty(Auth::user()->email)) {
                $email = Auth::user()->email;
            } else {
                $email = '';
            }
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return view('admin.claimfiled', ['user_claimfiled' => $user_claimfiled], ['counts' => $array, 'email' => $email]);
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function updateclaimfilestatus(Request $request, $id, $status) {
        $user = Cashbackclaimrequest::findOrFail($id);
        $user->status = $status;
        $user->save();
        $request->session()->flash('alert-success', 'Status successfully updated !');
        return redirect('admin/claimfiled');
    }

    public function viewclaimdetail(Request $request, $id) {
        /* news count start */
        $allnews = DB::table('news')->get();
        $allnews = count($allnews);
        /* news count end */
        /* categories count start */
        $categories = DB::table('category')->get();
        $categories_count = count($categories);
        /* categories count end */
        /* Affiliate count start */
        $affiliate_links = DB::table('company')->get();
        $affiliate_count = count($affiliate_links);
        /* Affiliate count end */
        /* Users count start */
        $users_count = DB::table('User')->get();
        $users_count = count($users_count);
        /* Users count end */
        $alltrasfers = DB::table('transfer_by_admin')->get();
        $alltrasfers = count($alltrasfers);
        $request = DB::table('user_redeem')->where('status', 0)->get();
        $request = count($request);
        $array = [
            'allnews' => $allnews,
            'categories_count' => $categories_count,
            'affiliate_count' => $affiliate_count,
            'users_count' => $users_count,
            'alltrasfers' => $alltrasfers,
            'request' => $request
        ];
        $view_detail = DB::table('cashback_claim_request')->where('id', '=', $id)->get();
        if (!empty(Auth::check())) {
            if (!empty(Auth::user()->email)) {
                $email = Auth::user()->email;
            } else {
                $email = '';
            }
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return view('admin.viewclaimdetail', ['view_detail' => $view_detail], ['counts' => $array, 'email' => $email]);
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

}
