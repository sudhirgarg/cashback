<?php

namespace App\Http\Controllers\admin;

use Auth;
use App\Category;
use App\Company;
use App\News;
use App\User;
use App\Transferbyadmin;
use App\Userredeem;
use App\Emailtemplate;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;
class EmailController extends MainAdminController
{
	public function __construct()
    {
		
		if (!empty(Auth::check())) {
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return redirect('admin/dashboard');
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
        }
         

    public function index()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.addemail',['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

    public function emaillist()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$template = DB::table('email_template')->get();
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.emaillist',['template' => $template],['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

    public function addemail(Request $request)
	{
		$errors=$this->validate($request, [
    			'name' => 'required',
    			'content' => 'required'
		]);
		$slug=str_replace(" ","",$request->input('name'));
		$slug=str_replace("/","",$slug);
		$slug=str_replace(",","",$slug);
		$slug=str_replace("'","",$slug);
		$slug=str_replace("+","",$slug);
		$slug=str_replace("  ","",$slug);
		$slug=str_replace("&","",$slug);
		$template = new Emailtemplate(array(
      			'name' => $request->input('name'),
      			'slug'  => $slug.''.rand(11111,99999),
			'content' =>$request->input('content'),
			'is_active' => $request->input('is_active')	
    		));
    		$template->save();
		$request->session()->flash('alert-success', 'Email Template successfully added!');
		return redirect('admin/emaillist');
	}
  
    public function updatetemplatestatus(Request $request,$id,$status)
	{
		$email = Emailtemplate::findOrFail($id);
		$email->is_active = $status;
		$email->save();
		$request->session()->flash('alert-success', 'Status successfully updated !');
		return redirect('admin/emaillist');
	}

    public function deleteemail(Request $request,$id){
		DB::table("email_template")->delete($id);
		$request->session()->flash('alert-success', 'Record successfully deleted !');
		return redirect('admin/emaillist');
	}

    public function editemail(Request $request,$id){
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$email_detail=DB::table('email_template')->where('id', '=', $id)->get();
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.editemail',['email_detail' => $email_detail],['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

    public function updateemail(Request $request,$id){
		$errors=$this->validate($request, [
    			'name' => 'required',
    			'content' => 'required',
		]);
		$email = Emailtemplate::findOrFail($id);
		$email->name   = $request->input('name');
		$email->content = $request->input('content');
		$email->is_active = $request->input('is_active');
		$email->save();
		$request->session()->flash('alert-success', 'Email Template successfully updated !');
		return redirect('admin/emaillist');	
	}

	  
}
