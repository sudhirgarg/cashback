<?php

namespace App\Http\Controllers\admin;

use Auth;
use App\Category;
use App\Company;
use App\News;
use App\User;
use App\Transferbyadmin;
use App\Userredeem;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;

class CategoryController extends MainAdminController {

    public function __construct() {

        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return redirect('admin/dashboard');
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function index() {
        /* news count start */
        $allnews = DB::table('news')->get();
        $allnews = count($allnews);
        /* news count end */
        /* categories count start */
        $categories = DB::table('category')->get();
        $categories_count = count($categories);
        /* categories count end */
        /* Affiliate count start */
        $affiliate_links = DB::table('company')->get();
        $affiliate_count = count($affiliate_links);
        /* Affiliate count end */
        /* Users count start */
        $users_count = DB::table('User')->where('type', 'user')->get();
        $users_count = count($users_count);
        /* Users count end */
        $alltrasfers = DB::table('transfer_by_admin')->get();
        $alltrasfers = count($alltrasfers);
        $request = DB::table('user_redeem')->where('status', 0)->get();
        $request = count($request);
        $array = [
            'allnews' => $allnews,
            'categories_count' => $categories_count,
            'affiliate_count' => $affiliate_count,
            'users_count' => $users_count,
            'alltrasfers' => $alltrasfers,
            'request' => $request
        ];
        if (!empty(Auth::check())) {
            if (!empty(Auth::user()->email)) {
                $email = Auth::user()->email;
            } else {
                $email = '';
            }
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return view('admin.addcategory', ['counts' => $array, 'email' => $email]);
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function categorylist() {
        /* news count start */
        $allnews = DB::table('news')->get();
        $allnews = count($allnews);
        /* news count end */
        /* categories count start */
        $categories = DB::table('category')->get();
        $categories_count = count($categories);
        /* categories count end */
        /* Affiliate count start */
        $affiliate_links = DB::table('company')->get();
        $affiliate_count = count($affiliate_links);
        /* Affiliate count end */
        /* Users count start */
        $users_count = DB::table('User')->where('type', 'user')->get();
        $users_count = count($users_count);
        /* Users count end */
        $alltrasfers = DB::table('transfer_by_admin')->get();
        $alltrasfers = count($alltrasfers);
        $request = DB::table('user_redeem')->where('status', 0)->get();
        $request = count($request);
        $array = [
            'allnews' => $allnews,
            'categories_count' => $categories_count,
            'affiliate_count' => $affiliate_count,
            'users_count' => $users_count,
            'alltrasfers' => $alltrasfers,
            'request' => $request
        ];
        $category = DB::table('category')->get();
        if (!empty(Auth::check())) {
            if (!empty(Auth::user()->email)) {
                $email = Auth::user()->email;
            } else {
                $email = '';
            }
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return view('admin.categorylist', ['category' => $category], ['counts' => $array, 'email' => $email]);
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function addcategory(Request $request) {
        $errors = $this->validate($request, [
            'name' => 'required',
            'sequence_number' => 'required'
        ]);
        $slug = str_replace(" ", "_", $request->input('name'));
        $slug = str_replace("/", "", $slug);
        $slug = str_replace(",", "", $slug);
        $slug = str_replace("'", "", $slug);
        $slug = str_replace("+", "", $slug);
        $slug = str_replace("  ", "", $slug);
        $slug = str_replace("&", "", $slug);
        $category = new Category(array(
            'name' => $request->input('name'),
            'slug' => $slug,
            'sequence_number' => $request->input('sequence_number'),
            'is_active' => $request->input('is_active'),
            'date_add' => date('Y-m-d h:i:s')
        ));
        $category->save();
        $request->session()->flash('alert-success', 'Category successfully added!');
        return redirect('admin/categorylist');
    }

    public function updatecatstatus(Request $request, $id, $status) {
        $category = Category::findOrFail($id);
        $category->is_active = $status;
        $category->save();
        $request->session()->flash('alert-success', 'Status successfully updated !');
        return redirect('admin/categorylist');
    }

    public function deletecat(Request $request, $id) {
        DB::table("category")->delete($id);
        $request->session()->flash('alert-success', 'Record successfully deleted !');
        return redirect('admin/categorylist');
    }

    public function editcat(Request $request, $id) {
        /* news count start */
        $allnews = DB::table('news')->get();
        $allnews = count($allnews);
        /* news count end */
        /* categories count start */
        $categories = DB::table('category')->get();
        $categories_count = count($categories);
        /* categories count end */
        /* Affiliate count start */
        $affiliate_links = DB::table('company')->get();
        $affiliate_count = count($affiliate_links);
        /* Affiliate count end */
        /* Users count start */
        $users_count = DB::table('User')->where('type', 'user')->get();
        $users_count = count($users_count);
        /* Users count end */
        $alltrasfers = DB::table('transfer_by_admin')->get();
        $alltrasfers = count($alltrasfers);
        $request = DB::table('user_redeem')->where('status', 0)->get();
        $request = count($request);
        $array = [
            'allnews' => $allnews,
            'categories_count' => $categories_count,
            'affiliate_count' => $affiliate_count,
            'users_count' => $users_count,
            'alltrasfers' => $alltrasfers,
            'request' => $request
        ];
        $category_detail = DB::table('category')->where('id', '=', $id)->get();
        if (!empty(Auth::check())) {
            if (!empty(Auth::user()->email)) {
                $email = Auth::user()->email;
            } else {
                $email = '';
            }
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return view('admin.editcategory', ['category_detail' => $category_detail], ['counts' => $array, 'email' => $email]);
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function updatecategory(Request $request, $id) {
        $errors = $this->validate($request, [
            'name' => 'required',
            'sequence_number' => 'required',
        ]);
        $cat = Category::findOrFail($id);
        $cat->name = $request->input('name');
        $cat->sequence_number = $request->input('sequence_number');
        $cat->is_active = $request->input('is_active');
        $cat->save();
        $request->session()->flash('alert-success', 'Category successfully updated !');
        return redirect('admin/categorylist');
    }

    public static function getsubcats($cat_id) {
        $cats = Category::where('is_active', 1)->where('parent_id', '=', $cat_id)->orderBy('id', 'asc')->get();
        $not = '';
        foreach ($cats as $list) {
            $not.=$list;
        }
        if (!empty($not)) {
            return $cats;
        } else {
            $cats = $not;
            return $cats;
        }
    }

    public static function getparentname($parent_id) {
        $parent_cat = Category::where('id', '=', $parent_id)->get();
        return $parent_cat;
    }

}
