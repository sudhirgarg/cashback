<?php

namespace App\Http\Controllers\admin;

use Auth;
use File;
use App\News;
use App\Company;
use App\Category;
use App\User;
use App\Transferbyadmin;
use App\Userredeem;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;
class NewsController extends MainAdminController
{
	public function __construct()
    {
		
		 if (!empty(Auth::check())) {
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return redirect('admin/dashboard');
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
        }
         

    public function index()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.addnews',['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

    public function addnews(Request $request)
	{
		$errors=$this->validate($request, [
    			'name' => 'required',
    			'news_content' => 'required',
    			'file' => 'required|mimes:jpeg,png,jpg|image|max:5000'
		]);
		$destinationPath = base_path() . '/public/news_images/'; // upload path
      		$extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
      		$fileName = rand(11111,99999).'.'.$extension; // renameing image
      		$request->file('file')->move($destinationPath, $fileName); // uploading file to given path
		$name=$request->input('name');
		$slug=str_replace(" ","",$name);
		$slug=str_replace(".","",$slug);
		$slug=str_replace(",","",$slug);
		$slug=str_replace("_","",$slug);
		$slug=str_replace("  ","",$slug);
		$news = new News(array(
      			'name' => $request->input('name'),
      			'slug'  => $slug.''.rand(11111,99999),
			'news_content' =>$request->input('news_content'),
			'link' => $request->input('link'),
			'is_active' => $request->input('is_active'),
			'file' => $fileName,
			'date_add' => date('Y-m-d h:i:s')	
    		));
    		$news->save();
		$request->session()->flash('alert-success', 'News successfully added!');
		return redirect('admin/newslist');
	}

    public function editnews(Request $request,$id)
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$news_detail=DB::table('news')->where('id', '=', $id)->get();
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.editnews',['news_detail' => $news_detail],['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}


    public function updatenews(Request $request,$id)
	{
		$news_detail=DB::table('news')->where('id', '=', $id)->get();
		$errors=$this->validate($request, [
    			'name' => 'required',
    			'news_content' => 'required',
		]);
		if($request->file('file')!=''){
			$errors=$this->validate($request, [
	    			'file' => 'required|mimes:jpeg,png,jpg|image|max:5000',
			]);
			$destinationPath = base_path() . '/public/news_images/'; // upload path
      			$extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
      			$fileName = rand(11111,99999).'.'.$extension; // renameing image
      			$request->file('file')->move($destinationPath, $fileName); // uploading file to given path
			File::delete('news_images/' . $news_detail[0]->file);			
			$news = News::findOrFail($id);
		    	$news->name   = $request->input('name');
		    	$news->news_content = $request->input('news_content');
		    	$news->link    = $request->input('link');
		    	$news->is_active = $request->input('is_active');
			$news->file    = $fileName;
		    	$news->save();
		    	$request->session()->flash('alert-success', 'News successfully updated !');
		    	return redirect('admin/newslist');
		}
		if($request->file('file')==''){
		    $news = News::findOrFail($id);
		    $news->name   = $request->input('name');
		    $news->news_content          = $request->input('news_content');
		    $news->link    = $request->input('link');
		    $news->is_active = $request->input('is_active');
		    $news->save();
		    $request->session()->flash('alert-success', 'News successfully updated !');
		    return redirect('admin/newslist');	
		}
		return view('admin.editnews',['news_detail' => $news_detail]);
	}
	
	public function deletenews(Request $request,$id)
	{	
		DB::table('news')->delete($id);
		$request->session()->flash('alert-success', 'Record successfully deleted !');
		return redirect('admin/newslist');	
	}

	public function updatestatus(Request $request,$id,$status)
	{	
		$news = News::findOrFail($id);
		$news->is_active = $status;
		$news->save();
		$request->session()->flash('alert-success', 'Status successfully updated !');
		return redirect('admin/newslist');	
	}
		

    public function newslist()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$news = DB::table('news')->get();
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.newslist',['news' => $news],['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}	

	  
}
