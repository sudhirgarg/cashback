<?php

namespace App\Http\Controllers\admin;

use Auth;
use App\Category;
use App\Company;
use App\News;
use App\User;
use App\Faq;
use App\Transferbyadmin;
use App\Userredeem;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;

class FaqController extends MainAdminController {

    public function __construct() {

        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return redirect('admin/dashboard');
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function index() {
        /* news count start */
        $allnews = DB::table('news')->get();
        $allnews = count($allnews);
        /* news count end */
        /* categories count start */
        $categories = DB::table('category')->get();
        $categories_count = count($categories);
        /* categories count end */
        /* Affiliate count start */
        $affiliate_links = DB::table('company')->get();
        $affiliate_count = count($affiliate_links);
        /* Affiliate count end */
        /* Users count start */
        $users_count = DB::table('User')->where('type', 'user')->get();
        $users_count = count($users_count);
        /* Users count end */
        $alltrasfers = DB::table('transfer_by_admin')->get();
        $alltrasfers = count($alltrasfers);
        $request = DB::table('user_redeem')->where('status', 0)->get();
        $request = count($request);
        $array = [
            'allnews' => $allnews,
            'categories_count' => $categories_count,
            'affiliate_count' => $affiliate_count,
            'users_count' => $users_count,
            'alltrasfers' => $alltrasfers,
            'request' => $request
        ];
        if (!empty(Auth::check())) {
            if (!empty(Auth::user()->email)) {
                $email = Auth::user()->email;
            } else {
                $email = '';
            }
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return view('admin.addfaq', ['counts' => $array, 'email' => $email]);
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function faqlist() {
        /* news count start */
        $allnews = DB::table('news')->get();
        $allnews = count($allnews);
        /* news count end */
        /* categories count start */
        $categories = DB::table('category')->get();
        $categories_count = count($categories);
        /* categories count end */
        /* Affiliate count start */
        $affiliate_links = DB::table('company')->get();
        $affiliate_count = count($affiliate_links);
        /* Affiliate count end */
        /* Users count start */
        $users_count = DB::table('User')->where('type', 'user')->get();
        $users_count = count($users_count);
        /* Users count end */
        $alltrasfers = DB::table('transfer_by_admin')->get();
        $alltrasfers = count($alltrasfers);
        $request = DB::table('user_redeem')->where('status', 0)->get();
        $request = count($request);
        $array = [
            'allnews' => $allnews,
            'categories_count' => $categories_count,
            'affiliate_count' => $affiliate_count,
            'users_count' => $users_count,
            'alltrasfers' => $alltrasfers,
            'request' => $request
        ];
        $faq = DB::table('faq')->get();
        if (!empty(Auth::check())) {
            if (!empty(Auth::user()->email)) {
                $email = Auth::user()->email;
            } else {
                $email = '';
            }
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return view('admin.faqlist', ['faq' => $faq], ['counts' => $array, 'email' => $email]);
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function addfaq(Request $request) {
        $errors = $this->validate($request, [
            'name' => 'required',
            'detail' => 'required'
        ]);
        $faq = new Faq(array(
            'name' => $request->input('name'),
            'detail' => $request->input('detail'),
            'is_active' => $request->input('is_active')
        ));
        $faq->save();
        $request->session()->flash('alert-success', 'FAQ successfully added!');
        return redirect('admin/faqlist');
    }

    public function updatefstatus(Request $request, $id, $status) {
        $faq = Faq::findOrFail($id);
        $faq->is_active = $status;
        $faq->save();
        $request->session()->flash('alert-success', 'Status successfully updated !');
        return redirect('admin/faqlist');
    }

    public function deletefaq(Request $request, $id) {
        DB::table("faq")->delete($id);
        $request->session()->flash('alert-success', 'Record successfully deleted !');
        return redirect('admin/faqlist');
    }

    public function editfaq(Request $request, $id) {
        /* news count start */
        $allnews = DB::table('news')->get();
        $allnews = count($allnews);
        /* news count end */
        /* categories count start */
        $categories = DB::table('category')->get();
        $categories_count = count($categories);
        /* categories count end */
        /* Affiliate count start */
        $affiliate_links = DB::table('company')->get();
        $affiliate_count = count($affiliate_links);
        /* Affiliate count end */
        /* Users count start */
        $users_count = DB::table('User')->where('type', 'user')->get();
        $users_count = count($users_count);
        /* Users count end */
        $alltrasfers = DB::table('transfer_by_admin')->get();
        $alltrasfers = count($alltrasfers);
        $request = DB::table('user_redeem')->where('status', 0)->get();
        $request = count($request);
        $array = [
            'allnews' => $allnews,
            'categories_count' => $categories_count,
            'affiliate_count' => $affiliate_count,
            'users_count' => $users_count,
            'alltrasfers' => $alltrasfers,
            'request' => $request
        ];
        $faq_detail = DB::table('faq')->where('id', '=', $id)->get();
        if (!empty(Auth::check())) {
            if (!empty(Auth::user()->email)) {
                $email = Auth::user()->email;
            } else {
                $email = '';
            }
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return view('admin.editfaq', ['faq_detail' => $faq_detail], ['counts' => $array, 'email' => $email]);
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function updatefaq(Request $request, $id) {
        $errors = $this->validate($request, [
            'name' => 'required',
            'detail' => 'required',
        ]);
        $faq = Faq::findOrFail($id);
        $faq->name = $request->input('name');
        $faq->detail = $request->input('detail');
        $faq->is_active = $request->input('is_active');
        $faq->save();
        $request->session()->flash('alert-success', 'FAQ successfully updated !');
        return redirect('admin/faqlist');
    }

}
