<?php

namespace App\Http\Controllers;

use Auth;
use App\Category;
use App\Company;
use App\News;
use App\Frontuser;
use App\Transferbyadmin;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;
class AssigncategoryController extends MainAdminController
{
	public function __construct()
    {
		
		 $this->middleware('auth');
        }
         

    public function index()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('front_user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count
			];
		$category = DB::table('category')->get();
		$company = DB::table('company')->get();
		return view('assigncategory',['counts' => $array],['category' => $category,'company' => $company]);
	}

    

    public function assign(Request $request)
	{
		$errors=$this->validate($request, [
    			'store_name' => 'required',
    			'cat' => 'required'
		]);
		echo "<pre>";
		print_r($request);
		exit;
		/*$category = new Category(array(
      			'name' => $request->input('name'),
      			'slug'  => $request->input('name').''.rand(11111,99999),
			'sequence_number' =>$request->input('sequence_number'),
			'is_active' => $request->input('is_active'),
			'date_add' => date('Y-m-d h:i:s')	
    		));
    		$category->save();
		$request->session()->flash('alert-success', 'Category successfully added!');*/
		return redirect('/categorylist');
	}
  
    
    
    		

	  
}
