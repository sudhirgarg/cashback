<?php

namespace App\Http\Controllers\admin;

use Auth;
use File;
use App\Company;
use App\News;
use App\Category;
use App\User;
use App\Transactions;
use App\Cashbackclaimrequest;
use App\Transferbyadmin;
use App\Userredeem;
use App\Iframe;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;

class IframeController extends MainAdminController {

    public function __construct() {

        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                return redirect('admin/dashboard');
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function index() {
        $allnews = DB::table('news')->get();
        $allnews = count($allnews);
        /* news count end */
        /* categories count start */
        $categories = DB::table('category')->get();
        $categories_count = count($categories);
        /* categories count end */
        /* Affiliate count start */
        $affiliate_links = DB::table('company')->get();
        $affiliate_count = count($affiliate_links);
        /* Affiliate count end */
        /* Users count start */
        $users_count = DB::table('User')->get();
        $users_count = count($users_count);
        /* Users count end */
        $alltrasfers = DB::table('transfer_by_admin')->get();
        $alltrasfers = count($alltrasfers);
        $request = DB::table('user_redeem')->where('status', 0)->get();
        $request = count($request);
        $array = [
            'allnews' => $allnews,
            'categories_count' => $categories_count,
            'affiliate_count' => $affiliate_count,
            'users_count' => $users_count,
            'alltrasfers' => $alltrasfers,
            'request' => $request
        ];
        if (!empty(Auth::check())) {
            if (!empty(Auth::user()->email)) {
                $email = Auth::user()->email;
            } else {
                $email = '';
            }
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'admin')->get();
            if (!empty($authenticate[0])) {
                $iframe = DB::table('iframe')->where('id', 1)->first();
                return view('admin.Iframe', ['counts' => $array, 'email' => $email, 'data' => $iframe])->with('iframe', $iframe);
            } else {
                return redirect('admin');
            }
        } else {
            return redirect('admin');
        }
    }

    public function saveiframe(REQUEST $request) {
        $is_active = isset($_POST['is_active']) ? $_POST['is_active'] : 0;
        $iframe = Iframe::findOrFail(1);
        $iframe->name = $request->input('name');
        $iframe->detail = $request->input('detail');
        $iframe->is_active = $is_active;
        $iframe->save();
        $request->session()->flash('alert-success', 'Rating Iframe successfully Updated!');
        return redirect('admin/addiframe');
    }

}
