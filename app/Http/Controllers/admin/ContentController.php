<?php

namespace App\Http\Controllers\admin;

use Auth;
use App\User;
use App\News;
use App\Company;
use App\Category;
use App\Content;
use App\Transaction;
use App\Transferbyadmin;
use App\Userredeem;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;
class ContentController extends MainAdminController
{
	public function __construct()
    {
		
		if (!empty(Auth::check())) {
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return redirect('admin/dashboard');
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
        }
         

    public function index()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.addcontent',['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

    public function add_content(Request $request){
		$errors=$this->validate($request, [
    			'page_name' => 'required',
    			'content_page_detail' => 'required'
		]);
		$name=$request->input('page_name');
		$slug=str_replace(" ","",$name);
		$slug=str_replace(".","",$slug);
		$slug=str_replace(",","",$slug);
		$slug=str_replace("_","",$slug);
		$slug=str_replace("  ","",$slug);
		$content = new Content(array(
      			'name' => $request->input('page_name'),
			'slug'  => $slug.''.rand(11111,99999),
			'page_name' => $request->input('page_name'),
      			'page' =>$request->input('content_page_detail'),
			'is_active' => $request->input('is_active'),
		));
    		$content->save();
		$request->session()->flash('alert-success', 'Content successfully added!');
		return redirect('admin/contentlist');
    }

    public function updatestatus_content(Request $request,$id,$status)
	{
		$content = Content::findOrFail($id);
		$content->is_active = $status;
		$content->save();
		$request->session()->flash('alert-success', 'Status successfully updated !');
		return redirect('admin/contentlist');
	}

    public function edit_content(Request $request,$id){
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$content_detail=DB::table('content')->where('id', '=', $id)->get();
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.editcontent',['content_detail' => $content_detail],['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

    public function editcontent(Request $request,$id){
		$errors=$this->validate($request, [
    			'page_name' => 'required',
    			'content_page_detail' => 'required',
		]);
		$content = Content::findOrFail($id);
		$content->name   = $request->input('page_name');
		$content->page_name   = $request->input('page_name');	
		$content->page = $request->input('content_page_detail');
		$content->is_active = $request->input('is_active');
		$content->save();
		$request->session()->flash('alert-success', 'Content successfully updated !');
		return redirect('admin/contentlist');	
	}

    public function deletecontent(Request $request,$id){
		DB::table("content")->delete($id);
		$request->session()->flash('alert-success', 'Content Page successfully deleted !');
		return redirect('admin/contentlist');
	}

    public function update_position(Request $request,$position,$id){
		$content = Content::findOrFail($id);
		$content->position   = $position;
		$content->save();
		$request->session()->flash('alert-success', 'Position successfully updated !');	
    }		

    public function contentpagelist()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$content = DB::table('content')->get();
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.contentpage_list',['counts' => $array,'content' => $content,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}	

	  
}
