<?php

namespace App\Http\Controllers\admin;

use Auth;
use App\Category;
use App\Company;
use App\News;
use App\User;
use App\Transferbyadmin;
use App\Userredeem;
use App\Cashbackdistribution;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;
class CashbackperController extends MainAdminController
{
	public function __construct()
    {
		
		if (!empty(Auth::check())) {
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return redirect('admin/dashboard');
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
        }
         

    public function index()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.addcashbackper',['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

    public function addcashbackper(Request $request)
	{
		$errors=$this->validate($request, [
    			'inviter_percentage' => 'required',
    			'user_percentage' => 'required',
			'cashback4you_percentage' => 'required'
		]);
		
		$dis = new Cashbackdistribution(array(
      			'inviter_percentage' => $request->input('inviter_percentage'),
      			'user_percentage' =>$request->input('user_percentage'),
			'cashback4you_percentage' => $request->input('cashback4you_percentage')	
    		));
    		$dis->save();
		$request->session()->flash('alert-success', 'Cashback % Distribution successfully added!');
		return redirect('admin/emaillist');
	}

    public function editcashbackper(Request $request){
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$cashback_distribution_detail=DB::table('cashback_distribution')->where('id', '=', '1')->get();
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.editcashback_distribution',['cashback_distribution_detail' => $cashback_distribution_detail],['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

    public function updatecashbackper(Request $request){
		$id=1;
		$errors=$this->validate($request, [
    			'inviter_percentage' => 'required',
    			'user_percentage' => 'required',
			'cashback4you_percentage' => 'required'
		]);
		$cashbackper = Cashbackdistribution::findOrFail($id);
		$cashbackper->inviter_percentage   = $request->input('inviter_percentage');
		$cashbackper->user_percentage = $request->input('user_percentage');
		$cashbackper->cashback4you_percentage = $request->input('cashback4you_percentage');
		$cashbackper->save();
		$request->session()->flash('alert-success', 'Cashback % Distribution successfully updated !');
		return redirect('admin/editcashbackper');	
	}

	  
}
