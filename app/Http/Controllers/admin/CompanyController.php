<?php

namespace App\Http\Controllers\admin;

use Auth;
use File;
use App\Company;
use App\News;
use App\Category;
use App\User;
use App\Store;
use App\Companycategory;
use App\Transferbyadmin;
use App\Userredeem;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;
class CompanyController extends MainAdminController
{
	public function __construct()
    {
		
		 if (!empty(Auth::check())) {
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return redirect('admin/dashboard');
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
        }
         

    public function index()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$store_list=DB::table('store')->where('is_active','=',1)->get();
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.company',['counts' => $array,'email'=>$email,'store_list'=>$store_list]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

    public function companylist()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$company=DB::table("company")->leftjoin('store', 'store.id', '=', 'company.name')->leftjoin('category', 'category.id', '=', 'company.cat_id')->select('store.name as storename','category.name as catname','company.id','company.name','company.cat_id','company.slug','company.detail','company.logo','company.affiliate_link','company.affiliate_name','company.is_active','company.date_add','company.date_upd')->get();
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.company_list',['company' => $company],['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

    public function addcompany(Request $request)
	{
		
		$errors=$this->validate($request, [
    			'company_name' => 'required',
    			'company_description' => 'required',
    			'file' => 'required|mimes:jpeg,png,jpg|image|max:5000',
			'affiliate_company_name' => 'required',
			'affiliate_link' => 'required',
			'cashback' => 'required'
		]);
		$destinationPath = base_path() . '/public/store_logos/'; // upload path
      		$extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
      		$fileName = rand(11111,99999).'.'.$extension; // renameing image
      		$request->file('file')->move($destinationPath, $fileName); // uploading file to given path
		$company = new Company(array(
      			'name' => $request->input('company_name'),
			'cat_id' => $request->input('category_name'),
			'product_name'=> $request->input('affiliate_company_name'),
      			'slug'  => $request->input('company_name').''.rand(11111,99999),
			'detail' =>$request->input('company_description'),
			'affiliate_name' => $request->input('affiliate_company_name'),
			'cashback' => $request->input('cashback'),
			'affiliate_link' => $request->input('affiliate_link'),
			'is_active' => $request->input('is_active'),
			'logo' => $fileName,
			'date_add' => date('Y-m-d h:i:s')	
    		));
    		$company->save();
		$request->session()->flash('alert-success', 'Affiliate Link successfully added!');
		return redirect('admin/companylist');
				
	}

    public function updatecomstatus(Request $request,$id,$status)
	{
		$company = Company::findOrFail($id);
		$company->is_active = $status;
		$company->save();
		$request->session()->flash('alert-success', 'Status successfully updated !');
		return redirect('admin/companylist');
	}
 
    public function deletecompany(Request $request,$id)
	{
		DB::table('company')->delete($id);
		$request->session()->flash('alert-success', 'Company successfully deleted !');
		return redirect('admin/companylist');
	}

    public function editcompany(Request $request,$id){
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$company_detail=DB::table('company')->where('id', '=', $id)->get();
		$store_list=DB::table('store')->where('is_active','=',1)->get();
		$store_cats=DB::table('company_category')->join('category', 'category.id', '=', 'company_category.category_id')->select('category.id','category.name')->where('store_id',$company_detail[0]->name)->get();
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.editcompany',['company_detail' => $company_detail,'store_list'=>$store_list,'store_cats'=>$store_cats],['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

    public function updatecompany(Request $request,$id)
	{
		$company_detail=DB::table('company')->where('id', '=', $id)->get();
		$errors=$this->validate($request, [
    			'company_name' => 'required',
			'category_name' => 'required',
    			'company_description' => 'required',
			'affiliate_company_name' => 'required',
			'affiliate_link' => 'required',
			'cashback' => 'required'	
		]);
		if($request->file('file')!=''){
			$errors=$this->validate($request, [
	    			'file' => 'required|mimes:jpeg,png,jpg|image|max:5000',
			]);		
			$destinationPath = base_path() . '/public/store_logos/'; // upload path
      			$extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
      			$fileName = rand(11111,99999).'.'.$extension; // renameing image
      			$request->file('file')->move($destinationPath, $fileName); // uploading file to given path
			File::delete('store_logos/' . $company_detail[0]->logo);			
			$company = Company::findOrFail($id);
		    	$company->name   = $request->input('company_name');
			$company->cat_id   = $request->input('category_name');
		    	$company->detail = $request->input('company_description');
			$company->product_name    = $request->input('affiliate_company_name');
		    	$company->affiliate_name    = $request->input('affiliate_company_name');
			$company->cashback    = $request->input('cashback');
			$company->affiliate_link    = $request->input('affiliate_link');
		    	$company->is_active = $request->input('is_active');
			$company->logo    = $fileName;
		    	$company->save();
		    	$request->session()->flash('alert-success', 'Company Detail successfully updated !');
		    	return redirect('admin/companylist');
		}
		if($request->file('file')==''){
		    $company = Company::findOrFail($id);
		    $company->name   = $request->input('company_name');
		    $company->cat_id   = $request->input('category_name');	
		    $company->detail  = $request->input('company_description');
		    $company->product_name    = $request->input('affiliate_company_name');	
		    $company->affiliate_name    = $request->input('affiliate_company_name');
		    $company->cashback    = $request->input('cashback'); 	
		    $company->affiliate_link    = $request->input('affiliate_link');	
		    $company->is_active = $request->input('is_active');
		    $company->save();
		    $request->session()->flash('alert-success', 'Company Detail successfully updated !');
		    return redirect('admin/companylist');	
		}
		return view('admin.editcompany',['company_detail' => $company_detail]);
	}

	public function assigncategory()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$category = DB::table('category')->get();
		$company = DB::table('company')->get();
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.assigncategory',['counts' => $array],['category' => $category,'company' => $company,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
		
	}

	public function assign(Request $request)
	{
		$errors=$this->validate($request, [
    			'store_name' => 'required',
    			'category' => 'required'
		]);
		foreach($request->input('category') as $list){
			$category_company = new Companycategory(array(
      			'store_id' => $request->input('store_name'),
      			'category_id'  => $list	
    			));
    			$category_company->save();
		}
		$request->session()->flash('alert-success', 'Categories successfully assigned to store!');
		return redirect('admin/companycategory');
	}

	public function companycategory()
	{
		/* news count start */
		$allnews =DB::table('news')->get();
		$allnews=count($allnews);
		/* news count end */
		/* categories count start*/
		$categories =DB::table('category')->get();
		$categories_count=count($categories);
		/* categories count end */
		/* Affiliate count start*/
		$affiliate_links =DB::table('company')->get();
		$affiliate_count=count($affiliate_links);
		/* Affiliate count end */
		/* Users count start*/
		$users_count=DB::table('User')->where('type','user')->get();
		$users_count=count($users_count);
		/* Users count end */
		$alltrasfers =DB::table('transfer_by_admin')->get();
		$alltrasfers=count($alltrasfers);
		$request =DB::table('user_redeem')->where('status',0)->get();
		$request=count($request);
		$array=[
			'allnews'=>$allnews,	
			'categories_count'=>$categories_count,
			'affiliate_count'=>$affiliate_count,
			'users_count'=>$users_count,
			'alltrasfers'=>$alltrasfers,
			'request'=>$request	
			];
		$res = DB::table('company_category')->select(DB::raw('count(company_category.category_id) as catcount'),'company_category.store_id', 'company.name')->groupBy('company_category.store_id')->join('company', 'company.id', '=', 'company_category.store_id')->get();
		if (!empty(Auth::check())) {
			if(!empty(Auth::user()->email)){
				$email=Auth::user()->email;
			}
			else{
				$email='';
			}
			$loginid = Auth::id();    
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','admin')->get();
			if(!empty($authenticate[0])){
            			return view('admin.companycategory',['countscat' => $res],['counts' => $array,'email'=>$email]);
			}else{
				return redirect('admin');
			} 
        	}
		else{
			return redirect('admin');
		}
	}

	public function select_category(Request $request){
		$store_id=$_REQUEST['storeid'];
		$store_cats=DB::table('company_category')->where('store_id',$store_id)->get();
		echo '<select name="category_name" id="category_name" class="form-control">';
		foreach($store_cats as $cats){
			$category_detail=DB::table('category')->where('id', '=', $cats->category_id)->get();
			//echo "<pre>";
			//print_r($category_detail);
			if(!empty($category_detail)){
			foreach($category_detail as $detail){
			echo '<option value='.$detail->id.'>'.$detail->name.'</option>';
			}
			}
		}
		echo '</select>';
	}

	  
}
