<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\News;
use App\Category;
use App\Company;
use App\Content;
use App\Bannerimg;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;
class FrontNewsController extends MainAdminController
{
	public function __construct()
    {
		
		 //$this->middleware('auth');
        }
         

    public function index()
	{
		$banner = Bannerimg::where('is_active', 1)->orderBy('id','desc')->take(1)->get();	
		$cat_lists = Category::where('is_active', 1)->where('parent_id','=', 0)->orderBy('id','asc')->get()->take(6);
		$links = Company::where('is_active', 1)->orderBy('id','desc')->take(12)->groupby('product_name')->distinct()->get();
		$loginid = Auth::id();
		$results = Content::where('is_active', 1)->where('id','!=', 20)->where('id','!=', 18)->where('id', '!=', 21)->orderBy('position','asc')->get();			
		$single_content = Content::where('is_active', 1)->where('id','=', 18)->orderBy('position','asc')->get();
		include 'Paginator.php';
        	$krows = \Illuminate\Support\Facades\DB::table('news')->where('is_active', '1')->count();
        	$pages = new Paginator('8','p');
        	$pages->set_total($krows);

		$data['news'] = \Illuminate\Support\Facades\DB::table('news')->where('is_active', '1')->orderBy('date_upd', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->get();
		$data['page_links'] = $pages->page_links('?');
		

		if (!empty(Auth::check())) {
			 
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','user')->where('User.is_active','1')->where('User.is_email_verified','1')->get();
		
			if(!empty($authenticate[0])){
				$loginid = Auth::id(); 
				if(!empty(Auth::user()->email)){
					$email=Auth::user()->email;
				}
				else{
					$email='';
				}
            			return \View('news',['menus'=>$results,'email'=>$email,'links'=>$links,'cat_lists'=>$cat_lists,'banner'=>$banner,'single_content'=>$single_content])->with('newslist',$data['news'])->with('newslist1',$data['page_links']);
			}
			else{
				return \View('news',['menus'=>$results,'links'=>$links,'cat_lists'=>$cat_lists,'banner'=>$banner,'single_content'=>$single_content])->with('newslist',$data['news'])->with('newslist1',$data['page_links']);
			}
		}else{
			return \View('news',['menus'=>$results,'links'=>$links,'cat_lists'=>$cat_lists,'banner'=>$banner,'single_content'=>$single_content])->with('newslist',$data['news'])->with('newslist1',$data['page_links']);
		}

	}

    public function sortnews($sort)
	{
		$loginid = Auth::id();	
		$banner = Bannerimg::where('is_active', 1)->orderBy('id','desc')->take(1)->get();	
		$cat_lists = Category::where('is_active', 1)->where('parent_id','=', 0)->orderBy('id','asc')->get()->take(6);
		$links = Company::where('is_active', 1)->orderBy('id','desc')->take(12)->groupby('product_name')->distinct()->get();	
		$results = Content::where('is_active', 1)->where('id','!=', 20)->where('id','!=', 18)->where('id', '!=', 21)->orderBy('position','asc')->get();			
		$single_content = Content::where('is_active', 1)->where('id','=', 18)->orderBy('position','asc')->get();
		include 'Paginator.php';
        	$krows = \Illuminate\Support\Facades\DB::table('news')->where('is_active', '1')->count();
        	$pages = new Paginator('8','p');
        	$pages->set_total($krows);
		if($sort=='date'){
        		$data['news'] = \Illuminate\Support\Facades\DB::table('news')->where('is_active', '1')->orderBy('date_upd', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->get();
		}
		if($sort=='name'){
        		$data['news'] = \Illuminate\Support\Facades\DB::table('news')->where('is_active', '1')->orderBy('name', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->get();
		}

        	$data['page_links'] = $pages->page_links('?');
		

		if (!empty(Auth::check())) {
			 
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','user')->where('User.is_active','1')->where('User.is_email_verified','1')->get();
		
			if(!empty($authenticate[0])){
				$loginid = Auth::id(); 
				if(!empty(Auth::user()->email)){
					$email=Auth::user()->email;
				}
				else{
					$email='';
				}
            			return \View('news',['menus'=>$results,'email'=>$email,$sort,'links'=>$links,'cat_lists'=>$cat_lists,'banner'=>$banner,'single_content'=>$single_content])->with('newslist',$data['news'])->with('newslist1',$data['page_links']);
			}
			else{
				return \View('news',['menus'=>$results,$sort,'links'=>$links,'cat_lists'=>$cat_lists,'banner'=>$banner,'single_content'=>$single_content])->with('newslist',$data['news'])->with('newslist1',$data['page_links']);
			}
		}else{
			return \View('news',['menus'=>$results,$sort,'links'=>$links,'cat_lists'=>$cat_lists,'banner'=>$banner,'single_content'=>$single_content])->with('newslist',$data['news'])->with('newslist1',$data['page_links']);
		}

	}

    public function singlenews($slug)
	{
		$banner = Bannerimg::where('is_active', 1)->orderBy('id','desc')->take(1)->get();	
		$cat_lists = Category::where('is_active', 1)->where('parent_id','=', 0)->orderBy('id','asc')->get()->take(6);
		$links = Company::where('is_active', 1)->orderBy('id','desc')->take(12)->groupby('product_name')->distinct()->get();
		$loginid = Auth::id();
		$results = Content::where('is_active', 1)->where('id','!=', 20)->where('id','!=', 18)->where('id', '!=', 21)->orderBy('position','asc')->get();			
		$single_content = Content::where('is_active', 1)->where('id','=', 18)->orderBy('position','asc')->get();
		$news_detail=DB::table('news')->where('slug', '=', $slug)->get();
		$current_id_news=$news_detail[0]->id;
		$previous = News::where('id', '<', $current_id_news)->max('id');
		if($previous!=''){
			$pre_detail=DB::table('news')->where('id', '=', $previous)->get();
			$pre_slug=$pre_detail[0]->slug;
		}
		if($previous==''){
			$pre_slug='';
		}
		$next = News::where('id', '>', $current_id_news)->min('id');
		if($next!=''){
			$next_detail=DB::table('news')->where('id', '=', $next)->get();
			$next_slug=$next_detail[0]->slug;
		}
		if($next==''){
			$next_slug='';
		}
		if (!empty(Auth::check())) {
			 
             		$authenticate=User::select('User.email')->where('User.id',$loginid)->where('User.type','user')->where('User.is_active','1')->where('User.is_email_verified','1')->get();
		
			if(!empty($authenticate[0])){
				$loginid = Auth::id(); 
				if(!empty(Auth::user()->email)){
					$email=Auth::user()->email;
				}
				else{
					$email='';
				}
            			return view('singlenews',['menus'=>$results,'news_detail'=>$news_detail,'email'=>$email,'links'=>$links,'cat_lists'=>$cat_lists,'banner'=>$banner,'single_content'=>$single_content,'previous'=>$previous,'pre_slug'=>$pre_slug,'next'=>$next,'next_slug'=>$next_slug]);
			}
			else{
				return view('singlenews',['menus'=>$results,'news_detail'=>$news_detail,'links'=>$links,'cat_lists'=>$cat_lists,'banner'=>$banner,'links'=>$links,'single_content'=>$single_content,'previous'=>$previous,'pre_slug'=>$pre_slug,'next'=>$next,'next_slug'=>$next_slug]);
			}
		}else{
			return view('singlenews',['menus'=>$results,'news_detail'=>$news_detail,'links'=>$links,'cat_lists'=>$cat_lists,'banner'=>$banner,'links'=>$links,'single_content'=>$single_content,'previous'=>$previous,'pre_slug'=>$pre_slug,'next'=>$next,'next_slug'=>$next_slug]);
		}
	}

	  
}
