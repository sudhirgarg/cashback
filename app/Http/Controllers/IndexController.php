<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use App\User;
use App\Content;
use App\Company;
use App\Userreferral;
use App\Transactions;
use App\Category;
use App\Bannerimg;
use App\Faq;
use App\Emailtemplate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use DB;

class IndexController extends MainAdminController {

    public function __construct() {
        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return redirect('account');
            } else {
                return redirect('');
            }
        } else {
            return redirect('');
        }
    }

    public function index() {

        $loginid = Auth::id();
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $moreinfo = Content::where('is_active', 1)->where('id', '=', 21)->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        $links = Company::where('is_active', 1)->orderBy('id', 'desc')->take(12)->groupby('product_name')->distinct()->get();
        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                if (!empty(Auth::user()->email)) {
                    $email = Auth::user()->email;
                } else {
                    $email = '';
                }
                return view('home', ['menus' => $results, 'email' => $email, 'links' => $links, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content, 'moreinfo' => $moreinfo]);
            } else {
                return view('home', ['menus' => $results, 'links' => $links, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content, 'moreinfo' => $moreinfo]);
            }
        } else {
            return view('home', ['menus' => $results, 'links' => $links, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content, 'moreinfo' => $moreinfo]);
        }
    }

    public function login() {
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return redirect('account');
            } else {
                return view('login', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content]);
            }
        } else {
            return view('login', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content]);
        }
    }

    public function dologin(Request $request) {
        $this->validate($request, ['email' => 'required|email',
            'password' => 'required']
        );

        $userdata = array(
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'type' => 'user',
            'is_active' => '1'
        );

        if (Auth::attempt($userdata)) {

            return redirect('account');
        } else {

            $request->session()->flash('alert-danger', 'Gebruikersnaam / wachtwoord is niet correct. Probeer het opnieuw !');
            return redirect('login');
        }
    }

    public function signup() {
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return redirect('account');
            } else {
                return view('signup', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content]);
            }
        } else {
            return view('signup', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content]);
        }
    }

    public function sign(Request $request) {
        $emailtemp = Emailtemplate::where('id', 3)->get();
        $content = $emailtemp[0]->content;
        $this->validate($request, ['firstname' => 'required',
            'lastname' => 'required',
            //'email' => 'required|email',
            'email' => 'required|email|unique:User,email',
            'password' => 'required|between:6,20',
            'confirm_password' => 'same:password',
            'terms' => 'required',
                ]
        );
        $password = Hash::make($request->input('password'));
        $email = $request->input('email');
        $email_confirmation_code = str_random(30);
        $firstname = $request->input('firstname');
        $inserts = $request->input('inserts');
        $lastname = $request->input('lastname');
        $link = 'activate/' . $email_confirmation_code;
        $signin = new User(array(
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'inserts' => $request->input('inserts'),
            'email' => $request->input('email'),
            'password' => $password,
            'email_confirmation_token' => $email_confirmation_code,
            'date_add' => date('Y-m-d h:i:s'),
            'type' => 'user'
        ));
        $signin->save();
        $data = array('email_confirmation_code' => $email_confirmation_code, 'firstname' => $firstname, 'lastname' => $lastname, 'link' => $link, 'email' => $email, 'econtent' => $content);


        /* Mail::send("emailverify", $data, function($message) use($data) {
          $message->from('admin@gmail.com');
          $message->to($data['email'])->subject('Verify your email address');
          }); */
        $request->session()->flash('alert-success', 'Bedankt voor uw inschrijving! Controleer uw e-mail.');
        return redirect('login');
    }

    public function activate(Request $request, $act_code) {
        $results = User::where('email_confirmation_token', $act_code)->get();
        if (!empty($results[0])) {
            $id = $results[0]->id;
            $email = $results[0]->email;
            $user = User::findOrFail($id);
            $user->is_email_verified = '1';
            $user->save();
            /* insert referral information */

            $results_ref = Userreferral::where('email_id', $email)->get();
            if (!empty($results_ref[0])) {
                $user_id = $results_ref[0]->user_id;
                $insert_trans = new Transactions(array(
                    'user_id' => $user_id,
                    'amount' => '10',
                    'transaction_id' => 'Referral',
                    'type' => 'referral',
                    'store_id' => 'Bevestigd',
                    'status' => '1',
                    'datetime' => date('Y-m-d h:i:s')
                ));
                $insert_trans->save();
            }
            /* insert referral information end */

            $request->session()->flash('alert-success', 'E-mail account geverifieerd. Gelieve in te loggen nu !');
            return redirect('login');
        } else {
            $request->session()->flash('alert-danger', 'Ongeldige Email Bevestigingscode !');
            return redirect('signup');
        }
    }

    public function forgot() {
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return redirect('account');
            } else {
                return view('forgot', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content]);
            }
        } else {
            return view('forgot', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content]);
        }
    }

    public function forgetpass(Request $request) {
        $emailtemp = Emailtemplate::where('id', 4)->get();
        $content = $emailtemp[0]->content;
        $email = $request->input('email');
        $results = User::where('email', $email)->get();
        if (!empty($results[0])) {
            $forget_pass_code = str_random(30);
            $link = 'updatepass/' . $forget_pass_code;
            $id = $results[0]->id;
            $user = User::findOrFail($id);
            $user->forgot_password_token = $forget_pass_code;
            $user->save();
            $data = array('forget_pass_code' => $forget_pass_code, 'link' => $link, 'email' => $email, 'econtent' => $content);

            Mail::send("emailforgetpass", $data, function($message) use($data) {
                $message->from('admin@gmail.com');
                $message->to($data['email'])->subject('Forget Password Link');
            });
            $request->session()->flash('alert-success', 'Password Link met succes te sturen. Controleer mail !');
            return redirect('login');
        } else {
            $request->session()->flash('alert-danger', 'Ongeldig e-mail ! Voer de juiste e-mailadres.');
            return redirect('forgot');
        }
    }

    public function updatepass($code) {
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        return view('updateforgetpass', ['menus' => $results, 'code' => $code, 'single_content' => $single_content]);
    }

    public function changeforgetpass(Request $request) {
        $this->validate($request, ['password' => 'required|between:6,20',
            'confirm_password' => 'same:password',
                ]
        );
        $password = Hash::make($request->input('password'));
        $code = $request->input('code');
        $results = User::where('forgot_password_token', $code)->get();
        if (!empty($results[0])) {
            $id = $results[0]->id;
            $user = User::findOrFail($id);
            $user->password = $password;
            $user->save();
            $request->session()->flash('alert-success', 'Nieuw wachtwoord met succes aangemaakt. Log alstublieft in !');
            return redirect('login');
        } else {
            $request->session()->flash('alert-danger', 'Ongeldige link ! Maak nieuw wachtwoord koppeling.');
            return redirect('forgot');
        }
    }

    public function content(Request $request, $slug) {
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $links = Company::where('is_active', 1)->orderBy('id', 'desc')->take(12)->groupby('product_name')->distinct()->get();
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        $content_detail = DB::table('content')->where('slug', '=', $slug)->get();
        $loginid = Auth::id();
        if (!empty(Auth::user()->email)) {
            $email = Auth::user()->email;
        } else {
            $email = '';
        }
        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return view('content', ['content_detail' => $content_detail], ['menus' => $results, 'email' => $email, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content, 'links' => $links]);
            } else {
                return view('content', ['content_detail' => $content_detail], ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content, 'links' => $links]);
            }
        } else {
            return view('content', ['content_detail' => $content_detail], ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content, 'links' => $links]);
        }
    }

    public function contact() {
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $loginid = Auth::id();
        if (!empty(Auth::user()->email)) {
            $email = Auth::user()->email;
        } else {
            $email = '';
        }
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        $contact_detail = DB::table('content')->where('id', '=', '20')->get();

        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return view('contact', ['menus' => $results, 'contact_detail' => $contact_detail, 'email' => $email, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content]);
            } else {
                return view('contact', ['menus' => $results, 'contact_detail' => $contact_detail, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content]);
            }
        } else {
            return view('contact', ['menus' => $results, 'contact_detail' => $contact_detail, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content]);
        }
    }

    public function contactus(Request $request) {
        $emailtemp = Emailtemplate::where('id', 6)->get();
        $content = $emailtemp[0]->content;
        $this->validate($request, ['name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
                ]
        );
        $name = $request->input('name');
        $email = $request->input('email');
        $subject = $request->input('subject');
        $msg = $request->input('message');

        $data = array('name' => $name, 'email' => $email, 'subject' => $subject, 'msg' => $msg, 'econtent' => $content);

        Mail::send("emailcontact", $data, function($message) use($data) {
            $message->from($data['email']);
            $message->to('sudhir@cwebconsultants.com')->subject('Query Mail');
        });
        $request->session()->flash('alert-success', 'Mail has been send. We will contact you soon !');
        return redirect('contact');
    }

    public function logout(Request $request) {
        Auth::logout();
        $request->session()->flash('alert-success', 'Succesvol uitgelogd.');
        return redirect('login');
    }

    public function faq() {
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        //$links = Company::where('is_active', 1)->orderBy('id','desc')->take(12)->groupby('product_name')->distinct()->get();
        $loginid = Auth::id();
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        include 'Paginator.php';
        $krows = \Illuminate\Support\Facades\DB::table('faq')->where('is_active', '1')->count();
        $pages = new Paginator('12', 'p');
        $pages->set_total($krows);

        $data['faq'] = \Illuminate\Support\Facades\DB::table('faq')->where('is_active', '1')->orderBy('id', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->get();
        $data['page_links'] = $pages->page_links('?');


        if (!empty(Auth::check())) {

            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                $loginid = Auth::id();
                if (!empty(Auth::user()->email)) {
                    $email = Auth::user()->email;
                } else {
                    $email = '';
                }
                return \View('faqs', ['menus' => $results, 'email' => $email, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content])->with('faqlist', $data['faq'])->with('faqlist1', $data['page_links']);
            } else {
                return \View('faqs', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content])->with('faqlist', $data['faq'])->with('faqlist1', $data['page_links']);
            }
        } else {
            return \View('faqs', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'single_content' => $single_content])->with('faqlist', $data['faq'])->with('faqlist1', $data['page_links']);
        }
    }

}
