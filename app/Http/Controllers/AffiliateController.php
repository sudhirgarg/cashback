<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use Mail;
use App\Content;
use App\Category;
use App\Companycategory;
use App\Company;
use App\Bannerimg;
use App\Linkclicks;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;

class AffiliateController extends MainAdminController {

    public function __construct() {
        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return redirect('account');
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }
    }

    public function index($slug) {

        $catname = Category::where('slug', $slug)->get();
        $cat_name = $catname[0]->name;
        $id = $catname[0]->id;
        $parent_id = $catname[0]->parent_id;
        $parentname = Category::where('id', $parent_id)->get();
        if (!empty($parentname[0])) {
            $pname = $parentname[0]->name;
            $pslug = $parentname[0]->slug;
        } else {
            $pname = '';
            $pslug = '';
        }
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $cats = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get();

        //$links = Company::where('is_active', 1)->orderBy('id','desc')->take(12)->groupby('product_name')->distinct()->get();
        $loginid = Auth::id();
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        include 'Paginator.php';
        $krows = \Illuminate\Support\Facades\DB::table('company')->where('cat_id', $id)->where('is_active', '1')->count();
        $pages = new Paginator('12', 'p');
        $pages->set_total($krows);

        $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('cat_id', $id)->where('is_active', '1')->orderBy('date_upd', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->get();
        $data['page_links'] = $pages->page_links('?');


        if (!empty(Auth::check())) {

            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                $loginid = Auth::id();
                if (!empty(Auth::user()->email)) {
                    $email = Auth::user()->email;
                } else {
                    $email = '';
                }
                return \View('affiliatelinks', ['menus' => $results, 'email' => $email, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $parent_id, 'single_content' => $single_content, 'pname' => $pname, 'pslug' => $pslug])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            } else {
                return \View('affiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $parent_id, 'single_content' => $single_content, 'pname' => $pname, 'pslug' => $pslug])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            }
        } else {
            return \View('affiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $parent_id, 'single_content' => $single_content, 'pname' => $pname, 'pslug' => $pslug])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
        }
    }

    public function header_cat_link($slug) {
        if (isset($_GET['rid'])) {
            $userId = Auth::id();
            $insert_cash = new Linkclicks(array(
                'user_id' => $userId,
                'link' => $_GET['rid'],
                'cashback_amt' => 0,
                'status' => '0'
            ));
            $insert_cash->save();
            return redirect($slug . '/?link_id=' . $insert_cash->id);
        }
        if (isset($_GET['link_id'])) {
            $link_detail = Linkclicks::where('id', $_GET['link_id'])->get();
            return redirect($link_detail[0]->link);
        }

        $catid = Category::where('slug', $slug)->get();
        $id = $catid[0]->id;
        $parent_id = $id;
        $catname = Category::where('slug', $slug)->get();
        $cat_name = $catname[0]->name;
        $cats = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get();
        $cat_detail = Category::where('parent_id', $id)->get();
        $cats_array = array();
        foreach ($cat_detail as $key => $val) {
            $cats_array[] = $val->id;
        }
        array_push($cats_array, $id);

        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        //$links = Company::where('is_active', 1)->orderBy('id','desc')->take(12)->groupby('product_name')->distinct()->get();
        $loginid = Auth::id();
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        include 'Paginator.php';

        if (!empty($cats_array)) {
            $cat_detail1 = Companycategory::whereIn('category_id', $cats_array)->get();
            /* $store = array();
              foreach ($cat_detail1 as $key => $val) {
              $store[] = $val->store_id;
              } */
            //$store = array_values(array_unique($store));
            $krows = \Illuminate\Support\Facades\DB::table('company')->whereIn('cat_id', $cats_array)->where('is_active', '1')->count('product_name');
            $pages = new Paginator('12', 'p');
            $pages->set_total($krows);

            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->whereIn('cat_id', $cats_array)->where('is_active', '1')->orderBy('date_upd', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('id')->distinct()->get();
            /* echo "<pre>";
              print_r($data['links']); */
            $data['page_links'] = $pages->page_links('?');
        }


        if (empty($cats_array)) {
            $krows = \Illuminate\Support\Facades\DB::table('company')->where('cat_id', $id)->where('is_active', '1')->count();
            $pages = new Paginator('12', 'p');
            $pages->set_total($krows);

            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('cat_id', $id)->where('is_active', '1')->orderBy('date_upd', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->get();
            $data['page_links'] = $pages->page_links('?');
        }
        if (!empty(Auth::check())) {

            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                $loginid = Auth::id();
                if (!empty(Auth::user()->email)) {
                    $email = Auth::user()->email;
                } else {
                    $email = '';
                }
                return \View('affiliatelinks', ['menus' => $results, 'email' => $email, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $parent_id, 'single_content' => $single_content])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            } else {
                return \View('affiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $parent_id, 'single_content' => $single_content])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            }
        } else {
            return \View('affiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $parent_id, 'single_content' => $single_content])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
        }
    }

    public function alllinks() {
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $cats = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get();
        //$links = Company::where('is_active', 1)->orderBy('id','desc')->take(12)->groupby('product_name')->distinct()->get();
        $loginid = Auth::id();
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        include 'Paginator.php';
        $krows = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->count();
        $pages = new Paginator('12', 'p');
        $pages->set_total($krows);

        $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->orderBy('date_upd', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('id')->distinct()->get();
        $data['page_links'] = $pages->page_links('?');
        if (!empty(Auth::check())) {

            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                $loginid = Auth::id();
                if (!empty(Auth::user()->email)) {
                    $email = Auth::user()->email;
                } else {
                    $email = '';
                }
                return \View('allaffiliatelinks', ['menus' => $results, 'email' => $email, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'single_content' => $single_content])->with('allaffiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            } else {
                return \View('allaffiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'single_content' => $single_content])->with('allaffiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            }
        } else {
            return \View('allaffiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'single_content' => $single_content])->with('allaffiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
        }
    }

    public static function getsubcats($cat_id) {
        $cats = Category::where('is_active', 1)->where('parent_id', '=', $cat_id)->orderBy('id', 'asc')->get();
        $not = '';
        foreach ($cats as $list) {
            $not.=$list;
        }
        if (!empty($not)) {
            return $cats;
        } else {
            $cats = $not;
            return $cats;
        }
    }

    public static function get_using_slug($slug) {
        $cats = Category::where('is_active', 1)->where('slug', 'LIKE', $slug)->orderBy('id', 'asc')->take(1)->get();
        return $cats;
    }

    public static function is_category_product_exist($category_id) {
        $products = Company::where('is_active', 1)->where('cat_id', $category_id)->get();
        return $products->count();
    }

    public function sortlinks($sort) {
        $parent_id = 0;
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $cats = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get();
        $links = Company::where('is_active', 1)->orderBy('id', 'desc')->take(12)->groupby('product_name')->distinct()->get();
        $loginid = Auth::id();
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        include 'Paginator.php';
        $krows = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->count();
        $pages = new Paginator('12', 'p');
        $pages->set_total($krows);
        if ($sort == 'date') {
            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->orderBy('date_add', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('id')->distinct()->get();
        }
        if ($sort == 'asc') {
            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->orderBy('affiliate_name', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('id')->distinct()->get();
        }
        if ($sort == 'des') {
            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->orderBy('affiliate_name', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('id')->distinct()->get();
        }

        $data['page_links'] = $pages->page_links('?');


        if (!empty(Auth::check())) {

            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                $loginid = Auth::id();
                if (!empty(Auth::user()->email)) {
                    $email = Auth::user()->email;
                } else {
                    $email = '';
                }
                return \View('allaffiliatelinks', ['menus' => $results, 'email' => $email, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'parent_id' => $parent_id, 'single_content' => $single_content])->with('allaffiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            } else {
                return \View('allaffiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'parent_id' => $parent_id, 'single_content' => $single_content])->with('allaffiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            }
        } else {
            return \View('allaffiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'cats' => $cats, 'banner' => $banner, 'parent_id' => $parent_id, 'single_content' => $single_content])->with('allaffiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
        }
    }

    public function sortcatlinks($slug, $sort) {
        $catname = Category::where('slug', $slug)->get();
        $cat_name = $catname[0]->name;
        $id = $catname[0]->id;
        $cats = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get();
        $cat_detail = Category::where('parent_id', $id)->get();
        $cats_array = array();
        foreach ($cat_detail as $key => $val) {
            $cats_array[] = $val->id;
        }
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $links = Company::where('is_active', 1)->orderBy('id', 'desc')->take(12)->groupby('product_name')->distinct()->get();
        $loginid = Auth::id();
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        include 'Paginator.php';
        $cat_detail1 = Companycategory::whereIn('category_id', $cats_array)->get();
        $store = array();
        foreach ($cat_detail1 as $key => $val) {
            $store[] = $val->store_id;
        }

        $store = array_values(array_unique($store));
        $krows = \Illuminate\Support\Facades\DB::table('company')->whereIn('name', $store)->where('is_active', '1')->distinct('name')->count('name');
        $pages = new Paginator('12', 'p');
        $pages->set_total($krows);
        if ($sort == 'date') {
            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->whereIn('name', $store)->orderBy('date_add', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('id')->distinct()->get();
        }
        if ($sort == 'asc') {
            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->whereIn('name', $store)->orderBy('affiliate_name', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('id')->distinct()->get();
        }
        if ($sort == 'des') {
            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->whereIn('name', $store)->orderBy('affiliate_name', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('id')->distinct()->get();
        }

        $data['page_links'] = $pages->page_links('?');


        if (!empty(Auth::check())) {

            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                $loginid = Auth::id();
                if (!empty(Auth::user()->email)) {
                    $email = Auth::user()->email;
                } else {
                    $email = '';
                }
                return \View('affiliatelinks', ['menus' => $results, 'email' => $email, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $id, 'single_content' => $single_content])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            } else {
                return \View('affiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $id, 'single_content' => $single_content])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            }
        } else {
            return \View('affiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $id, 'single_content' => $single_content])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
        }
    }

    public function sortcatlinksub($slug, $sort) {
        $catname = Category::where('slug', $slug)->get();
        $parent_id = $catname[0]->parent_id;
        $id = $catname[0]->id;
        $cat_name = $catname[0]->name;
        $parentname = Category::where('id', $parent_id)->get();
        if (!empty($parentname[0])) {
            $pname = $parentname[0]->name;
            $pslug = $parentname[0]->slug;
        } else {
            $pname = '';
            $pslug = '';
        }
        $cats = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get();
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $links = Company::where('is_active', 1)->orderBy('id', 'desc')->take(12)->groupby('product_name')->distinct()->get();
        $loginid = Auth::id();
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        include 'Paginator.php';
        $krows = \Illuminate\Support\Facades\DB::table('company')->where('cat_id', $id)->where('is_active', '1')->distinct('name')->count('name');
        $pages = new Paginator('12', 'p');
        $pages->set_total($krows);
        if ($sort == 'date') {
            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->where('cat_id', $id)->orderBy('date_add', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('id')->distinct()->get();
        }
        if ($sort == 'asc') {
            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->where('cat_id', $id)->orderBy('affiliate_name', 'asc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('id')->distinct()->get();
        }
        if ($sort == 'des') {
            $data['links'] = \Illuminate\Support\Facades\DB::table('company')->where('is_active', '1')->where('cat_id', $id)->orderBy('affiliate_name', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->groupby('id')->distinct()->get();
        }

        $data['page_links'] = $pages->page_links('?');


        if (!empty(Auth::check())) {

            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                $loginid = Auth::id();
                if (!empty(Auth::user()->email)) {
                    $email = Auth::user()->email;
                } else {
                    $email = '';
                }
                return \View('affiliatelinks', ['menus' => $results, 'email' => $email, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $parent_id, 'single_content' => $single_content, 'pname' => $pname, 'pslug' => $pslug])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            } else {
                return \View('affiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $parent_id, 'single_content' => $single_content, 'pname' => $pname, 'pslug' => $pslug])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
            }
        } else {
            return \View('affiliatelinks', ['menus' => $results, 'cat_lists' => $cat_lists, 'banner' => $banner, 'cat_name' => $cat_name, 'cats' => $cats, 'id' => $id, 'slug' => $slug, 'parent_id' => $parent_id, 'single_content' => $single_content, 'pname' => $pname, 'pslug' => $pslug])->with('affiliatelinks', $data['links'])->with('affiliatelinks1', $data['page_links']);
        }
    }

}

?>