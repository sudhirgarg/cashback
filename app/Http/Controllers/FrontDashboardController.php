<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\User;
use Lang;
use Carbon\Carbon;
use Mail;
use App\Content;
use App\Transactions;
use App\Userreferral;
use App\Cashbackclaimrequest;
use App\Userredeem;
use App\Transferbyadmin;
use App\Category;
use App\Company;
use App\Bannerimg;
use App\Emailtemplate;
use App\Linkclicks;
use App\Cashbackdistribution;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;
use DB;

class FrontDashboardController extends MainAdminController {

    public function __construct() {
        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return redirect('account');
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }
    }

    public function index() {
        $links = Company::where('is_active', 1)->orderBy('id', 'desc')->take(12)->groupby('product_name')->distinct()->get();
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $loginid = Auth::id();
        $total_referral_count = Userreferral::where('user_id', '=', $loginid)->count();
        $sum_redeemed_amount = DB::table('user_redeem')->where('user_id', '=', $loginid)->sum('amount');
        $sum_transfer_amount = DB::table('transfer_by_admin')->where('user_id', '=', $loginid)->sum('amount');
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        $transactions = Transactions::where('user_id', $loginid)->where('status', 1)->orderBy('id', 'desc')->take(5)->get();
        $transactions_all = Transactions::where('user_id', $loginid)->orderBy('id', 'desc')->take(5)->get();

        $trans_total = DB::table('transactions')
                ->select(DB::raw('SUM(amount) as total_sum'))
                ->where('status', 1)
                ->where('user_id', $loginid)
                ->havingRaw('SUM(amount)')
                ->first();
        if ($trans_total == '') {
            $trans_total = 0;
        } else {
            $trans_total = $trans_total->total_sum;
        }

        $balance_redeem_amount = ($trans_total + $sum_transfer_amount) - $sum_redeemed_amount;

        $user_detail = DB::table('User')->where('id', '=', $loginid)->get();

        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $email = Auth::user()->email;
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return view('account', ['menus' => $results, 'transactions' => $transactions, 'transactions_all' => $transactions_all, 'trans_total' => $trans_total, 'total_referral_count' => $total_referral_count, 'balance_redeem_amount' => $balance_redeem_amount, 'user_detail' => $user_detail, 'email' => $email, 'cat_lists' => $cat_lists, 'banner' => $banner, 'links' => $links, 'single_content' => $single_content]);
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }
    }

    public function listtrans() {

        $links = Company::where('is_active', 1)->orderBy('id', 'desc')->take(12)->groupby('product_name')->distinct()->get();
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $loginid = Auth::id();
        $total_referral_count = Userreferral::where('user_id', '=', $loginid)->count();
        $sum_redeemed_amount = DB::table('user_redeem')->where('user_id', '=', $loginid)->sum('amount');
        $sum_transfer_amount = DB::table('transfer_by_admin')->where('user_id', '=', $loginid)->sum('amount');
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        $transactions = Transactions::where('user_id', $loginid)->where('status', 1)->orderBy('id', 'desc')->take(5)->get();
        $transactions_all = Transactions::where('user_id', $loginid)->orderBy('id', 'desc')->get();
        include 'Paginator.php';
        $krows = \Illuminate\Support\Facades\DB::table('transactions')->where('user_id', $loginid)->count();
        $pages = new Paginator('12', 'p');
        $pages->set_total($krows);

        $data['links'] = \Illuminate\Support\Facades\DB::table('transactions')->where('user_id', $loginid)->orderBy('id', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->get();
        $data['page_links'] = $pages->page_links('?');



        $trans_total = DB::table('transactions')
                ->select(DB::raw('SUM(amount) as total_sum'))
                ->where('status', 1)
                ->where('user_id', $loginid)
                ->havingRaw('SUM(amount)')
                ->first();
        if ($trans_total == '') {
            $trans_total = 0;
        } else {
            $trans_total = $trans_total->total_sum;
        }

        $balance_redeem_amount = ($trans_total + $sum_transfer_amount) - $sum_redeemed_amount;

        $user_detail = DB::table('User')->where('id', '=', $loginid)->get();

        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $email = Auth::user()->email;
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return view('listtrans', ['menus' => $results, 'transactions' => $transactions, 'transactions_all' => $transactions_all, 'trans_total' => $trans_total, 'total_referral_count' => $total_referral_count, 'balance_redeem_amount' => $balance_redeem_amount, 'user_detail' => $user_detail, 'email' => $email, 'cat_lists' => $cat_lists, 'banner' => $banner, 'links' => $links, 'single_content' => $single_content])->with('alltrans', $data['links'])->with('alltrans1', $data['page_links']);
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }
    }

    public function bonustrans() {

        $links = Company::where('is_active', 1)->orderBy('id', 'desc')->take(12)->groupby('product_name')->distinct()->get();
        $banner = Bannerimg::where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        $cat_lists = Category::where('is_active', 1)->where('parent_id', '=', 0)->orderBy('id', 'asc')->get()->take(6);
        $loginid = Auth::id();
        $total_referral_count = Userreferral::where('user_id', '=', $loginid)->count();
        $sum_redeemed_amount = DB::table('user_redeem')->where('user_id', '=', $loginid)->sum('amount');
        $sum_transfer_amount = DB::table('transfer_by_admin')->where('user_id', '=', $loginid)->sum('amount');
        $results = Content::where('is_active', 1)->where('id', '!=', 20)->where('id', '!=', 18)->where('id', '!=', 21)->orderBy('position', 'asc')->get();
        $single_content = Content::where('is_active', 1)->where('id', '=', 18)->orderBy('position', 'asc')->get();
        $transactions = Transactions::where('user_id', $loginid)->where('status', 1)->orderBy('id', 'desc')->take(5)->get();
        $transactions_all = Transactions::where('user_id', $loginid)->orderBy('id', 'desc')->get();
        include 'Paginator.php';
        $krows = \Illuminate\Support\Facades\DB::table('transactions')->where('user_id', $loginid)->count();
        $pages = new Paginator('12', 'p');
        $pages->set_total($krows);

        $data['links'] = \Illuminate\Support\Facades\DB::table('transactions')->where('user_id', $loginid)->orderBy('id', 'desc')->skip($pages->get_limit2())->take($pages->get_perpage())->get();
        $data['page_links'] = $pages->page_links('?');



        $trans_total = DB::table('transactions')
                ->select(DB::raw('SUM(amount) as total_sum'))
                ->where('status', 1)
                ->where('user_id', $loginid)
                ->havingRaw('SUM(amount)')
                ->first();
        if ($trans_total == '') {
            $trans_total = 0;
        } else {
            $trans_total = $trans_total->total_sum;
        }

        $balance_redeem_amount = ($trans_total + $sum_transfer_amount) - $sum_redeemed_amount;

        $user_detail = DB::table('User')->where('id', '=', $loginid)->get();

        if (!empty(Auth::check())) {
            $loginid = Auth::id();
            $email = Auth::user()->email;
            $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();

            if (!empty($authenticate[0])) {
                return view('bonustrans', ['menus' => $results, 'transactions' => $transactions, 'transactions_all' => $transactions_all, 'trans_total' => $trans_total, 'total_referral_count' => $total_referral_count, 'balance_redeem_amount' => $balance_redeem_amount, 'user_detail' => $user_detail, 'email' => $email, 'cat_lists' => $cat_lists, 'banner' => $banner, 'links' => $links, 'single_content' => $single_content])->with('allbonustrans', $data['links'])->with('allbonustrans1', $data['page_links']);
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }
    }

    public function referral(Request $request) {
        $emailtemp = Emailtemplate::where('id', 5)->get();
        $content = $emailtemp[0]->content;
        $loginid = Auth::id();
        $this->validate($request, ['name' => 'required',
            'surname' => 'required',
            'email' => 'required|email|unique:user_referral,email_id',
            'my_email' => 'required|email'
                ]
        );

        $email = $request->input('email');
        $my_email = $request->input('my_email');
        $results = Userreferral::where('email_id', $email)->get();
        //$email_confirmation_code = str_random(30);
        $name = $request->input('name');
        $surname = $request->input('surname');
        //$link = 'activate/'.$email_confirmation_code;
        if (!empty($results[0])) {
            $request->session()->flash('alert-danger', 'E-mail Id al gebruikt voor verwijzing !');
            return redirect('account');
        } else {
            $referral = new Userreferral(array(
                'user_id' => $loginid,
                'email_id' => $request->input('email'),
            ));
            $referral->save();
            $results_referral_user = User::where('id', $loginid)->where('type', 'user')->get();
            $user_firstname = $results_referral_user[0]->firstname;
            $user_lastname = $results_referral_user[0]->lastname;
            $data = array('name' => $name, 'surname' => $surname, 'email' => $email, 'my_email' => $my_email, 'user_firstname' => $user_firstname, 'user_lastname' => $user_lastname, 'econtent' => $content);


            Mail::send("emailreferral", $data, function($message) use($data) {
                $message->from($data['my_email']);
                $message->to($data['email'])->subject('Referral Link');
            });
            $request->session()->flash('alert-success', 'Uitnodigingsmail is succesvol verstuurd.');
            return redirect('account');
        }
    }

    public function claimrequest(Request $request) {
        $loginid = Auth::id();
        $this->validate($request, ['type_claim' => 'required',
            'type_shop' => 'required',
            'select_visit' => 'required',
            'client_number' => 'required',
            'order_of' => 'required',
            'policy' => 'required',
            'used_equipment' => 'required',
            'used_browser' => 'required',
            'purchase_immidiate' => 'required',
            'use_discount_code' => 'required',
            'cookies' => 'required',
            'browser_in' => 'required',
            'ideal_payment' => 'required',
            'condition_of_shop' => 'required'
                ]
        );

        $claim_request = new Cashbackclaimrequest(array(
            'user_name' => $loginid,
            'type_claim' => $request->input('type_claim'),
            'select_shop' => $request->input('type_shop'),
            'select_visit' => $request->input('select_visit'),
            'client_number' => $request->input('client_number'),
            'order_of' => $request->input('order_of'),
            'policy' => $request->input('policy'),
            'used_equipment' => $request->input('used_equipment'),
            'used_browser' => $request->input('used_browser'),
            'purchase_immediately' => $request->input('purchase_immidiate'),
            'use_discount_code' => $request->input('use_discount_code'),
            'cookies' => $request->input('cookies'),
            'browser_in' => $request->input('browser_in'),
            'ideal_payment' => $request->input('ideal_payment'),
            'condition_of_shop' => $request->input('condition_of_shop'),
            'description' => $request->input('description'),
            'date_add' => date('Y-m-d h:i:s')
        ));
        $claim_request->save();

        $request->session()->flash('alert-success', ' Claim succes ingediend voor cash back .');
        return redirect('account');
    }

    public function updatedetail(Request $request) {
        $loginid = Auth::id();
        $user_detail = DB::table('User')->where('id', '=', $loginid)->get();
        $user_email = $user_detail[0]->email;
        $req_email = $request->input('email');
        $req_pass = $request->input('password');
        if ($user_email != $req_email && $req_pass != '') {
            $this->validate($request, ['firstname' => 'required',
                'lastname' => 'required',
                'dob' => 'required',
                'email' => 'required|email|unique:User,email',
                'password' => 'required|between:6,20',
                'confirm_password' => 'same:password',
                    ]
            );
            $password = Hash::make($request->input('password'));
            $signin = User::findOrFail($loginid);
            $signin->firstname = $request->input('firstname');
            $signin->lastname = $request->input('lastname');
            $signin->email = $request->input('email');
            $signin->dob = $request->input('dob');
            $signin->sex = $request->input('sex');
            $signin->password = $password;
            $signin->save();

            $signin_ad = User::findOrFail($loginid);
            $signin_ad->firstname = $request->input('firstname');
            $signin_ad->email = $request->input('email');
            $signin_ad->password = $password;
            $signin_ad->save();
        }
        if ($user_email == $req_email && $req_pass != '') {
            $this->validate($request, ['firstname' => 'required',
                'lastname' => 'required',
                'dob' => 'required',
                'password' => 'required|between:6,20',
                'confirm_password' => 'same:password',
                    ]
            );
            $password = Hash::make($request->input('password'));
            $signin = User::findOrFail($loginid);
            $signin->firstname = $request->input('firstname');
            $signin->lastname = $request->input('lastname');
            $signin->dob = $request->input('dob');
            $signin->sex = $request->input('sex');
            $signin->password = $password;
            $signin->save();

            $signin_ad = User::findOrFail($loginid);
            $signin_ad->firstname = $request->input('firstname');
            $signin_ad->password = $password;
            $signin_ad->save();
        }
        if ($user_email == $req_email && $req_pass == '') {
            $this->validate($request, ['firstname' => 'required',
                'lastname' => 'required',
                'dob' => 'required',
                    ]
            );
            $signin = User::findOrFail($loginid);
            $signin->firstname = $request->input('firstname');
            $signin->lastname = $request->input('lastname');
            $signin->dob = $request->input('dob');
            $signin->sex = $request->input('sex');
            $signin->save();

            $signin_ad = User::findOrFail($loginid);
            $signin_ad->firstname = $request->input('firstname');
            $signin_ad->save();
        }
        if ($user_email != $req_email && $req_pass == '') {
            $this->validate($request, ['firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email|unique:User,email',
                'dob' => 'required',
                    ]
            );
            $signin = User::findOrFail($loginid);
            $signin->firstname = $request->input('firstname');
            $signin->lastname = $request->input('lastname');
            $signin->email = $request->input('email');
            $signin->dob = $request->input('dob');
            $signin->sex = $request->input('sex');
            $signin->save();

            $signin_ad = User::findOrFail($loginid);
            $signin_ad->name = $request->input('firstname');
            $signin_ad->email = $request->input('email');
            $signin_ad->save();
        }
        $request->session()->flash('alert-success', ' Detail bijgewerkt .');
        return redirect('account');
    }

    public function paymentreq(Request $request) {
        $loginid = Auth::id();
        $this->validate($request, ['desired_amount' => 'required|numeric|min:1',
            'account_no' => 'required',
            'bic_number' => 'required',
            'ter_name_van' => 'required',
                ]
        );
        $sum_redeemed_amount = DB::table('user_redeem')->where('user_id', '=', $loginid)->sum('amount');
        $sum_transfer_amount = DB::table('transfer_by_admin')->where('user_id', '=', $loginid)->sum('amount');
        $trans_total = DB::table('transactions')
                ->select(DB::raw('SUM(amount) as total_sum'))
                ->where('status', 1)
                ->where('user_id', $loginid)
                ->havingRaw('SUM(amount)')
                ->first();
        if ($trans_total == '') {
            $trans_total = 0;
        } else {
            $trans_total = $trans_total->total_sum;
        }
        $balance_redeem_amount = ($trans_total + $sum_transfer_amount) - $sum_redeemed_amount;
        $req_amount = $request->input('desired_amount');
        if ($balance_redeem_amount > $req_amount && $req_amount > 25) {
            $redeem = new Userredeem(array(
                'user_id' => $loginid,
                'amount' => $request->input('desired_amount'),
                'acc_no' => $request->input('account_no'),
                'bic_number' => $request->input('bic_number'),
                'ter_name' => $request->input('ter_name_van')
            ));
            $redeem->save();
            $request->session()->flash('alert-success', 'Gevraagde bedrag succesvol ingediend.');
        } else {
            $request->session()->flash('alert-danger', 'Gevraagde bedrag moet de Term voorwaarden volgen!');
        }
        return redirect('account');
    }

    public function link_click(Request $request, $id) {
        $loginid = Auth::id();
        $authenticate = User::select('User.email')->where('User.id', $loginid)->where('User.type', 'user')->where('User.is_active', '1')->where('User.is_email_verified', '1')->get();
        $email = $authenticate[0]->email;
        $getid = Userreferral::select('user_referral.user_id')->where('user_referral.email_id', $email)->get();
        if (!empty($getid[0])) {
            $referral_user_id = $getid[0]->user_id;
        } else {
            $referral_user_id = 0;
        }
        $link_detail = DB::table('company')->where('id', '=', $id)->get();
        $distribution_detail = DB::table('cashback_distribution')->where('id', '=', '1')->get();
        $link = $link_detail[0]->affiliate_link;
        $insert_cash = new Linkclicks(array(
            'user_id' => $loginid,
            'link' => $link,
            'link_id' => $id,
            'cashback_amt' => 100,
            'status' => '1'
        ));
        $insert_cash->save();
        if ($referral_user_id > 0) {
            $invite_percentage = $distribution_detail[0]->inviter_percentage;
            $invite_cash = number_format((100 * $invite_percentage) / 100, 2);
            $insert_trans_invite = new Transactions(array(
                'user_id' => $referral_user_id,
                'amount' => $invite_cash,
                'transaction_id' => 'Referral',
                'type' => 'referral',
                'store_id' => 'Bevestigd',
                'status' => '1',
                'datetime' => date('Y-m-d h:i:s')
            ));
            $insert_trans_invite->save();
        }
        $user_percentage = $distribution_detail[0]->user_percentage;
        $user_cash_who_click = number_format((100 * $user_percentage) / 100, 2);
        $insert_trans_user_cash_who_click = new Transactions(array(
            'user_id' => $loginid,
            'amount' => $user_cash_who_click,
            'transaction_id' => 'Api',
            'type' => 'Api',
            'store_id' => 'Bevestigd',
            'status' => '1',
            'datetime' => date('Y-m-d h:i:s')
        ));
        $insert_trans_user_cash_who_click->save();
        $request->session()->flash('alert-success', 'Cashback succesvol toegevoegd aan uw account.');
        return redirect('account');
    }

}
