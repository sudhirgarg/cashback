<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Socialite;
use App\User;
use Illuminate\Support\Facades\Hash;

class SocialAuthController extends Controller {

    public function redirect() {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback() {
        $providerUser = \Socialite::driver('facebook')->user();
        $avtar = array(
            'avatar' => $providerUser->avatar,
            'avatar_original' => $providerUser->avatar_original,
        );

        $user_detail = $providerUser->user;

        if (User::where('email', '=', $user_detail['email'])->exists()) {
            $users = User::all();
            foreach ($users as $user) {
                if ($user->email == $user_detail['email']) {
                    $user = Auth::loginUsingId($user->id);
                    return redirect('account');
                }
            }
        } else {
            $without_hash = '12345678';
            $password = Hash::make($without_hash);
            $signin = new User();
            $signin->firstname = $user_detail['name'];
            $signin->lastname = $user_detail['name'];
            $signin->email = $user_detail['email'];
            $signin->password = $password;
            $signin->date_add = date('Y-m-d h:i:s');
            $signin->type = 'user';
            $signin->is_email_verified = 1;
            $signin->save();
        }

        if (Auth::attempt(['email' => $user_detail['email'], 'password' => $without_hash])) {
            return redirect('account');
        } else {
            return redirect('login');
        }
    }

}
