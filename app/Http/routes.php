<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

/* * *****************************Admin login Index controller********************* */
Route::get('admin/', 'admin\IndexController@index');
Route::post('admin/login', 'admin\IndexController@postLogin');
Route::get('admin/dashboard', 'admin\DashboardController@index');
Route::get('admin/logout', 'admin\IndexController@logout');
/* * *****************************Admin Company controller********************* */
Route::get('admin/company', 'admin\CompanyController@index');
Route::get('admin/companylist', 'admin\CompanyController@companylist');
Route::post('admin/addcompany', 'admin\CompanyController@addcompany');
Route::get('admin/updatecomstatus/{id}/{status}', 'admin\CompanyController@updatecomstatus');
Route::get('admin/deletecompany/{id}', 'admin\CompanyController@deletecompany');
Route::get('admin/editcompany/{id}', 'admin\CompanyController@editcompany');
Route::post('admin/updatecompany/{id}', 'admin\CompanyController@updatecompany');
Route::get('admin/select_category', 'admin\CompanyController@select_category');
/* * *****************************Admin Content controller********************* */
Route::get('admin/addcontent', 'admin\ContentController@index');
Route::post('admin/add_content', 'admin\ContentController@add_content');
Route::get('admin/contentlist', 'admin\ContentController@contentpagelist');
Route::get('admin/updatestatus_content/{id}/{status}', 'admin\ContentController@updatestatus_content');
Route::get('admin/edit_content/{id}', 'admin\ContentController@edit_content');
Route::post('admin/editcontent/{id}', 'admin\ContentController@editcontent');
Route::get('admin/deletecontent/{id}', 'admin\ContentController@deletecontent');
Route::get('admin/update_position/{position}/{id}', 'admin\ContentController@update_position');
/* * *****************************Admin Category controller********************* */
Route::get('admin/addcategory', 'admin\CategoryController@index');
Route::get('admin/categorylist', 'admin\CategoryController@categorylist');
Route::get('admin/updatecatstatus/{id}/{status}', 'admin\CategoryController@updatecatstatus');
Route::post('admin/addcat', 'admin\CategoryController@addcategory');
Route::get('admin/deletecat/{id}', 'admin\CategoryController@deletecat');
Route::get('admin/editcat/{id}', 'admin\CategoryController@editcat');
Route::post('admin/updatecategory/{id}', 'admin\CategoryController@updatecategory');
/* * *****************************Admin transaction controller********************* */
Route::get('admin/transactions', 'admin\TransactionController@index');
/* * *****************************Admin Users controller********************* */
Route::get('admin/users', 'admin\HomeuserController@index');
Route::get('admin/updateuserstatus/{id}/{status}', 'admin\HomeuserController@updateuserstatus');
Route::get('admin/deleteuser/{id}', 'admin\HomeuserController@deleteuser');
Route::get('admin/usertransaction/{id}', 'admin\HomeuserController@usertransaction');
/* * *****************************Admin News controller********************* */
Route::get('admin/addnews', 'admin\NewsController@index');
Route::get('admin/newslist', 'admin\NewsController@newslist');
Route::post('admin/addnews', 'admin\NewsController@addnews');
Route::get('admin/editnews/{id}', 'admin\NewsController@editnews');
Route::post('admin/updatenews/{id}', 'admin\NewsController@updatenews');
Route::get('admin/deletenews/{id}', 'admin\NewsController@deletenews');
Route::get('admin/updatestatus/{id}/{status}', 'admin\NewsController@updatestatus');

/* * ****************************Admin Store controller********************* */
Route::get('admin/addstore', 'admin\StoreController@index');
Route::get('admin/storelist', 'admin\StoreController@storelist');
Route::post('admin/addstore', 'admin\StoreController@addstore');
Route::get('admin/editstore/{id}', 'admin\StoreController@editstore');
Route::post('admin/updatestore/{id}', 'admin\StoreController@updatestore');
Route::get('admin/deletestore/{id}', 'admin\StoreController@deletestore');
Route::get('admin/updatestatus/{id}/{status}', 'admin\StoreController@updatestatus');
Route::get('admin/updatestorecat_storelist/{id}', 'admin\StoreController@updatestorecat_storelist');
Route::post('admin/updatestorecat/{id}', 'admin\StoreController@updatestorecat');
/* * *****************************Admin Category Assign to Store controller********************* */
Route::get('admin/assigncategory', 'CompanyController@assigncategory');
Route::post('admin/assign', 'CompanyController@assign');
Route::get('admin/companycategory', 'CompanyController@companycategory');
/* * *****************************Admin Payment Request controller********************* */
Route::get('admin/requestpayment', 'admin\RequestpaymentController@index');
Route::get('admin/updateredeemstatus/{id}/{status}', 'admin\RequestpaymentController@updateredeemstatus');
/* * *****************************Admin Claim Filed controller********************* */
Route::get('admin/claimfiled', 'admin\ClaimfiledController@index');
Route::get('admin/updateclaimfilestatus/{id}/{status}', 'admin\ClaimfiledController@updateclaimfilestatus');
Route::get('admin/viewclaimdetail/{id}', 'admin\ClaimfiledController@viewclaimdetail');

/* * *****************************Admin Rating Iframe controller********************* */
Route::get('admin/addiframe', 'admin\IframeController@index');
Route::post('admin/saveiframe', 'admin\IframeController@saveiframe');

/* * *****************************Admin Transfer Cashback controller********************* */
Route::get('admin/transfer', 'admin\TransfercashController@index');
Route::post('admin/addtransfer', 'admin\TransfercashController@addtransfer');
Route::get('admin/transfercashlist', 'admin\TransfercashController@transferlist');
/* * *****************************Admin Banner Image controller********************* */
Route::get('admin/addbanner', 'admin\BannerimgController@index');
Route::get('admin/bannerlist', 'admin\BannerimgController@bannerlist');
Route::post('admin/addbanner', 'admin\BannerimgController@addbanner');
Route::get('admin/updatebannerstatus/{id}/{status}', 'admin\BannerimgController@updatebannerstatus');
Route::get('admin/deletebanner/{id}', 'admin\BannerimgController@deletebanner');
/* * *****************************Admin Faq controller********************* */
Route::get('admin/addfaq', 'admin\FaqController@index');
Route::post('admin/addfaq', 'admin\FaqController@addfaq');
Route::get('admin/faqlist', 'admin\FaqController@faqlist');
Route::get('admin/updatefstatus/{id}/{status}', 'admin\FaqController@updatefstatus');
Route::get('admin/editfaq/{id}', 'admin\FaqController@editfaq');
Route::post('admin/updatefaq/{id}', 'admin\FaqController@updatefaq');
Route::get('admin/deletefaq/{id}', 'admin\FaqController@deletefaq');
/* * *****************************Admin Api controller********************* */
Route::get('admin/addapi', 'admin\ApiController@index');
Route::post('admin/addapi', 'admin\ApiController@addapi');
Route::get('admin/apilist', 'admin\ApiController@apilist');
Route::get('admin/editapi/{id}', 'admin\ApiController@editapi');
Route::post('admin/updateapi/{id}', 'admin\ApiController@updateapi');
/* * *****************************Admin Mail Setting controller********************* */
Route::get('admin/addmail', 'admin\MailController@index');
Route::post('admin/addmail', 'admin\MailController@addmail');
Route::get('admin/maillist', 'admin\MailController@maillist');
Route::get('admin/editmail/{id}', 'admin\MailController@editmail');
Route::post('admin/updatemail/{id}', 'admin\MailController@updatemail');
/* * *****************************Admin Email Template controller********************* */
Route::get('admin/addemailtemplate', 'admin\EmailController@index');
Route::get('admin/emaillist', 'admin\EmailController@emaillist');
Route::get('admin/updatetemplatestatus/{id}/{status}', 'admin\EmailController@updatetemplatestatus');
Route::post('admin/addemail', 'admin\EmailController@addemail');
Route::get('admin/deleteemail/{id}', 'admin\EmailController@deleteemail');
Route::get('admin/editemail/{id}', 'admin\EmailController@editemail');
Route::post('admin/updateemail/{id}', 'admin\EmailController@updateemail');
/* * *****************************Admin Cashback % Distribution controller********************* */
Route::get('admin/addcashback', 'admin\CashbackperController@index');
Route::post('admin/addcashbackper', 'admin\CashbackperController@addcashbackper');
Route::get('admin/editcashbackper', 'admin\CashbackperController@editcashbackper');
Route::post('admin/updatecashbackper', 'admin\CashbackperController@updatecashbackper');

/* Front End controllers */

/* * ******************************* Facebook Login Controller ******************************** */
Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

/* * ******************************* Search Controller ******************************** */
Route::get('search/', 'SearchController@index');
Route::get('search/{var}/{type}', 'SearchController@sort');

/* * ******************************* Home page Controller ******************************** */

Route::get('', 'IndexController@index');
Route::get('login', 'IndexController@login');
Route::get('logout', 'IndexController@logout');
Route::get('signup', 'IndexController@signup');
Route::post('sign', 'IndexController@sign');
Route::post('forgetpass', 'IndexController@forgetpass');
Route::get('updatepass/{code}', 'IndexController@updatepass');
Route::post('changeforgetpass', 'IndexController@changeforgetpass');
Route::get('activate/{act_code}', 'IndexController@activate');
Route::get('forgot', 'IndexController@forgot');
Route::get('content/{slug}', 'IndexController@content');
Route::post('dologin', 'IndexController@dologin');
Route::get('contact', 'IndexController@contact');
Route::get('faq', 'IndexController@faq');
Route::post('contactus', 'IndexController@contactus');
/* * ******************************* Account page Controller ******************************** */
Route::get('account', 'FrontDashboardController@index');
Route::get('listtrans', 'FrontDashboardController@listtrans');
Route::get('bonustrans', 'FrontDashboardController@bonustrans');
Route::post('referral', 'FrontDashboardController@referral');
Route::post('claimrequest', 'FrontDashboardController@claimrequest');
Route::post('updatedetail', 'FrontDashboardController@updatedetail');
Route::post('paymentreq', 'FrontDashboardController@paymentreq');
/* * ******************************* News page Controller ******************************** */
Route::get('news', 'FrontNewsController@index');
Route::get('sortnews/{date}', 'FrontNewsController@sortnews');
Route::get('singlenews/{slug}', 'FrontNewsController@singlenews');
/* * ******************************* Category Affiliate Links Controller ******************************** */
Route::get('alllinks', 'AffiliateController@alllinks');
Route::get('{slug}', 'AffiliateController@index');
Route::get('sortlinks/{date}', 'AffiliateController@sortlinks');
Route::get('{slug}/{date}', 'AffiliateController@sortcatlinks');
Route::get('catlinks/sort/{slug}/{date}', 'AffiliateController@sortcatlinksub');
Route::get('{slug}', 'AffiliateController@header_cat_link');


/* * ******************************* Link Click Controller ******************************** */
Route::get('{id}', 'FrontDashboardController@link_click');