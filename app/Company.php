<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company';

    protected $fillable = ['name','cat_id','product_name','slug','detail','logo','affiliate_link','affiliate_name','cashback','is_active','date_add','date_upd','api_product_id'];
 
	
	 public $timestamps = false;
    
}
