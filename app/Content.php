<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'content';

    protected $fillable = ['name','slug','urlname','page','short_description','meta_keyword','meta_description','publish_date','page_name','position','is_active','is_deleted','image','meta_robots_index','meta_robots_follow','include_xml_sitemap','sitemap_priority','canonical'];
 
	 public $timestamps = false;
    
}
