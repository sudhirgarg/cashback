<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cashbackdistribution extends Model
{
    protected $table = 'cashback_distribution';

    protected $fillable = ['inviter_percentage','user_percentage','cashback4you_percentage'];
 
	
	 public $timestamps = false;
    
}
