<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faq';

    protected $fillable = ['name','detail','is_active','date_add'];
 
	
	 public $timestamps = false;
    
}
