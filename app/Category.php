<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    protected $table = 'category';
    protected $fillable = ['name', 'slug', 'parent_id', 'sequence_number', 'is_active', 'date_add', 'date_upd', 'api_cat_id'];
    public $timestamps = false;

}
