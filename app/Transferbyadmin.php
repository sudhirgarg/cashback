<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transferbyadmin extends Model
{
    protected $table = 'transfer_by_admin';

    protected $fillable = ['user_id','amount','date_add','status'];
 
	 public $timestamps = false;
    
}
