<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userreferral extends Model
{
    protected $table = 'user_referral';

    protected $fillable = ['user_id','email_id','datetime','status'];
 
	
	 public $timestamps = false;
    
}
