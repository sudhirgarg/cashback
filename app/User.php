<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'User';

    protected $fillable = ['email','password','remember_token','ip_address','last_visit','total_visit','last_visit_ip_address','is_active','is_deleted','is_email_verified','firstname','lastname','inserts','sex','dob','forgot_password_token','email_confirmation_token','type','date_add','date_upd'];
 
	
	 public $timestamps = false;
    
}
