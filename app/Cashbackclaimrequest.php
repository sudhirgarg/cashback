<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cashbackclaimrequest extends Model
{
    protected $table = 'cashback_claim_request';

    protected $fillable = ['user_name','type_claim','select_shop','select_visit','client_number','order_of','policy','used_equipment','used_browser','purchase_immediately','use_discount_code','cookies','browser_in','ideal_payment','condition_of_shop','description','status','date_add','date_upd'];
 
	
	 public $timestamps = false;
    
}
