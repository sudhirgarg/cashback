<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adminemail extends Model
{
    protected $table = 'admin_email';

    protected $fillable = ['admin_email','support_email'];
 
	
	 public $timestamps = false;
    
}
