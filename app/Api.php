<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    protected $table = 'Api';

    protected $fillable = ['api_name','api_key','api_secret','is_active'];
 
	
	 public $timestamps = false;
    
}
