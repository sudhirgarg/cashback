<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bannerimg extends Model
{
    protected $table = 'banner_img';

    protected $fillable = ['img','date_add','is_active'];
 
	
	 public $timestamps = false;
    
}
