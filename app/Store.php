<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model {

    protected $table = 'store';
    protected $fillable = ['name', 'slug', 'is_active', 'date_add', 'date_upd','store_api_id'];
    public $timestamps = false;

}
