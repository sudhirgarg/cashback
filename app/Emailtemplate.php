<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emailtemplate extends Model
{
    protected $table = 'email_template';

    protected $fillable = ['name','emailvar','slug','content','is_active'];
 
	
	 public $timestamps = false;
    
}
