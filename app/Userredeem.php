<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userredeem extends Model
{
    protected $table = 'user_redeem';

    protected $fillable = ['user_id','amount','acc_no','bic_number','ter_name','datetime','status'];
 
	
	 public $timestamps = false;
    
}
