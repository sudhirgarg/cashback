<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companycategory extends Model
{
    protected $table = 'company_category';

    protected $fillable = ['store_id','category_id'];
 
	
	 public $timestamps = false;
    
}
