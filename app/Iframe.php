<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iframe extends Model {

    protected $table = 'iframe';
    protected $fillable = ['name', 'detail', 'is_active', 'date_add'];
    public $timestamps = false;

}
