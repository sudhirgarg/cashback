<?php

if ($_SERVER['HTTP_HOST'] == 'localhost') {
    $facebook_client_id = '347145535677258';
    $facebook_client_secret = '4aa492825eaf96898849923b1ee1c2fc';
    $facebook_redirect = 'http://localhost/cashback/callback';
}

if ($_SERVER['HTTP_HOST'] == 'qtx.in') {
    $facebook_client_id = '1808259536097718';
    $facebook_client_secret = 'b344fd3a9d2c03073ace0fdc9facfcf2';
    $facebook_redirect = 'http://qtx.in/cashback/callback';
}
if ($_SERVER['HTTP_HOST'] == '139.59.3.57') {
    $facebook_client_id = '778321955638839';
    $facebook_client_secret = '920121d709cbb62059738412a2fadfe8';
    $facebook_redirect = 'http://139.59.3.57/cashback/callback';
}

if ($_SERVER['HTTP_HOST'] == 'cashback4you.nl') {
    $facebook_client_id = '543559282500471';
    $facebook_client_secret = '80772f38107afd42785e9bc0f446790e';
    $facebook_redirect = 'http://cashback4you.nl/callback';
}


return [

    /*
      |--------------------------------------------------------------------------
      | Third Party Services
      |--------------------------------------------------------------------------
      |
      | This file is for storing the credentials for third party services such
      | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
      | default location for this type of information, allowing packages
      | to have a conventional place to find your various credentials.
      |
     */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],
    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],
    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],
    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => $facebook_client_id,
        'client_secret' => $facebook_client_secret,
        'redirect' => $facebook_redirect,
    ],
];
